package ph.com.cdu.apptwo.presentation.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.presentation.activity.view.SearchView;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteBookInfoAsyncTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPdfFragment extends Fragment implements View.OnClickListener {

    /**
     * Key string for saving the state of current page index.
     */
    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";

    private ParcelFileDescriptor mFileDescriptor;
    private PdfRenderer mPdfRenderer;
    private MainView mMainView;
    private SearchView mSearchView;
    private FragmentManager mFragmentManager;
    private PdfRenderer.Page mCurrentPage;
    @BindView(R.id.imageViewPdf)
    ImageView mImgViewPdf;
    @BindView(R.id.linearLayoutNavigation)
    LinearLayout mLnrLayoutNavigation;
    @BindView(R.id.buttonPrevious)
    Button mBtnPrevious;
    @BindView(R.id.buttonNext)
    Button mBtnNext;
    String pdfUserId,pdfTitle;
    Uri pdfUri;
    int pdfId,pageCount;

    public ViewPdfFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_pdf, container, false);
        ButterKnife.bind(this,view);
        initFragmentManager();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            pdfId = getArguments().getInt(getString(R.string.params_unique_id));
            pdfUserId = getArguments().getString(getString(R.string.params_userId));
            pdfTitle = getArguments().getString(getString(R.string.params_book_title));
            pdfUri = getArguments().getParcelable(getString(R.string.params_book_url_download));
            initLocker();
        }
    }

    private void initLocker() {
        try {
            if (hasFileActive(pdfUri.getPath())) {
                decrypt(pdfUri.getPath());
//                openRenderer();
            } else {
                deleteBookInfo();
                Toast.makeText(getContext(), R.string.alert_error_occured_please_download_again,Toast.LENGTH_SHORT).show();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFragment() {
        mFragmentManager = getActivity().getSupportFragmentManager();
        int index = mFragmentManager.getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(index);
        return backEntry.getName();
    }

    @Override
    public void onDetach() {
        try {
            closeRenderer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        File encrypt = new File(getString(R.string.app_root_directory)+pdfId
                +pdfUserId);
        if (encrypt.exists()) {
            if (encrypt.delete()) {
                System.out.print(getString(R.string.app_root_directory)+pdfId
                        +pdfUserId);
            } else {
                System.out.print(getString(R.string.app_root_directory)+pdfId
                        +pdfUserId);
            }
        }
    }

    private void closeRenderer() throws IOException {
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        mPdfRenderer.close();
        mFileDescriptor.close();
    }

    private void openRenderer() throws IOException {
        File decrypt = new File(getString(R.string.app_root_directory)+pdfId
                +pdfUserId);
        mFileDescriptor = ParcelFileDescriptor.open(decrypt, ParcelFileDescriptor.MODE_READ_ONLY);
        mPdfRenderer = new PdfRenderer(mFileDescriptor);
        pageCount=mPdfRenderer.getPageCount();
    }

    private void deleteBookInfo() {
        DeleteBookInfoAsyncTask deleteBookInfoAsyncTask = new DeleteBookInfoAsyncTask(getContext(),pdfId);
        deleteBookInfoAsyncTask.execute();
    }

    private boolean hasFileActive(String path) {
        File f = new File(path);
        boolean fileExists =  f.isFile();
        if (!fileExists) {
            Log.d("not exist", "create");
            return false;
        }
        return true;
    }

    void decrypt(String path) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        FileInputStream fis = new FileInputStream(path);

        FileOutputStream fos = new FileOutputStream(getString(R.string.app_root_directory)+pdfId
                +pdfUserId);
        SecretKeySpec sks = new SecretKeySpec(getString(R.string.app_password).getBytes(), getString(R.string.app_type_file));
        Cipher cipher = Cipher.getInstance(getString(R.string.app_type_file));
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[8];
        while((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
    }

    private void initFragmentManager() {
        if (!getFragment().equals(getString(R.string.title_activity_search))) {
            mMainView = (MainView) getContext();
        } else {
            mSearchView = (SearchView) getContext();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null != mCurrentPage) {
            outState.putInt(STATE_CURRENT_PAGE_INDEX, mCurrentPage.getIndex());
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Bind events.
        mBtnPrevious.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        // Show the first page by default.
        int index = 0;
        // If there is a savedInstanceState (screen orientations, etc.), we restore the page index.
        if (null != savedInstanceState) {
            index = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 0);
        }
        showPage(index);
    }

    private void showPage(int index) {
        if (pageCount <= index) {
            return;
        }
        // Make sure to close the current page before opening another one.
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        // Use `openPage` to open a specific page in PDF.
        mCurrentPage = mPdfRenderer.openPage(index);
        // Important: the destination bitmap must be ARGB (not RGB).
        Bitmap bitmap = Bitmap.createBitmap(
                getResources().getDisplayMetrics().densityDpi * mCurrentPage.getWidth() / 72,
                getResources().getDisplayMetrics().densityDpi * mCurrentPage.getHeight() / 72,
                Bitmap.Config.ARGB_8888);

//                Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(),
//                Bitmap.Config.ARGB_8888);
        // Here, we render the page onto the Bitmap.
        // To render a portion of the page, use the second and third parameter. Pass nulls to get
        // the default result.
        // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
        mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        // We are ready to show the Bitmap to user.
        mImgViewPdf.setImageBitmap(bitmap);
        updateUi();
    }

    private void updateUi() {
        int index = mCurrentPage.getIndex();
        mBtnPrevious.setEnabled(0 != index);
        mBtnNext.setEnabled(index + 1 < pageCount);
        mLnrLayoutNavigation.setVisibility(View.VISIBLE);
        if (mMainView != null) {
            mMainView.setMainTitle(pdfTitle,getString(R.string.book_name_with_index, index + 1, pageCount),View.GONE);
        } else {
            mSearchView.setMainTitle(pdfTitle,getString(R.string.book_name_with_index, index + 1, pageCount));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButtonBack: {
                mFragmentManager.popBackStackImmediate();
                break;
            }
            case R.id.buttonPrevious: {
                // Move to the previous page
                showPage(mCurrentPage.getIndex() - 1);
                break;
            }
            case R.id.buttonNext: {
                // Move to the next page
                showPage(mCurrentPage.getIndex() + 1);
                break;
            }
        }
    }
}
