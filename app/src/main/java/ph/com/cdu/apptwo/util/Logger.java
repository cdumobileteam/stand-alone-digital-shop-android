package ph.com.cdu.apptwo.util;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import ph.com.cdu.apptwo.R;

import static com.google.firebase.analytics.FirebaseAnalytics.*;
import static com.google.firebase.analytics.FirebaseAnalytics.Event.*;
import static com.google.firebase.analytics.FirebaseAnalytics.Param.*;

/**
 * Created by Patrick Santos on 08/02/2018.
 */

public class Logger {

    private static final String LOGOUT = "logout";
    private Context mContext;
    private Bundle mBundle;
    private FirebaseAnalytics mFirebaseAnalytics;

    public Logger(Context context) {
        mContext = context;
        mBundle = new Bundle();
        mFirebaseAnalytics = getInstance(mContext);
    }

    private void sendLogEventParams(String eventName, Bundle mBundle) {
        mFirebaseAnalytics.logEvent(eventName,mBundle);
    }

    public void logBookShare(String contentType, String itemId) {
        mBundle.putString(CONTENT_TYPE,contentType);
        mBundle.putString(ITEM_ID,itemId);
        sendLogEventParams(SHARE,mBundle);
    }

    public void logSignUp(String signUpMethod) {
        mBundle.putString(METHOD,signUpMethod);
        sendLogEventParams(SIGN_UP,mBundle);
    }

    public void logSignIn(String signUpMethod) {
        mBundle.putString(METHOD,signUpMethod);
        sendLogEventParams(LOGIN,mBundle);
    }

    public void logSignOut(String signUpMethod) {
        mBundle.putString(METHOD,signUpMethod);
        sendLogEventParams(LOGOUT,mBundle);
    }

    public void logAppEvent(String contentType) {
        mBundle.putString(CONTENT_TYPE,contentType);
        sendLogEventParams(contentType,mBundle);
    }

    public void logBookCategory(String category) {
        mBundle.putString(ITEM_CATEGORY,category);
        sendLogEventParams(VIEW_ITEM_LIST, mBundle);
    }

    public void logBookDetail(String itemId, String itemName, String itemCategory, Double price) {
        mBundle.putString(ITEM_ID,itemId);
        mBundle.putString(ITEM_NAME,itemName);
        mBundle.putString(ITEM_CATEGORY,itemCategory);
        mBundle.putDouble(PRICE,price);
        mBundle.putString(CURRENCY, mContext.getString(R.string.currency_philippines));
        sendLogEventParams(VIEW_ITEM,mBundle);
    }

    public void logBookPurchase(String transactionId) {
        mBundle.putDouble(VALUE, 15.00);
        mBundle.putString(CURRENCY, mContext.getString(R.string.currency_philippines));
        mBundle.putString(TRANSACTION_ID,transactionId);
        sendLogEventParams(ECOMMERCE_PURCHASE, mBundle);
    }

    public void logBookDownload() {
        mBundle.putDouble(VALUE, 0.00);
        mBundle.putString(CURRENCY, mContext.getString(R.string.currency_philippines));
        sendLogEventParams(PURCHASE_REFUND, mBundle);
    }

    public void logBookSearch(String keyword) {
        mBundle.putString(SEARCH_TERM,keyword);
        sendLogEventParams(VIEW_SEARCH_RESULTS, mBundle);
    }
}
