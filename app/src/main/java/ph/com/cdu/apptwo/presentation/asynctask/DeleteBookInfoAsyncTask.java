package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 26/10/2017.
 */

public class DeleteBookInfoAsyncTask extends AsyncTask<BookInfo, Void, Integer> {
    private Context mContext;
    private int mUniqueId;
    private BookInfo mBookDetails;

    public DeleteBookInfoAsyncTask(Context context, int uniqueId) {
        mBookDetails = new BookInfo();
        this.mContext = context;
        this.mUniqueId = uniqueId;
    }

    @Override
    protected Integer doInBackground(BookInfo... params) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        mBookDetails.setPid(mUniqueId);
        return databaseInteractor.deleteBookInfo(mContext, mBookDetails);
    }
}
