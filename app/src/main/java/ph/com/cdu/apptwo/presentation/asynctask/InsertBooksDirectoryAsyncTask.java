package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Patrick Santos on 09/02/2018.
 */

public class InsertBooksDirectoryAsyncTask extends AsyncTask<BookShop, Void, Long> {
    private Context mContext;
    private String mBookId, mBookTitle, mBookAuthor, mBookGenre,
            mBookDescription, mBookImageUrl, mBookDownloadUrl, mBookPrice, mBookCategory;
    private BookShop mBookData;

    public InsertBooksDirectoryAsyncTask(Context mContext, String mBookId, String mBookTitle,
                                         String mBookAuthor, String mBookGenre, String mBookDescription,
                                         String mBookImageUrl, String mBookDownloadUrl, String mBookPrice, String mBookCategory) {
        mBookData = new BookShop();
        this.mContext = mContext;
        this.mBookId = mBookId;
        this.mBookTitle = mBookTitle;
        this.mBookAuthor = mBookAuthor;
        this.mBookGenre = mBookGenre;
        this.mBookDescription = mBookDescription;
        this.mBookImageUrl = mBookImageUrl;
        this.mBookDownloadUrl = mBookDownloadUrl;
        this.mBookPrice = mBookPrice;
        this.mBookCategory = mBookCategory;
    }

    @Override
    protected Long doInBackground(BookShop... bookShops) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        mBookData.setId(mBookId);
        mBookData.setTitle(mBookTitle);
        mBookData.setAuthor(mBookAuthor);
        mBookData.setGenre(mBookGenre);
        mBookData.setDescription(mBookDescription);
        mBookData.setImage_url(mBookImageUrl);
        mBookData.setBook_url(mBookDownloadUrl);
        mBookData.setPrice(mBookPrice);
        mBookData.setCategory(mBookCategory);
        return databaseInteractor.insertBooks(mContext, mBookData);
    }

    private interface  CallBackTask {

    }
}
