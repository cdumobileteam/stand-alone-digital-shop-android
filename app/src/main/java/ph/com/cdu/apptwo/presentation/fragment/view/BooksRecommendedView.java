package ph.com.cdu.apptwo.presentation.fragment.view;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.books.RecommendedBooks;
import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksRecommendedView {

    void setUpRecyclerViewDeck();

    void setUpLayoutManagerDeck();

    void countBooksAvailable(int librarySize);

    void onStoreRecommendedBookContent(List<FreeBooks> recommendedBooks);

    void goToFullVersion(FreeBooks recommendedBooksData);

    void onInitRecommendedBooksContent();

    void setUpBookViews();

    void noRomanceAvailable(int romance);

    void noHorrorMysteryAvailable(int horrorMystery);

    void noFantasyAvailable(int fantasy);

    void noSciFiAvailable(int sciFi);

    void noActionAdventureAvailable(int actionAdventure);

    void noComedyAvailable(int comedy);

    void setUpListeners();

    void getBooks();
}
