package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.requestParameters.NewUserParams;
import ph.com.cdu.apptwo.data.rest.response.NewUserResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 03/11/2017.
 */

public class NewUserInteractorImpl implements NewUserInteractor {

    private CDUService mService;

    @Override
    public void requestNewUser(final onNewUserDataLoadedListener listener, Context context, NewUserParams newUserParams) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_NEW_USER);

        mService.insertNewUser(newUserParams.mapParameters(context)).enqueue(new Callback<NewUserResponse>() {
            @Override
            public void onResponse(Call<NewUserResponse> call, Response<NewUserResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<NewUserResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
