package ph.com.cdu.apptwo.presentation.presenter;

/**
 * Created by Neil Cruz on 11/10/2017.
 */

public interface MainActivityPresenter {

    void onCreate();

    void onLoginSuccess();

    void onTappedLogoutFacebook();

    void onTappedLogoutGooglePlus();

    void onLogoutSuccess();

    void onDisplayedUserInfo(String accountUsed, String fName, String lName, String profilePicUri);

    void onManageGoogleApi();

    void initBindService();

    void initUserAccount();

    void onTappedSearch();

    void onReCreate();

    void initAnalytics();

    void onNetworkConnectionLoss();

    void initDownloadRequest();

    void setUpSocialButtons(int socialMedia, int color);
}
