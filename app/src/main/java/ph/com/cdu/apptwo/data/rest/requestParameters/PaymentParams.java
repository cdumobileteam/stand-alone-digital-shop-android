package ph.com.cdu.apptwo.data.rest.requestParameters;

import com.fasterxml.jackson.annotation.JsonProperty;

import ph.com.cdu.apptwo.data.rest.BaseParameter;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public class PaymentParams extends BaseParameter {
    @JsonProperty("account_id")
    private String account_id;
    @JsonProperty("product_id")
    private String product_id;
    @JsonProperty("transaction_id")
    private String transaction_id;
    @JsonProperty("order_id")
    private String order_id;
    @JsonProperty("book_id")
    private String book_id;
    @JsonProperty("book_title")
    private String book_title;
    @JsonProperty("book_image")
    private String book_image;
    @JsonProperty("book_url")
    private String book_url;

    public PaymentParams(String account_id, String product_id, String transaction_id, String order_id, String book_id, String book_title, String book_image, String book_url) {
        this.account_id = account_id;
        this.product_id = product_id;
        this.transaction_id = transaction_id;
        this.order_id = order_id;
        this.book_id = book_id;
        this.book_title = book_title;
        this.book_image = book_image;
        this.book_url = book_url;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getBook_title() {
        return book_title;
    }

    public void setBook_title(String book_title) {
        this.book_title = book_title;
    }

    public String getBook_image() {
        return book_image;
    }

    public void setBook_image(String book_image) {
        this.book_image = book_image;
    }

    public String getBook_url() {
        return book_url;
    }

    public void setBook_url(String book_url) {
        this.book_url = book_url;
    }
}
