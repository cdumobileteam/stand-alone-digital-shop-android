package ph.com.cdu.apptwo.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.SearchBooks;
import ph.com.cdu.apptwo.presentation.activity.view.SearchView;
import ph.com.cdu.apptwo.util.NetworkConnection;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public class BookInfoAdapter extends RecyclerView.Adapter<BookInfoAdapter.ViewHolder> {

    private Context mContext;
    private SearchView mSearchView;
    private List<SearchBooks> mBookShop;
    private NetworkConnection mInternet;
    String keyword;

    public BookInfoAdapter(Context context, SearchView searchView, List<SearchBooks> bookShop) {
        mContext = context;
        mSearchView = searchView;
        mBookShop = bookShop;
        mInternet = new NetworkConnection(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_bookinfo, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        SearchBooks bookShop = mBookShop.get(position);
        showResult(holder, bookShop);
        holder.mLnrLayoutBook.setVisibility(VISIBLE);
    }

    private void showResult(final ViewHolder holder, SearchBooks bookShop) {
        holder.mTxtViewBookTitle.setText(bookShop.getTitle());
        holder.mTxtViewBookAuthor.setText(bookShop.getAuthor());
        Picasso.with(mContext).load(bookShop.getImage_url()).fit().into(holder.mImgButtonBookImage, new Callback() {
            @Override
            public void onSuccess() {
                holder.mImgViewPlaceholder.setVisibility(GONE);
            }

            @Override
            public void onError() {
                holder.mImgButtonBookImage.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mBookShop != null) {
            mSearchView.countBooksAvailable(mBookShop.size());
            return mBookShop.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imageButtonBookImage)
        ImageButton mImgButtonBookImage;
        @BindView(R.id.imageViewPlaceholder)
        ImageView mImgViewPlaceholder;
        @BindView(R.id.linearLayoutBook)
        LinearLayout mLnrLayoutBook;
        @BindView(R.id.textViewBookTitle)
        TextView mTxtViewBookTitle;
        @BindView(R.id.textViewBookAuthor)
        TextView mTxtViewBookAuthor;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mImgButtonBookImage.setOnClickListener(this);
            mLnrLayoutBook.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            SearchBooks bookShop = getBookDetails(getAdapterPosition());
            if (checkConnection()) {
//                mSearchView.goToBookAvailability(bookShop);
                mSearchView.goToBookDetails(bookShop);
            } else {
                mSearchView.setUpSnackBar();
            }
        }

        private boolean checkConnection() {
            return mInternet.hasActiveConnection(mContext.getString(R.string.url_digitalshop2));
        }
    }

    private SearchBooks getBookDetails(int position) {
    return mBookShop.get(position);
    }
}
