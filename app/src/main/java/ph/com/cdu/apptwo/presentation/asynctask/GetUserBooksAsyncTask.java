package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public class GetUserBooksAsyncTask extends AsyncTask<String, Void, List<BookInfo>> {
    private Context mContext;
    private CallBackTask mCallBackTask;

    public GetUserBooksAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<BookInfo> doInBackground(String... strings) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        return databaseInteractor.getUserBooks(mContext,strings[0]);
    }

    @Override
    protected void onPostExecute(List<BookInfo> bookInfo) {
        super.onPostExecute(bookInfo);
        mCallBackTask.callBack(bookInfo);
    }

    public void setmCallBackTask(CallBackTask mCallBackTask) {
        this.mCallBackTask = mCallBackTask;
    }

    public interface CallBackTask {
        void callBack(List<BookInfo> bookInfo);
    }
}
