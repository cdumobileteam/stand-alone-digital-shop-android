package ph.com.cdu.apptwo.data.rest;

import android.content.Context;

import java.util.Map;

/**
 * Created by Neil Cruz on 06/11/2017.
 */

public class BaseParameter {

    public Map<String, Object> mapParameters(Context context) {
        return BaseMapper.mapParameters(this);
    }
}
