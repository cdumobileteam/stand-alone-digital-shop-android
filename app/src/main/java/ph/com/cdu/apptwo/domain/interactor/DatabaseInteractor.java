package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

public interface DatabaseInteractor {

    void setUpDatabase(Context context) throws IOException;

    List<UserInfo> loadAllUserInfo(Context context);

    long insertUserInfo(Context context, UserInfo userInfo);

    void deleteUserInfo(Context context);

    boolean isUserInfoSaved(Context context, int userId);

    List<BookInfo> loadAllBookInfo(Context context);

    long insertBookInfo(Context context, BookInfo bookInfo);

    int deleteBookInfo(Context context, BookInfo bookInfo);

    List<BookInfo> getUserBooks(Context context, String userId);

    List<BookShop> loadAllBooks(Context context);

    long insertBooks(Context context, BookShop bookShop);

    void deleteAllBooks(Context context);

    List<BookShop> getAllBooksByCategory(Context context, String bookCategory);

    List<String> getBooksByTitle(Context context, String bookTitle);

    List<BookShop> getBookInfo(Context context, String bookTitle);
}
