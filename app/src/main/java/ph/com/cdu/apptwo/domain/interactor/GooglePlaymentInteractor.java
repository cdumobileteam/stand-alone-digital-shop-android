package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.requestParameters.GooglePlaymentParams;
import ph.com.cdu.apptwo.data.rest.response.GooglePlaymentResponse;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public interface GooglePlaymentInteractor {
    interface onGooglePlaymentDataLoadedListener {
        void onSuccess(GooglePlaymentResponse googlePlaymentData);

        void onFailed();
    }

    void onRequestGooglePlayment(onGooglePlaymentDataLoadedListener listener, Context context, GooglePlaymentParams googlePlaymentParams);
}
