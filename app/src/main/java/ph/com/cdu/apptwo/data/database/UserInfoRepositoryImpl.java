package ph.com.cdu.apptwo.data.database;

import android.content.Context;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.UserInfo;
import ph.com.cdu.apptwo.domain.repository.UserInfoRepository;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

public class UserInfoRepositoryImpl implements UserInfoRepository {

    AppDatabase mAppDatabase;

    public UserInfoRepositoryImpl(Context context) {
        mAppDatabase = AppDatabase.getDatabase(context);
    }

    @Override
    public List<UserInfo> getALLUserInfo() {
        return mAppDatabase.userInfoDao().selectAllUserInfo();
    }

    @Override
    public boolean isUserSaved(int userId) {
        UserInfo userInfoWithId = mAppDatabase.userInfoDao().selectUserInfoById(userId);
        return userInfoWithId != null;
    }

    @Override
    public Long insert(UserInfo userInfo) {
        return mAppDatabase.userInfoDao().insertUserInfo(userInfo);
    }

    @Override
    public void delete() {
        mAppDatabase.userInfoDao().deleteUserInfo();
    }
}
