package ph.com.cdu.apptwo.presentation.presenter;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksRecommendedPresenter {

    void onCreate();

    void initRecommendedBooks();

    void initLinearLayoutManagerDeck();

    void initLoadRecommendedBooksContent();

    void onFinish();

    void onConnected();
}
