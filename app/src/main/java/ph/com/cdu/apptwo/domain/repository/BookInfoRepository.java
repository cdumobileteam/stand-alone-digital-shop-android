package ph.com.cdu.apptwo.domain.repository;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 25/10/2017.
 */

public interface BookInfoRepository {
    List<BookInfo> getALLBookInfo();

    List<BookInfo> getUserBooks(String userId);

    Long insert(BookInfo bookInfo);

    int delete(BookInfo bookInfo);
}
