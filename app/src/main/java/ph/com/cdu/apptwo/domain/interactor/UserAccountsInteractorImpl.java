package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.response.UserAccountsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 17/01/2018.
 */

public class UserAccountsInteractorImpl implements UserAccountsInteractor {

    private CDUService mService;

    @Override
    public void requestUserAccount(final onUserAccountsDataLoadedListener listener, Context context) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_USERS);

        mService.getUserAccounts().enqueue(new Callback<UserAccountsResponse>() {
            @Override
            public void onResponse(Call<UserAccountsResponse> call, Response<UserAccountsResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<UserAccountsResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
