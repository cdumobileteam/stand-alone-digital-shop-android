package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.requestParameters.MyBooksParams;
import ph.com.cdu.apptwo.data.rest.response.MyBooksResponse;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public interface MyBooksInteractor {
    interface onMyBooksOnDataListener {
        void onSuccess(MyBooksResponse myBooksData);

        void onFailed();
    }

    void onRequestMyBooks(onMyBooksOnDataListener listener, Context context, MyBooksParams myBooksParams);
}
