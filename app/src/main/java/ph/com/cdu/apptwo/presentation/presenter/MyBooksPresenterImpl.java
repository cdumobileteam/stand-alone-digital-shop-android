package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.requestParameters.MyBooksParams;
import ph.com.cdu.apptwo.data.rest.response.MyBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.MyBooksInteractor;
import ph.com.cdu.apptwo.domain.interactor.MyBooksInteractorImpl;
import ph.com.cdu.apptwo.domain.model.books.MyBooks;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteBookInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.GetUserBooksAsyncTask;
import ph.com.cdu.apptwo.presentation.fragment.view.MyBooksView;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;
import ph.com.cdu.apptwo.util.NetworkConnection;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public class MyBooksPresenterImpl implements MyBooksPresenter, MyBooksInteractor.onMyBooksOnDataListener {

    private Context mContext;
    private DataDownloader mSharedPreferences;
    private MyBooksView mMyBooksView;
    private MyBooksInteractor mMyBooksInteractor;
    private MyBooksParams mMyBooksParams;
    private NetworkConnection mInternet;

    public MyBooksPresenterImpl(Context context, MyBooksView myBooksView) {
        mContext = context;
        mMyBooksView = myBooksView;
        mSharedPreferences = new DataDownloader(mContext);
        mMyBooksInteractor = new MyBooksInteractorImpl();
        mInternet = new NetworkConnection(mContext);
    }

    @Override
    public void onCreate() {
        mMyBooksView.setUpRecyclerView();
        mMyBooksView.setUpListeners();
        mMyBooksParams = new MyBooksParams(mSharedPreferences.getUserId());
        mMyBooksInteractor.onRequestMyBooks(this,mContext,mMyBooksParams);
    }

    @Override
    public void initMyBooks(final String user_id_fk, List<MyBooks> cShopBooks) {
        readBooks(user_id_fk, cShopBooks);
    }

    @Override
    public void onTappedButton() {
        mMyBooksInteractor.onRequestMyBooks(this,mContext,mMyBooksParams);
    }

    private void readBooks(final String user_id_fk, final List<MyBooks> cShopBooks) {
        GetUserBooksAsyncTask getUserBooksAsyncTask = new GetUserBooksAsyncTask(mContext);
        getUserBooksAsyncTask.setmCallBackTask(new GetUserBooksAsyncTask.CallBackTask() {
            @Override
            public void callBack(List<BookInfo> bookInfo) {
                if (cShopBooks.size() == bookInfo.size() &&
                        bookInfo.size() > 0) {
                    mMyBooksView.onLoadMyBooksContent(bookInfo, user_id_fk);
                } else {
                    if (hasNetworkConnection()) {
                        if (cShopBooks.size() == 0) {
                            mMyBooksView.showBookStatus(hasNetworkConnection());
                        } else {
                            mMyBooksView.showMyCShopBooks(cShopBooks, user_id_fk, bookInfo);
                        }
                    } else {
                        if (bookInfo.size() != 0) {
                            mMyBooksView.onLoadMyBooksContent(bookInfo, user_id_fk);
                        } else {
                            mMyBooksView.showBookStatus(hasNetworkConnection());
                        }
                    }
                }
            }
        });
        getUserBooksAsyncTask.execute(user_id_fk);
    }

    private boolean hasNetworkConnection() {
        return mInternet.hasActiveConnection(mContext.getString(R.string.url_digitalshop2));
    }

    @Override
    public void onSuccess(MyBooksResponse myBooksData) {
        mMyBooksView.onLoadUserTransactions(myBooksData.getResponse_code(),myBooksData.getResult());
    }

    @Override
    public void onFailed() {
        readBooks(mSharedPreferences.getUserId(), new ArrayList<MyBooks>(0));
    }

    @Override
    public void onTappedOpenPDFReader(int pid, String userId, String id, String title, Uri uri) {
        mMyBooksView.initReader(pid,userId,id,title,uri);
    }

    @Override
    public void onView(int uniqueId, File filePath, Uri uriPath, String mime) {
        Log.d("filePath:", filePath.getPath());
        Log.d("uriPath",uriPath.getPath());
        if (!hasFileActive(filePath.getPath())) {
            deleteBookInfo(uniqueId);
            mMyBooksView.onViewBookFailed();
        }
        mMyBooksView.goToViewBook(uriPath, mime);
    }

    private void deleteBookInfo(int uniqueId) {
        DeleteBookInfoAsyncTask deleteBookInfoAsyncTask = new DeleteBookInfoAsyncTask(mContext,uniqueId);
        deleteBookInfoAsyncTask.execute();
    }

    @Override
    public void initAnalytics() {
        mMyBooksView.goAnalytics();
    }

    private boolean hasFileActive(String path) {
        File f = new File(path);
        boolean fileExists =  f.isFile();
        if (!fileExists) {
            Log.d("not exist", "create");
            return false;
        }
        return true;
    }
}
