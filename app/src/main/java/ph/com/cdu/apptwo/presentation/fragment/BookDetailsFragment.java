package ph.com.cdu.apptwo.presentation.fragment;

import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.BuildConfig;
import ph.com.cdu.apptwo.domain.model.User.UserPurchase;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.requestParameters.CShopBooksParams;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.presentation.activity.view.SearchView;
import ph.com.cdu.apptwo.presentation.asynctask.QueryBookInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.fragment.view.BookDetailsView;
import ph.com.cdu.apptwo.presentation.presenter.BookDetailsPresenter;
import ph.com.cdu.apptwo.presentation.presenter.BookDetailsPresenterImpl;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;
import ph.com.cdu.apptwo.presentation.util.ServiceDownloader;
import ph.com.cdu.apptwo.presentation.util.TimerUtil;
import ph.com.cdu.apptwo.util.Gaid;
import ph.com.cdu.apptwo.util.Logger;
import ph.com.cdu.apptwo.util.NetworkConnection;
import ph.com.cdu.apptwo.util.PDFViewer;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

import static android.app.Activity.RESULT_OK;
import static ph.com.cdu.apptwo.util.IabHelper.BILLING_RESPONSE_RESULT_OK;
import static ph.com.cdu.apptwo.util.IabHelper.RESPONSE_BUY_INTENT;
import static ph.com.cdu.apptwo.util.IabHelper.RESPONSE_CODE;


public class BookDetailsFragment extends Fragment implements BookDetailsView, View.OnClickListener {

    private Bundle mBundle, mOwnedBooks;
    private Context mContext;
    private NetworkConnection mInternet;
    private FragmentManager mFragmentManager;
    private DataDownloader mSharedPreferences;
    private MainView mMainView;
    private SearchView mSearchView;
    private BookDetailsPresenter mPresenter;
    private CShopBooksParams mFreeBookDetails;
    private ServiceConnection mServiceConn;
    private GifDrawable mGifDrawable;
    private BroadcastReceiver onComplete;
    private IInAppBillingService mService;
    private List<UserPurchase> mUserPurchase;
    private ArrayList<String> mOwnedSkus;
    private ArrayList<String> mPurchaseDataList;
    private ArrayList<String> mSignatureList;
    private Logger mLogger;
    private Gaid mAnalytics;
    int mBookPid;
    String mEvent, mBookId, mUserId, mBookImageUrl,
            mBookURLDownload, mBookPrice,
    mContinuationToken, mBookTitle, mBookUri;
    @BindView(R.id.gifImageViewBookStatusLoading)
    GifImageView mImgViewBookLoading;
    @BindView(R.id.imageViewBook)
    ImageView mImgViewBook;
    @BindView(R.id.textViewBookTitle)
    TextView mTxtViewBookTitle;
    @BindView(R.id.textViewBookAuthor)
    TextView mTxtViewBookAuthor;
    @BindView(R.id.buttonBookPrice)
    Button mBtnBookPrice;
    @BindView(R.id.textViewBookDescription)
    TextView mTxtViewBookDescription;

    public BookDetailsFragment() {
        // Required empty public constructor
    }

    public static BookDetailsFragment newInstance() {
        BookDetailsFragment fragment = new BookDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book_details, container, false);
        ButterKnife.bind(this, view);
        mContext = getContext();
        mSharedPreferences = new DataDownloader(mContext);
        mPresenter = new BookDetailsPresenterImpl(mContext,this);
        mFragmentManager = getActivity().getSupportFragmentManager();
        mBundle = getArguments();
        mUserPurchase = new ArrayList<>();
        mOwnedSkus = new ArrayList<>();
        mPurchaseDataList = new ArrayList<>();
        mSignatureList = new ArrayList<>();
        mInternet = new NetworkConnection(mContext);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.initAnalytics();
        mPresenter.initBindService();
        mPresenter.onSuccess();
        mPresenter.onCreate();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mService != null) {
            mContext.unbindService(mServiceConn);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onComplete != null) {
            mGifDrawable.stop();
            mContext.unregisterReceiver(onComplete);
        }
    }

    @Override
    public void checkBookStatus() {
        QueryBookInfoAsyncTask queryBookInfoAsyncTask = new QueryBookInfoAsyncTask(mContext);
        queryBookInfoAsyncTask.setOnCallBack(new QueryBookInfoAsyncTask.CallBackTask() {
            @Override
            public void callBack(List<BookInfo> result) {
                if (result.size() != mContext.getResources().getInteger(R.integer.empty_array)) {
                    for (int i = mContext.getResources().getInteger(R.integer.initial_value);
                         i < result.size(); i++) {
                        if (result.get(i).getUserId().equals(mUserId) &&
                                result.get(i).getTitle().equals(mTxtViewBookTitle.getText())) {
                            initBookPrice(result.get(i).getPid(),
                                    result.get(i).getTitle(),
                                    result.get(i).getUrlDownload());
                        }
                    }
                }
                mPresenter.validateLocalFile(mBookId,mUserId);
                mBtnBookPrice.setVisibility(View.VISIBLE);
            }
        });
        queryBookInfoAsyncTask.execute();
    }

    private void initBookPrice(int pid, String title, String urlDownload) {
        mBookPid = pid;
        mBookTitle = title;
        mBookUri = urlDownload;
    }

    private void initBookStatus(String status) {
        if (mMainView != null) {
            mMainView.onTapped(status);
        } else {
            mSearchView.onTapped(status);
        }
    }

    @Override
    public void setUpListeners() {
        mBtnBookPrice.setOnClickListener(this);
    }

    @Override
    public void onBindService() {
        mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                mService = IInAppBillingService.Stub.asInterface(service);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.initBindService();
        mPresenter.onSuccess();
    }

    @Override
    public void getContentDetails() {
        mUserId = mSharedPreferences.getUserId();
        mEvent = mBundle.getString(getString(R.string.book_content));
        mFreeBookDetails = mBundle.getParcelable(getString(R.string.params_recommended_books_content));
        mPresenter.onDataLoad();
    }

    @Override
    public void setBookDetails(String book_title, String book_id) {
        if (hasUserReDownload(book_title, book_id)) {
            mPresenter.setUpButtonPrice(getString(R.string.download), R.drawable.shape_rectangle_blue_rounded_corner, true);
//            initBookStatus(getString(R.string.alert_error_occured_please_try_other_books));
//            mPresenter.setUpButtonPrice(getString(R.string.downloading), R.drawable.shape_rectangle_gray_rounded_corner, false);
        } else if(!hasUserReDownload(book_title, book_id)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSharedPreferences.setBookDownloaded("");
                }
            }, mContext.getResources().getInteger(R.integer.longest_relay));
//            initBookStatus(getString(R.string.alert_error_occured_please_try_other_books));
            mBtnBookPrice.setVisibility(View.INVISIBLE);
            mPresenter.onReDownload();
        } else {
            mBtnBookPrice.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBookConfirmationDone() {
        mBtnBookPrice.setVisibility(View.VISIBLE);
    }

    @Override
    public void setFreeBookDetails() {
        if (mFreeBookDetails != null) {
            mBookId = mFreeBookDetails.getId();
            setUpBookImage();
            mTxtViewBookTitle.setText(mFreeBookDetails.getTitle());
            mTxtViewBookAuthor.setText(String.format(getString(R.string.written_by), mFreeBookDetails.getAuthor()));
            setUpBookDetails(mFreeBookDetails.getDescription());
            mBookImageUrl = mFreeBookDetails.getImage_url();
            mBookURLDownload = mFreeBookDetails.getBook_url();
            mBookPrice = mFreeBookDetails.getPrice();
            mPresenter.setUpButtonPrice(mBookPrice,
                    R.drawable.shape_rectangle_blue_rounded_corner, true);
            mLogger.logBookDetail(mBookId,mTxtViewBookTitle.getText().toString(),
                    mEvent,getPrice(mBookPrice));
        }
    }

    private boolean hasUserReDownload(String book_title, String book_id) {
        if (book_title.equals(mTxtViewBookTitle.getText()) &&
                book_id.equals(mBookId) && !mSharedPreferences.getBookDownloaded().equals(mBookId)) {
            return true;
        } else if (book_title.equals(mTxtViewBookTitle.getText()) &&
                book_id.equals(mBookId) && mSharedPreferences.getBookDownloaded().equals(mBookId)) {
            return false;
        }
        return false;
    }

    private Double getPrice(String price) {
        if (price.equals(getString(R.string.buy))) {
            return 15d;
        } else {
            return 0d;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBookPrice:
                mPresenter.onDownloadTapped();
                break;
        }
    }

    @Override
    public void initBookPurchase() {
        if (mBtnBookPrice.getText().equals(getString(R.string.download_free_books)) &&
                hasNetworkConnection()) {
            mPresenter.initBookStatus();
            mLogger.logBookDownload();
        } else if (mBtnBookPrice.getText().equals(getString(R.string.download)) &&
                hasNetworkConnection()) {
            mPresenter.initBookStatus();
            mLogger.logBookDownload();
        } else if (mBtnBookPrice.getText().equals(getString(R.string.view_book))) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    PDFViewer view = new PDFViewer(mContext);
                    mLogger.logBookShare(getString(R.string.view_book), mBookId);
                    initBookStatus(view.openPDFReader(getParser(mBookUri), getString(R.string.mime_type_pdf)));
//                        openPDFReader(mBookTitle,getParser(mBookUri));
                }
            }, mContext.getResources().getInteger(R.integer.normal_relay));
//                view.openPDFReader(getParser(mBookURLDownload), getString(R.string.mime_type_pdf));
        } else {
            mPresenter.onCheckPurchaseHistory();

            if (mUserPurchase.size() == mContext.getResources().getInteger(R.integer.empty_array)) {
                mPresenter.onPurchaseTapped();
            } else {
                for (int a = mContext.getResources().getInteger(R.integer.initial_value);
                     a < mUserPurchase.size(); a++) {
                    Log.d("downloaded", mUserPurchase.get(a).getPurchaseToken() + " ");
                    mPresenter.initConsumePurchase(mUserPurchase.get(a).getPurchaseToken());
                }
            }

//            PaymentKit.launchPayment(getActivity(),getString(R.string.secret_key),
//                    getString(R.string.unique_product_key), UUID.randomUUID().toString(),
//                    getString(R.string.amount_code));
        }
    }

    private void onDownloadComplete() {
        BroadcastReceiver callback =  new CallbackManager();
        IntentFilter intentCallback = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mContext.registerReceiver(callback,intentCallback);
    }

    private void openPDFReader(String bookTitle, Uri urlBook) {
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.params_book_title),bookTitle);
        bundle.putParcelable(getString(R.string.params_book_url_download), urlBook);
        ViewPdfFragment viewMyBooks = new ViewPdfFragment();
        viewMyBooks.setArguments(bundle);
        if (hasUserSearch()) {
            mFragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in,0)
                    .addToBackStack(getString(R.string.title_activity_search))
                    .replace(R.id.frameLayoutBookSearch, viewMyBooks)
                    .commit();
        } else {
            mFragmentManager.beginTransaction().addToBackStack(getString(R.string.title_activity_main))
                    .replace(R.id.flContent, viewMyBooks)
                    .commit();
        }
    }

    private boolean hasUserSearch() {
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            int index = mFragmentManager.getBackStackEntryCount() - 1;
            FragmentManager.BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(index);
            String tag = backEntry.getName();
            return tag.equals(getString(R.string.title_activity_search));
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!getFragment().equals(getString(R.string.title_activity_search))) {
            mMainView = (MainView) getContext();
        } else {
            mSearchView = (SearchView) getContext();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == mContext.getResources().getInteger(R.integer.request_code)) {
            int responseCode = data.getIntExtra(getString(R.string.params_response_code), mContext.getResources().
                    getInteger(R.integer.response_default_value));
            String purchaseData = data.getStringExtra(getString(R.string.params_inapp_purchase_data));
            String dataSignature = data.getStringExtra(getString(R.string.params_inapp_data_signature));

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String order_id = jo.getString(getString(R.string.params_order_id));
                    String sku = jo.getString(getString(R.string.params_product_id));
                    String transaction_id = jo.getString(getString(R.string.params_purchase_token));
                    mLogger.logBookPurchase(transaction_id);
                    mAnalytics.sendGAID(getString(R.string.title_fragment_book_details),
                            getString(R.string.log_category_action),getString(R.string.log_action_paid_purchase));
                    Log.d("billingsuccess", "You have bought the " + sku + " Excellent choice, Bibliophile!");
                    mPresenter.onGooglePlayPayment(mUserId,sku,transaction_id,order_id,
                            mBookId,mTxtViewBookTitle.getText(),mBookImageUrl,mBookURLDownload);
                    mPresenter.initBookStatus();
                }
                catch (JSONException e) {
                    Log.d("billingfailed","Failed to parse purchase data.");
                    e.printStackTrace();
                }
                mPresenter.onCheckPurchaseHistory();
            }
        }
    }

    private Uri getParser (String bookUri) {
        Uri uri = Uri.parse(bookUri);
        File file = new File(uri.getPath());
        return FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID, file);
    }

    @Override
    public void insertBookDetails(String bookId, CharSequence bookTitle, String bookImageUrl, String bookURLDownload, Date date) {
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        onComplete = new ServiceDownloader.MyRequestReceiver();
        mContext.registerReceiver(onComplete, filter);
    }

    @Override
    public void removeDownloadQueue() {
        DownloadManager downloadManager = (DownloadManager) mContext
                .getSystemService(Context.DOWNLOAD_SERVICE);
        assert downloadManager != null;
        downloadManager.remove(mSharedPreferences.getDownloadedFileID());
    }

    @Override
    public void setBookTransaction() {
        mAnalytics.sendGAID(getString(R.string.title_fragment_book_details), getString(R.string.log_category_action), getString(R.string.log_action_free_purchase));
        Toast.makeText(getActivity(), R.string.alert_download_progress, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity().getApplicationContext(), ServiceDownloader.class);
        intent.putExtra(getString(R.string.params_userId), mUserId);
        intent.putExtra(getString(R.string.params_book_id), mBookId);
        intent.putExtra(getString(R.string.params_book_title), mTxtViewBookTitle.getText());
        intent.putExtra(getString(R.string.params_book_image_url), mBookImageUrl);
        intent.putExtra(getString(R.string.params_book_url_download), mBookURLDownload);
        intent.putExtra(getString(R.string.params_book_price), mBookPrice);
        getActivity().startService(intent);

        mSharedPreferences.setBookDownloaded(mBookId);
//        mPresenter.setUpButtonPrice(getString(R.string.downloading),R.drawable.shape_rectangle_gray_rounded_corner,false);
        mBtnBookPrice.setVisibility(View.INVISIBLE);

        mPresenter.onTransactionDone(mBookId, mTxtViewBookTitle.getText()
                , mBookImageUrl, mBookURLDownload, new Date(), mBookPrice);

        onDownloadComplete();

    }

    @Override
    public void setService() {
        Intent mServiceIntent = new Intent(getString(R.string.action_inapp_billing_service_bind));
        mServiceIntent.setPackage(getString(R.string.params_vending));
        mContext.bindService(mServiceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    private void setUpBookImage() {

        Picasso.with(mContext).load(mFreeBookDetails.getImage_url()).fit().into(mImgViewBook, new Callback() {
            @Override
            public void onSuccess() {
                mPresenter.onCheckUserTransaction();
            }

            @Override
            public void onError() {
                mImgViewBook.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void setPurchases() {
        try {

            Bundle bundle = mService.getBuyIntent(mContext.getResources().
                            getInteger(R.integer.api_version), mContext.getPackageName(),
                    getString(R.string.item_product_id_eshopbok)
                    , getString(R.string.params_inapp_billing), "");

            PendingIntent pendingIntent = bundle.getParcelable(RESPONSE_BUY_INTENT);
            if (bundle.getInt(RESPONSE_CODE) == BILLING_RESPONSE_RESULT_OK) {

                // Start purchase flow (this brings up the Google Play UI).
                // Result will be delivered through onActivityResult().
                try {
                    startIntentSenderForResult(pendingIntent.getIntentSender(), mContext.getResources()
                                    .getInteger(R.integer.request_code), new Intent(),
                            getResources().getInteger(R.integer.initial_value),
                            getResources().getInteger(R.integer.initial_value),
                            getResources().getInteger(R.integer.initial_value),null);
                } catch (Exception e) {
                    Log.d("error", e.getMessage());
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void consumePurchases(String purchaseToken) {
        try {
            int response = mService.consumePurchase(mContext.getResources()
                    .getInteger(R.integer.api_version), mContext.getPackageName(), purchaseToken);

            if (response == BILLING_RESPONSE_RESULT_OK) {
                Log.d("consumesuccess", "Your a Bibliophile!");
//                Toast.makeText(mContext,"consume success!",Toast.LENGTH_SHORT).show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getPurchases() {
        try {
            mOwnedBooks = mService.getPurchases(mContext.getResources().
                            getInteger(R.integer.api_version), mContext.getPackageName(),
                    getString(R.string.params_inapp_billing), null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        int response = mOwnedBooks.getInt(getString(R.string.params_response_code));
        if (response == BILLING_RESPONSE_RESULT_OK) {
            mOwnedSkus =
                    mOwnedBooks.getStringArrayList(getString(R.string.params_inapp_purchase_item_list));
            mPurchaseDataList =
                    mOwnedBooks.getStringArrayList(getString(R.string.params_inapp_purchase_data_list));
            mSignatureList =
                    mOwnedBooks.getStringArrayList(getString(R.string.params_inapp_data_signature_list));
            mContinuationToken =
                    mOwnedBooks.getString(getString(R.string.params_inapp_continuation_token));

            for (int i = 0; i < mPurchaseDataList.size(); ++i) {
                String purchaseData = mPurchaseDataList.get(i);
                String signature = mSignatureList.get(i);
                String sku = mOwnedSkus.get(i);

                try {
                    JSONObject jsonObject = new JSONObject(purchaseData);
                    UserPurchase mBookTransaction = new UserPurchase(
                            jsonObject.getString(getString(R.string.params_order_id)),
                            jsonObject.getString(getString(R.string.params_package_name)),
                            jsonObject.getString(getString(R.string.params_product_id)),
                            jsonObject.getString(getString(R.string.params_purchase_time)),
                            jsonObject.getString(getString(R.string.params_purchase_state)),
                            getString(R.string.params_developer_payload),
                            jsonObject.getString(getString(R.string.params_purchase_token)));
                    mPresenter.initConsumePurchase(jsonObject.getString(getString(R.string.params_purchase_token)));
                    mUserPurchase.add(mBookTransaction);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // do something with this purchase information
                // e.g. display the updated list of products owned by user
            }

            // if continuationToken != null, call getPurchases again
            // and pass in the token to retrieve more items
        }
    }

    @Override
    public void setBookConfirmation() {
        mGifDrawable = (GifDrawable) mImgViewBookLoading.getDrawable();
    }

    @Override
    public void goAnalytics() {
        mLogger = new Logger(mContext);
        mAnalytics = new Gaid(mContext);
    }

    private void setUpBookDetails(String description) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mTxtViewBookDescription.setText(Html.fromHtml(description,Html.FROM_HTML_MODE_LEGACY));
        } else {
            mTxtViewBookDescription.setText(Html.fromHtml(description));
        }
    }

    private String getFragment() {
        mFragmentManager = getActivity().getSupportFragmentManager();
        int index = mFragmentManager.getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(index);
        return backEntry.getName();
    }

    private boolean hasNetworkConnection() {
        if (mSharedPreferences.getDownloadedFileID() > 0 &&
                mInternet.hasActiveConnection(getString(R.string.url_digitalshop2))) {
            mPresenter.onReDownload();
            return true;
        } else if (!mInternet.hasActiveConnection(getString(R.string.url_digitalshop2))) {
            mPresenter.onCheckPurchaseHistory();
            return false;
        }
        return true;
    }

    @Override
    public void initButtonPrice(String status, int color, boolean function) {
        mBtnBookPrice.setText(status);
        mBtnBookPrice.setBackgroundResource(color);
        mBtnBookPrice.setClickable(function);
    }

    private class CallbackManager extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction())) {
                checkBookStatus();
                mPresenter.validateLocalFile(mBookId,mUserId);
            }
        }
    }

    @Override
    public void onValidationSuccessful(String filePath, String id, String userId) {
        File localFilePath = new File(filePath+id+userId);
        if (localFilePath.exists()) {
            mBookUri = localFilePath.getAbsolutePath();
            mPresenter.setUpButtonPrice(getString(R.string.view_book),R.drawable.shape_rectangle_blue_rounded_corner,true);
        } else {
            mPresenter.onMiss(mBookPid);
//            initBookStatus(getString(R.string.alert_error_occured_please_try_other_books));
            boolean delete = localFilePath.delete();
            Log.w("Delete Check", "File deleted: " + delete);
        }
    }
}
