package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.requestParameters.PaymentParams;
import ph.com.cdu.apptwo.data.rest.response.PaymentResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public class PaymentInteractorImpl implements PaymentInteractor {

    private CDUService mService;

    @Override
    public void onRequestPayment(final onPaymentDataLoadedListener listener, Context context, PaymentParams paymentParams) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_GOOGLE_PAYMENT);

        mService.payNewBook(paymentParams.mapParameters(context)).enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
