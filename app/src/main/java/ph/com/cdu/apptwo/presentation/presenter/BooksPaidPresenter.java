package ph.com.cdu.apptwo.presentation.presenter;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksPaidPresenter {

    void onCreate();

    void initPaidBooks();

    void initLinearLayoutManagerDeck();

    void initLoadPaidBooksContent();

    void onFinish();

    void onConnected();
}
