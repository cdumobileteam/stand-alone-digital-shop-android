package ph.com.cdu.apptwo.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.UserInfo;

/**
 * Created by Neil Cruz on 24/10/2017.
 */
@Dao
public interface UserInfoDao {
    String TABLE_NAME = "userinfo";

    @Query("SELECT * FROM " + TABLE_NAME)
    List<UserInfo> selectAllUserInfo();

    @Query("SELECT * from " + TABLE_NAME + " WHERE  user_id = :userId")
    UserInfo selectUserInfoById(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertUserInfo(UserInfo userInfo);

    @Query("DELETE FROM " + TABLE_NAME)
    void deleteUserInfo();

}
