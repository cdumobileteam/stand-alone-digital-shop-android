package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;

/**
 * Created by Patrick Santos on 08/02/2018.
 */

public class InsertUserInfoAsyncTask extends AsyncTask<UserInfo, Void, Long> {
    private Context mContext;
    private String mUserId, mUserEmail, mUserFname, mUserLname, mGender, mProfilePic, mSocialAccount;
    private UserInfo mUserDetails;

    public InsertUserInfoAsyncTask(Context mContext, String userId,
                                   String userEmail, String userFname,
                                   String userLname, String gender,
                                   String profilePic, String socialAccount) {
        mUserDetails = new UserInfo();
        this.mContext = mContext;
        this.mUserId = userId;
        this.mUserEmail = userEmail;
        this.mUserFname = userFname;
        this.mUserLname = userLname;
        this.mGender = gender;
        this.mProfilePic = profilePic;
        this.mSocialAccount = socialAccount;
    }

    @Override
    protected Long doInBackground(UserInfo... userInfos) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        mUserDetails.setId(mUserId);
        mUserDetails.setEmail(mUserEmail);
        mUserDetails.setFirstName(mUserFname);
        mUserDetails.setLastName(mUserLname);
        mUserDetails.setGender(mGender);
        mUserDetails.setProfilePicUri(mProfilePic);
        mUserDetails.setSocialAccount(mSocialAccount);
        return databaseInteractor.insertUserInfo(mContext, mUserDetails);
    }

    private interface  CallBackTask {

    }
}
