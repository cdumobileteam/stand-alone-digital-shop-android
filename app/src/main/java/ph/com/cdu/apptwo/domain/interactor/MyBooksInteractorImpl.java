package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.requestParameters.MyBooksParams;
import ph.com.cdu.apptwo.data.rest.response.MyBooksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public class MyBooksInteractorImpl implements MyBooksInteractor {

    private CDUService mService;

    @Override
    public void onRequestMyBooks(final onMyBooksOnDataListener listener, Context context, MyBooksParams myBooksParams) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_BOOKS);

        mService.getMyBooksContent(myBooksParams.mapParameters(context)).enqueue(new Callback<MyBooksResponse>() {
            @Override
            public void onResponse(Call<MyBooksResponse> call, Response<MyBooksResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<MyBooksResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
