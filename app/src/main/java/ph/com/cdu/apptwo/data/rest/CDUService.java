package ph.com.cdu.apptwo.data.rest;

import java.util.Map;

import ph.com.cdu.apptwo.data.rest.response.GooglePlaymentResponse;
import ph.com.cdu.apptwo.data.rest.response.NewUserResponse;
import ph.com.cdu.apptwo.data.rest.response.PaidBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.PaymentResponse;
import ph.com.cdu.apptwo.data.rest.response.TopSellingBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.UserAccountsResponse;
import ph.com.cdu.apptwo.data.rest.response.FreeBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.MyBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.NewBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.PurchaseNewBookResponse;
import ph.com.cdu.apptwo.data.rest.response.RecommendedBooksResponse;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface CDUService {

    @GET("get_free.php")
    Call<FreeBooksResponse> getFreeBooksContent();

    @GET("get_paid.php")
    Call<PaidBooksResponse> getPaidBooksContent();

    @GET("get_topselling.php")
    Call<TopSellingBooksResponse> getTopSellingBooksContent();

    @GET("get_new.php")
    Call<NewBooksResponse> getNewBooksContent();

    @GET("get_recommended.php")
    Call<RecommendedBooksResponse> getRecommendedBooksContent();

    @POST("insert_book_transactions.php")
    Call<PurchaseNewBookResponse> purchaseNewBook(@QueryMap(encoded = true) Map<String, Object> purchaseNewBook);

    @POST("insert_payment.php")
    Call<PaymentResponse> payNewBook(@QueryMap(encoded = true) Map<String, Object> payNewBook);

    @GET("get_payment.php")
    Call<GooglePlaymentResponse> getGooglePlayReceipt(@QueryMap Map<String, Object> getGooglePlayPayment);

    @GET("get_all_books.php")
    Call<MyBooksResponse> getMyBooksContent(@QueryMap(encoded = true) Map<String, Object> myBooksParams);

    @GET("get_digitalshop_books.php")
    Call<FreeBooksResponse> getAllDigitalShopBooks();

    @POST("insert_login.php")
    Call<NewUserResponse> insertNewUser(@QueryMap(encoded = true) Map<String, Object> newUserParams);

    @GET("get_registered_users.php")
    Call<UserAccountsResponse> getUserAccounts();
}
