package ph.com.cdu.apptwo.presentation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;
import ph.com.cdu.apptwo.presentation.activity.view.SplashScreenView;
import ph.com.cdu.apptwo.presentation.asynctask.DatabaseSetUpAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.QueryBookDirectoryAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.QueryUserInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.presenter.SplashScreenPresenterImpl;
import ph.com.cdu.apptwo.util.Gaid;
import ph.com.cdu.apptwo.util.Logger;
import ph.com.cdu.apptwo.util.NetworkConnection;
import ph.com.cdu.apptwo.presentation.presenter.SplashScreenPresenter;

public class SplashScreenActivity extends AppCompatActivity implements SplashScreenView, View.OnClickListener, FacebookCallback<LoginResult>, GoogleApiClient.OnConnectionFailedListener {

    private Context mContext;
    private SplashScreenPresenter mPresenter;
    private CallbackManager mCallBackManager;
    private NetworkConnection mInternet;
    private Animation mFadeOut;
    private Snackbar mSnackbar;
    private Logger mLogger;
    private Gaid mAnalytics;
    @BindView(R.id.linearLayoutSocialPlugin)
    LinearLayout mLnrLayoutSocialPlugin;
    @BindView(R.id.facebook_sign_in_button)
    ImageButton mLoginFacebook;
    private GoogleSignInOptions mGoogleSigninOptions;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInAccount mGoogleAccount;
    private GoogleSignInResult mBasicInfo;
    private Person mOtherInfo;
    private OptionalPendingResult<GoogleSignInResult> mOptionalPendingResult;
    private ProgressDialog mProgressDialog;
    @BindView(R.id.google_sign_in_button)
    ImageButton mLoginGooglePlus;
    private String mSignUpMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new SplashScreenPresenterImpl(this,this,this);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        mContext = this;
        mInternet = new NetworkConnection(mContext);
        mLogger = new Logger(mContext);
        mFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        DatabaseSetUpAsyncTask databaseSetUpAsyncTask = new DatabaseSetUpAsyncTask(mContext);
        databaseSetUpAsyncTask.execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.initAnalytics();
        mPresenter.onCreate();
        mPresenter.onSocialPluginLaunch();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.facebook_sign_in_button:
                mPresenter.onTappedLoginFacebook();
                break;
            case R.id.google_sign_in_button:
                mPresenter.onTappedLoginGooglePlus();
                break;
        }
    }

    private void updateUI(boolean coldSignedIn, boolean signedInGooglePlus, boolean signedInFacebook) {
        if (coldSignedIn) {
            mLnrLayoutSocialPlugin.setAnimation(mFadeOut);
            mLnrLayoutSocialPlugin.setVisibility(View.GONE);
        } else if (signedInGooglePlus) {
            setButtonClickable(true, false);
        } else if (signedInFacebook) {
            setButtonClickable(false, true);
        } else {
            setButtonClickable(true, true);
            mLnrLayoutSocialPlugin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNewUserAccount(int responseCode) {
        if (responseCode == 0) {
            mLogger.logSignUp(mSignUpMethod);
        } else if (responseCode == 1) {
            mLogger.logSignIn(mSignUpMethod);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == getResources().getInteger(R.integer.rc_sign_in)) {
            mBasicInfo = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            mPresenter.onSuccessLogin(mGoogleApiClient);
            handleSignInResult(mBasicInfo, mOtherInfo);
        } else {
            mCallBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void validateLogin() {
        Bundle bundle = getIntent().getExtras();
        mOptionalPendingResult = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (mOptionalPendingResult.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(getString(R.string.title_activity_splash_screen), "Got cached sign-in");
            if (bundle != null) {
                if (bundle.getBoolean(getString(R.string.has_user_logout))) {
                    return;
                }
            } else {
                mBasicInfo = mOptionalPendingResult.get();
                handleSignInResult(mBasicInfo, mOtherInfo);
            }
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            mOptionalPendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult, mOtherInfo);
                    updateUI(false, false, false);
                }
            });
        }
        if (AccessToken.getCurrentAccessToken() != null) {
//            mPresenter.onTappedLoginFacebook();
            mPresenter.onColdSignedIn();
        } else {
            updateUI(false, false, false);
        }
    }

    @Override
    public void initUserAccount() {
        QueryUserInfoAsyncTask queryUserInfoAsyncTask = new QueryUserInfoAsyncTask(mContext);
        queryUserInfoAsyncTask.setOnCallBack(new QueryUserInfoAsyncTask.CallBackTask() {
            @Override
            public void callback(List<UserInfo> result) {
                if (result.size() != mContext.getResources().getInteger(R.integer.empty_array)) {
                    for (int i = mContext.getResources().getInteger(R.integer.initial_value);
                         i < result.size();i++){
                        mSignUpMethod = result.get(i).getSocialAccount();
                    }
                } else {
                    mSignUpMethod = "";
                }
            }
        });
    }

    private void handleSignInResult(GoogleSignInResult result, Person personalInfo) {
        Log.d(getString(R.string.title_activity_splash_screen), "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
//            Toast.makeText(getApplicationContext(), R.string.alert_login_success, Toast.LENGTH_SHORT).show();
            // Signed in successfully, show authenticated UI.
            mGoogleAccount = result.getSignInAccount();
            mPresenter.setGooglePlusData(mGoogleAccount, personalInfo);
            updateUI(true, true,false);
        } else if (result.getStatus().isCanceled()) {
            Toast.makeText(getApplicationContext(), R.string.alert_login_cancelled, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setOtherInfo(Person advanceInfo) {
        mOtherInfo = advanceInfo;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        updateUI(true,false,false);
        mPresenter.setFacebookData(loginResult);
//        Toast.makeText(getApplicationContext(), R.string.alert_login_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancel() {
        Toast.makeText(getApplicationContext(), R.string.alert_login_cancelled, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(getApplicationContext(), getString(R.string.alert_login_error) + error.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setUserInfo(String socialApp, String userId, String email, String firstName, String lastName, String gender, String profilePicUri) {
        mAnalytics.sendGAID(getString(R.string.title_activity_splash_screen),getString(R.string.log_category_action),getString(R.string.log_action_view));
        final Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(getString(R.string.params_social), socialApp);
        intent.putExtra(getString(R.string.params_userId), userId);
        intent.putExtra(getString(R.string.params_email), email);
        intent.putExtra(getString(R.string.params_firstName), firstName);
        intent.putExtra(getString(R.string.params_lastName), lastName);
        intent.putExtra(getString(R.string.params_gender), gender);
        intent.putExtra(getString(R.string.params_profilePic), profilePicUri);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, mContext.getResources().getInteger(R.integer.longer_relay));
    }

    @Override
    public void getCshop() {
        QueryBookDirectoryAsyncTask queryBookDirectoryAsyncTask = new QueryBookDirectoryAsyncTask(mContext);
        queryBookDirectoryAsyncTask.setOnCallBack(new QueryBookDirectoryAsyncTask.CallBackTask() {
            @Override
            public void callback(List<BookShop> bookShops) {
                if (bookShops.size() != 0 && hasNetwork(getString(R.string.url_digitalshop2))) {
                    mPresenter.onReCreate();
                }
                    mPresenter.setCShop();
            }
        });
        queryBookDirectoryAsyncTask.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideProgressDialog();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void setUpListeners() {
        mLoginFacebook.setOnClickListener(this);
        mLoginGooglePlus.setOnClickListener(this);
        mSnackbar.setAction(R.string.button_retry, this);
    }

    private void setButtonClickable(boolean googlePlus, boolean facebook) {
        mLoginGooglePlus.setClickable(googlePlus);
        mLoginFacebook.setClickable(facebook);
    }

    @Override
    public void initFacebookSdk() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallBackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallBackManager, this);
    }

    @Override
    public void initGoogleSdk() {
        mGoogleSigninOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        try {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Plus.API)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSigninOptions)
                    .build();
        } catch (Exception e) {
            Log.e("GoogleApiClient", e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.blink_in,R.anim.blink_out);
    }

    @Override
    public void setUpSnackBars() {
        mSnackbar = Snackbar.make(findViewById(android.R.id.content),
                R.string.alert_no_connection,
                Snackbar.LENGTH_LONG).setActionTextColor(Color.RED);
    }

    @Override
    public void checkGooglePlusStatus() {
        if(hasNetwork(getString(R.string.url_google))) {
            mPresenter.onLoginGooglePlus(mGoogleApiClient);
        }
    }

    private boolean hasNetwork(String url) {
        if(mInternet.hasActiveConnection(url)){
            return true;
        }else {
            mSnackbar.show();
        }
        return false;
    }

    @Override
    public void checkFacebookStatus() {
        if (hasNetwork(getString(R.string.url_facebook))) {
            mPresenter.onLoginFacebook();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(getString(R.string.title_activity_splash_screen), "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onLoadRecommendedBooksContent(List<FreeBooks> recommended) {
        mPresenter.storeRecommendedBooks(recommended);
    }

    @Override
    public void onLoadFreeBooksContent(List<FreeBooks> free) {
        mPresenter.storeFreeBooks(free);
    }

    @Override
    public void onLoadPaidBooksContent(List<FreeBooks> paid) {
        mPresenter.storePaidBooks(paid);
    }

    @Override
    public void onLoadTopSellingContent(List<FreeBooks> top) {
        mPresenter.storeTopSellingBooks(top);
    }

    @Override
    public void onLoadNewContent(List<FreeBooks> latest) {
        mPresenter.storeNewBooks(latest);
    }

    @Override
    public void goAnalytics() {
        mAnalytics = new Gaid(mContext);
    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
        updateUI(false, false, false);
    }
}
