package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public class GetBookTitleAsyncTask extends AsyncTask<String, Void, List<String>> {
    private Context mContext;
    private CallBackTask mCallBackTask;

    public GetBookTitleAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<String> doInBackground(String... strings) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        return databaseInteractor.getBooksByTitle(mContext,strings[0]);
    }

    @Override
    protected void onPostExecute(List<String> bookShops) {
        super.onPostExecute(bookShops);
        mCallBackTask.callBack(bookShops);
    }

    public void setmCallBackTask(CallBackTask mCallBackTask) {
        this.mCallBackTask = mCallBackTask;
    }

    public interface CallBackTask {
        void callBack(List<String> bookShops);
    }
}
