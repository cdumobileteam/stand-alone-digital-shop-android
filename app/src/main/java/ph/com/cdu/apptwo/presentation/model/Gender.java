package ph.com.cdu.apptwo.presentation.model;

/**
 * Created by Neil Cruz on 19/10/2017.
 */

public enum Gender {
    FEMALE(1), MALE(0);

    private int code;

    Gender(int genderCd) {
        code = genderCd;
    }

    public int getGenderCd() {
        return code;
    }
}
