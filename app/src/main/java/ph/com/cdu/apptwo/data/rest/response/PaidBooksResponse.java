package ph.com.cdu.apptwo.data.rest.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.books.PaidBooks;

/**
 * Created by Neil Cruz on 13/10/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaidBooksResponse {
    private List<FreeBooks> result;

    public List<FreeBooks> getResult() {
        return result;
    }

    public void setResult(List<FreeBooks> result) {
        this.result = result;
    }
}
