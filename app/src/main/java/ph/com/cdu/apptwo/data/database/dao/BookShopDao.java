package ph.com.cdu.apptwo.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Patrick Santos on 09/02/2018.
 */
@Dao
public interface BookShopDao {
    String TABLE_NAME = "bookdirectory";

    @Query("SELECT * FROM " + TABLE_NAME)
    List<BookShop> selectAllBooks();

    @Query("SELECT * from " + TABLE_NAME + " WHERE  book_category = :bookPrice")
    List<BookShop> selectBookByCategory(String bookPrice);

    @Query("SELECT DISTINCT book_title from " + TABLE_NAME + " WHERE book_title LIKE :bookTitle")
    List<String> selectBookByTitle(String bookTitle);

    @Query("SELECT * from " + TABLE_NAME + " WHERE book_title LIKE :bookTitle")
    List<BookShop> selectBookInfo(String bookTitle);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertBook(BookShop bookShop);

    @Query("DELETE FROM " + TABLE_NAME)
    void deleteAllBooks();
}
