package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.response.FreeBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.AllBooksInteractor;
import ph.com.cdu.apptwo.domain.interactor.AllBooksInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.presentation.activity.view.SearchView;
import ph.com.cdu.apptwo.util.NetworkConnection;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public class SearchPresenterImpl implements SearchPresenter, AllBooksInteractor.onAllBooksDataLoadedListener {

    private Context mContext;
    private NetworkConnection mInternet;
    private SearchView mSearchView;
    private AllBooksInteractor mAllBooksInteractor;
    String search;

    public SearchPresenterImpl(Context context, SearchView searchView) {
        this.mContext = context;
        this.mSearchView = searchView;
        mAllBooksInteractor = new AllBooksInteractorImpl();
        mInternet = new NetworkConnection(mContext);
    }

    @Override
    public void onCreate() {
        mSearchView.setUpSearchParams();
        mSearchView.setUpSnackBar();
    }

    @Override
    public void onTappedSearch(String bookTitle) {
        if (hasNetworkConnection()) {
            mSearchView.initSearchResults(View.VISIBLE,View.VISIBLE,View.GONE,View.VISIBLE,View.VISIBLE);
            checkSearchValue(bookTitle);
        }
    }

    private void checkSearchValue(String bookTitle) {
        if (bookTitle.length() > 0) {
            mSearchView.initSearchView(bookTitle);
        } else {
            mSearchView.showMessage();
            Toast.makeText(mContext, "Please enter your search", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onTappedButton(String bundle, Fragment fragment) {
        mAllBooksInteractor.requestAllBooks(this,mContext);
        search = bundle;
    }

    @Override
    public void onTappedBook(Bundle bundle, Fragment fragment) {
        mSearchView.makeArguments(bundle, fragment);
    }

    @Override
    public void onTappedTitle(List<BookShop> bookTitle) {
        for (int i=0;i<bookTitle.size();i++){
            Log.d("values", bookTitle.get(i).getTitle() + " "+bookTitle.get(i).getCategory());
        }
//        GetBookInfoAsyncTask getBookInfoAsyncTask = new GetBookInfoAsyncTask(mContext);
//        getBookInfoAsyncTask.setmCallBackTask(new GetBookInfoAsyncTask.CallBackTask() {
//            @Override
//            public void callback(List<BookShop> bookShops) {
//                checkBookCategory(bookShops);
//            }
//        });
//        getBookInfoAsyncTask.execute(bookTitle);
    }

    private void checkBookCategory(List<BookShop> bookShops) {
        for (int i =0;i<bookShops.size();i++){
            if (bookShops.get(i).getCategory().equals(mContext.getString(R.string.params_free_books_content))){
//                mSearchView.setUpBookDetails(bookShops);
                break;
            } else if (bookShops.get(i).getCategory().equals(mContext.getString(R.string.params_paid_books_content))) {
                Toast.makeText(mContext,bookShops.get(i).getId() + " " + bookShops.get(i).getCategory(),Toast.LENGTH_SHORT).show();
                break;
            } else if (bookShops.get(i).getCategory().equals(mContext.getString(R.string.params_topselling_books_content))) {
                Toast.makeText(mContext,bookShops.get(i).getId() + " " + bookShops.get(i).getCategory(),Toast.LENGTH_SHORT).show();
                break;
            } else if (bookShops.get(i).getCategory().equals(mContext.getString(R.string.params_new_books_content))) {
                Toast.makeText(mContext,bookShops.get(i).getId() + " " + bookShops.get(i).getCategory(),Toast.LENGTH_SHORT).show();
                break;
            } else if (bookShops.get(i).getCategory().equals(mContext.getString(R.string.params_recommended_books_content))) {
                Toast.makeText(mContext,bookShops.get(i).getId() + " " + bookShops.get(i).getCategory(),Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }

    @Override
    public void setUpFind(CharSequence message) {
        mSearchView.showFindResult(message);
    }

    @Override
    public void onSuccess(FreeBooksResponse booksResponse) {
        for (int i=0;i<booksResponse.getResult().size();i++){
            String title = booksResponse.getResult().get(i).getTitle().toLowerCase();
            String noSpace = title.replaceAll("\\s+","");
            String keyword = search.toLowerCase();
            String newKeyword = keyword.replaceAll("\\s+","");
            if (noSpace.matches(".*"+newKeyword+".*")) {
                mSearchView.setUpBookDetails(booksResponse.getResult().get(i), search);
            } else {
                mSearchView.showFindResult(mContext.getString(R.string.text_no_books_found));
            }
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onTappedBack() {
        mSearchView.initBackButton();
    }

    private boolean hasNetworkConnection() {
        if (!mInternet.hasActiveConnection(mContext.getString(R.string.url_digitalshop2))) {
            mSearchView.showMessageOk(mContext.getString(R.string.alert_no_connection),null);
//            mSearchView.goToMain();
            return false;
        }
        return true;
    }

    @Override
    public void initMain() {
        mSearchView.goToMain();
    }

    @Override
    public void initAnalytics() {
        mSearchView.goAnalytics();
    }
}
