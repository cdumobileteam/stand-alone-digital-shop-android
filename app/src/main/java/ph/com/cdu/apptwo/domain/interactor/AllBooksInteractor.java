package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.FreeBooksResponse;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface AllBooksInteractor {
    interface onAllBooksDataLoadedListener {
        void onSuccess(FreeBooksResponse freeBooksData);

        void onFailed();
    }

    void requestAllBooks(onAllBooksDataLoadedListener listener, Context context);
}
