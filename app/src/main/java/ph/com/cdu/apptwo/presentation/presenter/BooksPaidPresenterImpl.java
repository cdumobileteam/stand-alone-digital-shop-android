package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.response.PaidBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.BooksPaidInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksPaidInteractorImpl;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksPaidView;
import ph.com.cdu.apptwo.util.NetworkConnection;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksPaidPresenterImpl implements BooksPaidPresenter, BooksPaidInteractor.onBooksPaidDataLoadedListener {

    private Context mContext;
    private BooksPaidView mBooksPaidView;
    private BooksPaidInteractor mBooksPaidInteractor;

    public BooksPaidPresenterImpl(Context context, BooksPaidView booksPaidView) {
        mContext = context;
        mBooksPaidView = booksPaidView;
        mBooksPaidInteractor = new BooksPaidInteractorImpl();
    }

    @Override
    public void onCreate() {
        mBooksPaidView.setUpListeners();
        mBooksPaidView.setUpRecyclerViewDeck();
    }

    @Override
    public void onConnected() {
        mBooksPaidView.getBooks();
    }

    @Override
    public void initPaidBooks() {
        mBooksPaidView.onInitPaidBooksContent();
    }

    @Override
    public void initLinearLayoutManagerDeck() {
        mBooksPaidView.setUpLayoutManagerDeck();
    }

    @Override
    public void onSuccess(PaidBooksResponse paidBooksData) {
        for (int i = mContext.getResources().getInteger(R.integer.initial_value);
             i<paidBooksData.getResult().size(); i++) {
            mBooksPaidView.onStorePaidBookContent(paidBooksData.getResult());
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void initLoadPaidBooksContent() {
        mBooksPaidInteractor.requestPaidBooks(this, mContext);
    }

    @Override
    public void onFinish() {
        mBooksPaidView.setUpBookViews();
    }
}
