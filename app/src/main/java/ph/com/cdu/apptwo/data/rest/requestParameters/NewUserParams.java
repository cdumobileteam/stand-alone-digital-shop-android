package ph.com.cdu.apptwo.data.rest.requestParameters;

import com.fasterxml.jackson.annotation.JsonProperty;

import ph.com.cdu.apptwo.data.rest.BaseParameter;

/**
 * Created by Neil Cruz on 03/11/2017.
 */

public class NewUserParams extends BaseParameter {
    @JsonProperty("account_id")
    private String id;
    @JsonProperty("first_name")
    private String fName;
    @JsonProperty("last_name")
    private String lName;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("email")
    private String email;
    @JsonProperty("account_used")
    private String account_used;
    @JsonProperty("profile_pic_uri")
    private String profile_pic_uri;

    public NewUserParams(String id, String fName, String lName, String gender, String email, String account_used, String profile_pic_uri) {
        this.id = id;
        this.fName = fName;
        this.lName = lName;
        this.gender = gender;
        this.email = email;
        this.account_used = account_used;
        this.profile_pic_uri = profile_pic_uri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount_used() {
        return account_used;
    }

    public void setAccount_used(String account_used) {
        this.account_used = account_used;
    }

    public String getProfile_pic_uri() {
        return profile_pic_uri;
    }

    public void setProfile_pic_uri(String profile_pic_uri) {
        this.profile_pic_uri = profile_pic_uri;
    }
}
