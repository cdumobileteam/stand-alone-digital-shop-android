package ph.com.cdu.apptwo.domain.repository;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.UserInfo;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

public interface UserInfoRepository {
    List<UserInfo> getALLUserInfo();

    boolean isUserSaved(int userId);

    Long insert(UserInfo userInfo);

    void delete();
}
