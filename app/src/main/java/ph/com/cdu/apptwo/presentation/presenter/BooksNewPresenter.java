package ph.com.cdu.apptwo.presentation.presenter;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksNewPresenter {

    void onCreate();

    void initNewBooks();

    void initLinearLayoutManagerDeck();

    void initLoadNewBooksContent();

    void onFinish();

    void onConnected();
}
