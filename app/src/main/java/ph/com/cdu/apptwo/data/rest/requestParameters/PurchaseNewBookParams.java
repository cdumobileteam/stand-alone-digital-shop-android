package ph.com.cdu.apptwo.data.rest.requestParameters;

import com.fasterxml.jackson.annotation.JsonProperty;

import ph.com.cdu.apptwo.data.rest.BaseParameter;

/**
 * Created by Patrick Santos on 19/01/2018.
 */

public class PurchaseNewBookParams extends BaseParameter {
    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("book_id")
    private String bookId;
    @JsonProperty("book_title")
    private String bookTitle;
    @JsonProperty("book_image")
    private String bookImage;
    @JsonProperty("book_url")
    private String bookUrl;
    @JsonProperty("transaction_date")
    private String transactionDate;
    @JsonProperty("price")
    private String bookPrice;

    public PurchaseNewBookParams(String transactionId, String userId, String bookId, String bookTitle, String bookImage, String bookUrl, String transactionDate, String bookPrice) {
        this.transactionId = transactionId;
        this.userId = userId;
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.bookImage = bookImage;
        this.bookUrl = bookUrl;
        this.transactionDate = transactionDate;
        this.bookPrice = bookPrice;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(String bookPrice) {
        this.bookPrice = bookPrice;
    }
}
