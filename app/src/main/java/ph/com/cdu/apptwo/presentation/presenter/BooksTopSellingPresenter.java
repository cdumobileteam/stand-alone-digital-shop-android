package ph.com.cdu.apptwo.presentation.presenter;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksTopSellingPresenter {

    void onCreate();

    void initTopSellingBooks();

    void initLinearLayoutManagerDeck();

    void initLoadTopSellingBooksContent();

    void onFinish();

    void onConnected();
}
