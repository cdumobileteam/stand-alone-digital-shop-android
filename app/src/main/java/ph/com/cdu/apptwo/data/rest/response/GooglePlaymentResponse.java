package ph.com.cdu.apptwo.data.rest.response;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.googlePlay.InappBilling;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public class GooglePlaymentResponse {
    private int response_code;
    private String status;
    private List<InappBilling> result;

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<InappBilling> getResult() {
        return result;
    }

    public void setResult(List<InappBilling> result) {
        this.result = result;
    }
}
