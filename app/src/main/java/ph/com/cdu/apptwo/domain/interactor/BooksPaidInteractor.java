package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.PaidBooksResponse;

/**
 * Created by Neil Cruz on 13/10/2017.
 */

public interface BooksPaidInteractor {
    interface onBooksPaidDataLoadedListener {
       void onSuccess(PaidBooksResponse paidBooksData);

        void onFailed();
    }

    void requestPaidBooks(onBooksPaidDataLoadedListener listener, Context context);
}
