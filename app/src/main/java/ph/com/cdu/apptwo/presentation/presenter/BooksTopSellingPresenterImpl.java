package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.response.TopSellingBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.BooksTopSellingInteractor;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksTopSellingView;
import ph.com.cdu.apptwo.domain.interactor.BooksTopSellingInteractorImpl;
import ph.com.cdu.apptwo.util.NetworkConnection;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksTopSellingPresenterImpl implements BooksTopSellingPresenter, BooksTopSellingInteractor.onBooksTopSellingDataLoadedListener {

    private Context mContext;
    private BooksTopSellingView mBooksTopSellingView;
    private BooksTopSellingInteractor mBooksTopSellingInteractor;

    public BooksTopSellingPresenterImpl(Context context, BooksTopSellingView booksTopSellingView) {
        mContext = context;
        mBooksTopSellingView = booksTopSellingView;
        mBooksTopSellingInteractor = new BooksTopSellingInteractorImpl();
    }

    @Override
    public void onCreate() {
        mBooksTopSellingView.setUpListeners();
        mBooksTopSellingView.setUpRecyclerViewDeck();
    }

    @Override
    public void onConnected() {
        mBooksTopSellingView.getBooks();
    }

    @Override
    public void initTopSellingBooks() {
        mBooksTopSellingView.onInitTopSellingBooksContent();
    }

    @Override
    public void initLinearLayoutManagerDeck() {
        mBooksTopSellingView.setUpLayoutManagerDeck();
    }

    @Override
    public void onSuccess(TopSellingBooksResponse topSellingBooksData) {
        for (int i = mContext.getResources().getInteger(R.integer.initial_value);
             i<topSellingBooksData.getResult().size(); i++) {
            mBooksTopSellingView.onStoreTopSellingBookContent(topSellingBooksData.getResult());
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void initLoadTopSellingBooksContent() {
        mBooksTopSellingInteractor.requestTopSellingBooks(this, mContext);
    }

    @Override
    public void onFinish() {
        mBooksTopSellingView.setUpBookViews();
    }
}
