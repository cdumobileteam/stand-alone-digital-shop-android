package ph.com.cdu.apptwo.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 25/10/2017.
 */
@Dao
public interface BookInfoDao {
    String TABLE_NAME = "bookinfo";

    @Query("SELECT * FROM " + TABLE_NAME)
    List<BookInfo> selectAllBookInfo();

    @Query("SELECT * from " + TABLE_NAME + " WHERE userId = :userId")
    List<BookInfo> selectBookInfoById(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertBookInfo(BookInfo bookInfo);

    @Delete
    int deleteBookInfo(BookInfo bookInfo);


}
