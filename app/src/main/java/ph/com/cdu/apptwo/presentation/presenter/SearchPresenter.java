package ph.com.cdu.apptwo.presentation.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public interface SearchPresenter {

    void onCreate();

    void onTappedSearch(String bookTitle);

    void onTappedButton(String bundle, Fragment fragment);

    void onTappedTitle(List<BookShop> bookTitle);

    void onTappedBook(Bundle mBundle, Fragment mFragment);

    void onTappedBack();

    void initMain();

    void setUpFind(CharSequence message);

    void initAnalytics();
}
