package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.TopSellingBooksResponse;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public interface BooksTopSellingInteractor {
    interface onBooksTopSellingDataLoadedListener {
        void onSuccess(TopSellingBooksResponse topSellingData);

        void onFailed();
    }

    void requestTopSellingBooks(onBooksTopSellingDataLoadedListener listener, Context context);
}
