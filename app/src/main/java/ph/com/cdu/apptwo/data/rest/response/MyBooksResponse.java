package ph.com.cdu.apptwo.data.rest.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.MyBooks;

/**
 * Created by Neil Cruz on 27/10/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyBooksResponse {
    private int response_code;
    private String status;
    private List<MyBooks> result;

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MyBooks> getResult() {
        return result;
    }

    public void setResult(List<MyBooks> result) {
        this.result = result;
    }
}
