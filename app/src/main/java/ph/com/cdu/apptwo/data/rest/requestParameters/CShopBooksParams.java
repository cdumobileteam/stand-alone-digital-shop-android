package ph.com.cdu.apptwo.data.rest.requestParameters;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.SearchBooks;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class CShopBooksParams implements Parcelable {
    private String id;
    private String title;
    private String author;
    private String genre;
    private String description;
    private String image_url;
    private String book_url;
    private String price;

    protected CShopBooksParams(Parcel in) {
        id = in.readString();
        title = in.readString();
        author = in.readString();
        genre = in.readString();
        description = in.readString();
        image_url = in.readString();
        book_url = in.readString();
        price = in.readString();
    }

    public static final Creator<CShopBooksParams> CREATOR = new Creator<CShopBooksParams>() {
        @Override
        public CShopBooksParams createFromParcel(Parcel in) {
            return new CShopBooksParams(in);
        }

        @Override
        public CShopBooksParams[] newArray(int size) {
            return new CShopBooksParams[size];
        }
    };

    public CShopBooksParams() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getBook_url() {
        return book_url;
    }

    public void setBook_url(String book_url) {
        this.book_url = book_url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(author);
        parcel.writeString(genre);
        parcel.writeString(description);
        parcel.writeString(image_url);
        parcel.writeString(book_url);
        parcel.writeString(price);
    }

    public CShopBooksParams getFreeBooksDetails(Context context, FreeBooks freeBooksDetails) {
        CShopBooksParams cShopBooksParams = new CShopBooksParams();
        cShopBooksParams.setId(freeBooksDetails.getId());
        cShopBooksParams.setTitle(freeBooksDetails.getTitle());
        cShopBooksParams.setAuthor(freeBooksDetails.getAuthor());
        cShopBooksParams.setGenre(freeBooksDetails.getGenre());
        cShopBooksParams.setDescription(freeBooksDetails.getDescription());
        cShopBooksParams.setImage_url(freeBooksDetails.getImage_url());
        cShopBooksParams.setBook_url(freeBooksDetails.getBook_url());
        cShopBooksParams.setPrice(checkAvailability(context, freeBooksDetails.getPrice()));
        return cShopBooksParams;
    }

    public CShopBooksParams getBooksDetails(Context context, SearchBooks freeBooksDetails) {
        CShopBooksParams cShopBooksParams = new CShopBooksParams();
        cShopBooksParams.setId(freeBooksDetails.getId());
        cShopBooksParams.setTitle(freeBooksDetails.getTitle());
        cShopBooksParams.setAuthor(freeBooksDetails.getAuthor());
        cShopBooksParams.setGenre(freeBooksDetails.getGenre());
        cShopBooksParams.setDescription(freeBooksDetails.getDescription());
        cShopBooksParams.setImage_url(freeBooksDetails.getImage_url());
        cShopBooksParams.setBook_url(freeBooksDetails.getBook_url());
        cShopBooksParams.setPrice(checkAvailability(context, freeBooksDetails.getPrice()));
        return cShopBooksParams;
    }

    private String checkAvailability(Context context, String price) {
        if (!price.equals(context.getString(R.string.download_free_books))) {
//            return String.format(context.getString(R.string.download_paid_books), price);
            return context.getString(R.string.buy);
        }
        return context.getString(R.string.download_free_books);
    }
}
