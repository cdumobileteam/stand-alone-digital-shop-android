package ph.com.cdu.apptwo.presentation.fragment.view;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.books.NewBooks;
import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksNewView {

    void setUpRecyclerViewDeck();

    void setUpLayoutManagerDeck();

    void countBooksAvailable(int librarySize);

    void onStoreNewBookContent(List<FreeBooks> result);

    void goToFullVersion(FreeBooks newBooksData);

    void onInitNewBooksContent();

    void setUpBookViews();

    void noRomanceAvailable(int romance);

    void noHorrorMysteryAvailable(int horrorMystery);

    void noFantasyAvailable(int fantasy);

    void noSciFiAvailable(int sciFi);

    void noActionAdventureAvailable(int actionAdventure);

    void noComedyAvailable(int comedy);

    void setUpListeners();

    void getBooks();
}
