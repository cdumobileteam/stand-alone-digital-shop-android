package ph.com.cdu.apptwo.presentation.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.requestParameters.NewUserParams;
import ph.com.cdu.apptwo.data.rest.response.FreeBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.NewBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.NewUserResponse;
import ph.com.cdu.apptwo.data.rest.response.PaidBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.RecommendedBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.TopSellingBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.BooksFreeInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksFreeInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.BooksNewInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksNewInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.BooksPaidInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksPaidInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.BooksRecommendedInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksRecommendedInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.BooksTopSellingInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksTopSellingInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.NewUserInteractor;
import ph.com.cdu.apptwo.domain.interactor.NewUserInteractorImpl;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.books.NewBooks;
import ph.com.cdu.apptwo.domain.model.books.PaidBooks;
import ph.com.cdu.apptwo.domain.model.books.RecommendedBooks;
import ph.com.cdu.apptwo.domain.model.books.TopSellingBooks;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;
import ph.com.cdu.apptwo.presentation.activity.view.SplashScreenView;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteBooksAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.InsertBooksDirectoryAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.InsertUserInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.model.Gender;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;

/**
 * Created by Neil Cruz on 17/10/2017.
 */

public class SplashScreenPresenterImpl implements SplashScreenPresenter, NewUserInteractor.onNewUserDataLoadedListener,
        BooksRecommendedInteractor.onBooksRecommendedDataLoadedListener, BooksFreeInteractor.onBooksFreeDataLoadedListener,
        BooksPaidInteractor.onBooksPaidDataLoadedListener, BooksTopSellingInteractor.onBooksTopSellingDataLoadedListener,
        BooksNewInteractor.onBooksNewDataLoadedListener{

    private Activity mActivity;
    private Context mContext;
    private DataDownloader mSharedPreferences;
    private SplashScreenView mSplashScreenView;
    private InsertBooksDirectoryAsyncTask mInsertBooksDirectoryAsyncTask;
    private Person mOtherInfo;
    private String mUserId, mEmail, mFirstName, mLastName, mGender, mProfilePicUri;
    private BooksRecommendedInteractor mBooksRecommendedInteractor;
    private BooksFreeInteractor mBooksFreeInteractor;
    private BooksPaidInteractor mBooksPaidInteractor;
    private BooksTopSellingInteractor mBooksTopSellingInteractor;
    private BooksNewInteractor mBooksNewInteractor;

    public SplashScreenPresenterImpl(Activity activity, Context context, SplashScreenView splashScreenView) {
        mActivity = activity;
        mContext = context;
        mSplashScreenView = splashScreenView;
        mSharedPreferences = new DataDownloader(mContext);
        mBooksRecommendedInteractor = new BooksRecommendedInteractorImpl();
        mBooksFreeInteractor = new BooksFreeInteractorImpl();
        mBooksPaidInteractor = new BooksPaidInteractorImpl();
        mBooksTopSellingInteractor = new BooksTopSellingInteractorImpl();
        mBooksNewInteractor = new BooksNewInteractorImpl();
    }

    @Override
    public void onCreate() {
//        mSplashScreenView.getCshop();
        mSplashScreenView.initFacebookSdk();
        mSplashScreenView.initGoogleSdk();
        mSplashScreenView.setUpSnackBars();
        mSplashScreenView.setUpListeners();
    }

    @Override
    public void setCShop() {
        mBooksRecommendedInteractor.requestRecommendedBooks(this,mContext);
        mBooksFreeInteractor.requestFreeBooks(this, mContext);
        mBooksPaidInteractor.requestPaidBooks(this, mContext);
        mBooksTopSellingInteractor.requestTopSellingBooks(this, mContext);
        mBooksNewInteractor.requestNewBooks(this, mContext);
    }

    @Override
    public void onTappedLoginGooglePlus() {
        mSplashScreenView.checkGooglePlusStatus();
    }

    @Override
    public void onTappedLoginFacebook() {
        mSplashScreenView.checkFacebookStatus();
    }

    @Override
    public void onLoginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
    }

    @Override
    public void setFacebookData(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {

                             mUserId = response.getJSONObject().getString("id");
                             mEmail = response.getJSONObject().getString("email");
                             mFirstName = response.getJSONObject().getString("first_name");
                             mLastName = response.getJSONObject().getString("last_name");
                             mGender = response.getJSONObject().getString("gender");

                            if (Profile.getCurrentProfile() != null)
                            {
                                mProfilePicUri = Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString();
                                System.out.println("Facebook Login ProfilePic: " + mProfilePicUri.toString());
                            }

                            saveUserInfo(mUserId, mEmail, mFirstName, mLastName, mGender, mProfilePicUri, mContext.getString(R.string.social_media_facebook));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoginManager.getInstance().logOut();
                            mSplashScreenView.onFailed("Login Failed");
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, email, first_name, last_name, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void saveUserInfo(String mUserId, String mEmail, String mFirstName, String mLastName, String mGender, String mProfilePicUri, String mSocialAccount) {
        InsertUserInfoAsyncTask insertUserInfoAsyncTask = new InsertUserInfoAsyncTask(mContext, mUserId, mEmail, mFirstName,
                mLastName, mGender, mProfilePicUri, mSocialAccount);
        insertUserInfoAsyncTask.execute();

        NewUserInteractor newUserInteractor = new NewUserInteractorImpl();
        NewUserParams newUserParams = new NewUserParams(mUserId, mFirstName, mLastName,mGender,mEmail,mSocialAccount, mProfilePicUri);
        newUserInteractor.requestNewUser(this,mContext,newUserParams);

        mSplashScreenView.setUserInfo(mSocialAccount, mUserId, mEmail, mFirstName, mLastName, mGender, mProfilePicUri);
    }

    @Override
    public void onLoginGooglePlus(GoogleApiClient mGoogleApiClient) {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mActivity.startActivityForResult(signInIntent, mContext.getResources().getInteger(R.integer.rc_sign_in));
    }

    @Override
    public void setGooglePlusData(GoogleSignInAccount mGoogleAccount, Person personalInfo) {
        mUserId = mGoogleAccount.getId();
        mEmail = mGoogleAccount.getEmail();
        mFirstName = mGoogleAccount.getGivenName();
        mLastName = mGoogleAccount.getFamilyName();
        if (personalInfo != null ) {
            int gender = personalInfo.getGender();
            if (gender == Gender.FEMALE.getGenderCd()) {
                mGender = Gender.FEMALE.name();
            } else {
                mGender = Gender.MALE.name();
            }
        }
        if (mGoogleAccount.getPhotoUrl() != null)
        {
            mProfilePicUri = mGoogleAccount.getPhotoUrl().toString();
            System.out.println("Google Login ProfilePic: " + mProfilePicUri.toString());
        }
        saveUserInfo(mUserId, mEmail, mFirstName, mLastName, mGender, mProfilePicUri, mContext.getString(R.string.social_media_google_plus));
    }

    @Override
    public void onSuccessLogin(GoogleApiClient mGoogleApiClient) {
        if (mGoogleApiClient.hasConnectedApi(Plus.API)) {
            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            if (person != null) {
                mOtherInfo = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            } else {
                Log.e(mContext.getString(R.string.title_activity_splash_screen), "Error!");
            }
            mSplashScreenView.setOtherInfo(mOtherInfo);
        }
    }

    @Override
    public void onSocialPluginLaunch() {
        mSplashScreenView.validateLogin();
    }

    @Override
    public void onSuccess(NewUserResponse newUserData) {
        mSplashScreenView.initUserAccount();
        mSplashScreenView.onNewUserAccount(newUserData.getResponseCode());
    }

    @Override
    public void onSuccess(RecommendedBooksResponse recommendedBooksData) {
        mSplashScreenView.onLoadRecommendedBooksContent(recommendedBooksData.getResult());
    }

    @Override
    public void onSuccess(FreeBooksResponse freeBooksData) {
        mSplashScreenView.onLoadFreeBooksContent(freeBooksData.getResult());
    }

    @Override
    public void onSuccess(PaidBooksResponse paidBooksData) {
        mSplashScreenView.onLoadPaidBooksContent(paidBooksData.getResult());
    }

    @Override
    public void onSuccess(TopSellingBooksResponse topSellingData) {
        mSplashScreenView.onLoadTopSellingContent(topSellingData.getResult());
    }

    @Override
    public void onSuccess(NewBooksResponse newBooksData) {
        mSplashScreenView.onLoadNewContent(newBooksData.getResult());
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onColdSignedIn() {
        mSplashScreenView.setUserInfo(mContext.getString(R.string.social_media_facebook),
                mSharedPreferences.getUserId(),mSharedPreferences.getUserEmail(),
                mSharedPreferences.getUserName(),"","","");
    }

    @Override
    public void storeRecommendedBooks(List<FreeBooks> result) {
        for (int i=0;i<result.size();i++) {
            mInsertBooksDirectoryAsyncTask = new InsertBooksDirectoryAsyncTask(mContext,result.get(i).getId(),
                    result.get(i).getTitle(),result.get(i).getAuthor(),
                    result.get(i).getGenre(),result.get(i).getDescription(),
                    result.get(i).getImage_url(),result.get(i).getBook_url(),
                    result.get(i).getPrice(),mContext.getString(R.string.params_recommended_books_content));
            mInsertBooksDirectoryAsyncTask.execute();
        }
    }

    @Override
    public void storeFreeBooks(List<FreeBooks> result) {
        for (int i=0;i<result.size();i++) {
            mInsertBooksDirectoryAsyncTask = new InsertBooksDirectoryAsyncTask(mContext,result.get(i).getId(),
                    result.get(i).getTitle(),result.get(i).getAuthor(),
                    result.get(i).getGenre(),result.get(i).getDescription(),
                    result.get(i).getImage_url(),result.get(i).getBook_url(),
                    result.get(i).getPrice(),mContext.getString(R.string.params_free_books_content));
            mInsertBooksDirectoryAsyncTask.execute();
        }
    }

    @Override
    public void storePaidBooks(List<FreeBooks> result) {
        for (int i=0;i<result.size();i++) {
            mInsertBooksDirectoryAsyncTask = new InsertBooksDirectoryAsyncTask(mContext,result.get(i).getId(),
                    result.get(i).getTitle(),result.get(i).getAuthor(),
                    result.get(i).getGenre(),result.get(i).getDescription(),
                    result.get(i).getImage_url(),result.get(i).getBook_url(),
                    result.get(i).getPrice(),mContext.getString(R.string.params_paid_books_content));
            mInsertBooksDirectoryAsyncTask.execute();
        }
    }

    @Override
    public void storeTopSellingBooks(List<FreeBooks> result) {
        for (int i=0;i<result.size();i++) {
            mInsertBooksDirectoryAsyncTask = new InsertBooksDirectoryAsyncTask(mContext,result.get(i).getId(),
                    result.get(i).getTitle(),result.get(i).getAuthor(),
                    result.get(i).getGenre(),result.get(i).getDescription(),
                    result.get(i).getImage_url(),result.get(i).getBook_url(),
                    result.get(i).getPrice(),mContext.getString(R.string.params_topselling_books_content));
            mInsertBooksDirectoryAsyncTask.execute();
        }
    }

    @Override
    public void storeNewBooks(List<FreeBooks> result) {
        for (int i=0;i<result.size();i++) {
            mInsertBooksDirectoryAsyncTask = new InsertBooksDirectoryAsyncTask(mContext,result.get(i).getId(),
                    result.get(i).getTitle(),result.get(i).getAuthor(),
                    result.get(i).getGenre(),result.get(i).getDescription(),
                    result.get(i).getImage_url(),result.get(i).getBook_url(),
                    result.get(i).getPrice(),mContext.getString(R.string.params_new_books_content));
            mInsertBooksDirectoryAsyncTask.execute();
        }
    }

    @Override
    public void onReCreate() {
        DeleteBooksAsyncTask deleteBooksAsyncTask = new DeleteBooksAsyncTask(mContext);
        deleteBooksAsyncTask.execute();
    }

    @Override
    public void initAnalytics() {
        mSplashScreenView.goAnalytics();
    }

}
