package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.NewUserResponse;
import ph.com.cdu.apptwo.data.rest.requestParameters.NewUserParams;

/**
 * Created by Neil Cruz on 03/11/2017.
 */

public interface NewUserInteractor {
    interface onNewUserDataLoadedListener {
        void onSuccess(NewUserResponse newUserData);

        void onFailed();
    }

    void requestNewUser(onNewUserDataLoadedListener listener, Context context, NewUserParams newUserParams);
}
