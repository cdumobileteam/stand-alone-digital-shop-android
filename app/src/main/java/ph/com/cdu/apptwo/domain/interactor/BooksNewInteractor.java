package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.NewBooksResponse;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public interface BooksNewInteractor {
    interface onBooksNewDataLoadedListener {
        void onSuccess(NewBooksResponse newBooksData);

        void onFailed();
    }

    void requestNewBooks(onBooksNewDataLoadedListener listener, Context context);
}
