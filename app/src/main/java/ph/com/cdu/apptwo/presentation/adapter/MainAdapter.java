package ph.com.cdu.apptwo.presentation.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import ph.com.cdu.apptwo.presentation.fragment.BooksPaidFragment;
import ph.com.cdu.apptwo.presentation.fragment.BooksFreeFragment;
import ph.com.cdu.apptwo.presentation.fragment.BooksNewFragment;
import ph.com.cdu.apptwo.presentation.fragment.BooksTopSellingFragment;

/**
 * Created by Neil Cruz on 11/10/2017.
 */

public class MainAdapter extends FragmentStatePagerAdapter {

    private FragmentManager mFragmentManager;
    private ArrayList<Fragment> mFragmentArraylist = new ArrayList<>();

    public MainAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
        mFragmentArraylist.add(BooksFreeFragment.newInstance());
        mFragmentArraylist.add(BooksPaidFragment.newInstance());
        mFragmentArraylist.add(BooksNewFragment.newInstance());
        mFragmentArraylist.add(BooksTopSellingFragment.newInstance());
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case  0:
                return mFragmentArraylist.get(position);
            case 1:
                return mFragmentArraylist.get(position);
            case 2:
                return mFragmentArraylist.get(position);
            case 3:
                return mFragmentArraylist.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
