package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.requestParameters.GooglePlaymentParams;
import ph.com.cdu.apptwo.data.rest.response.GooglePlaymentResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public class GooglePlaymentInteractorImpl implements GooglePlaymentInteractor {

    private CDUService mService;

    @Override
    public void onRequestGooglePlayment(final onGooglePlaymentDataLoadedListener listener, Context context, GooglePlaymentParams googlePlaymentParams) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_GOOGLE_PAYMENT);

        mService.getGooglePlayReceipt(googlePlaymentParams.mapParameters(context)).enqueue(new Callback<GooglePlaymentResponse>() {
            @Override
            public void onResponse(Call<GooglePlaymentResponse> call, Response<GooglePlaymentResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<GooglePlaymentResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
