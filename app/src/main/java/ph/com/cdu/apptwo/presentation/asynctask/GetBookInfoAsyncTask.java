package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Patrick Santos on 08/03/2018.
 */

public class GetBookInfoAsyncTask extends AsyncTask<String, Void, List<BookShop>> {
    private Context mContext;
    private CallBackTask mCallBackTask;

    public GetBookInfoAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<BookShop> doInBackground(String... strings) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        return databaseInteractor.getBookInfo(mContext,strings[0]);
    }

    @Override
    protected void onPostExecute(List<BookShop> bookShops) {
        super.onPostExecute(bookShops);
        mCallBackTask.callback(bookShops);
    }

    public void setmCallBackTask(CallBackTask mCallBackTask) {
        this.mCallBackTask = mCallBackTask;
    }

    public interface CallBackTask {
        void callback(List<BookShop> bookShops);
    }
}
