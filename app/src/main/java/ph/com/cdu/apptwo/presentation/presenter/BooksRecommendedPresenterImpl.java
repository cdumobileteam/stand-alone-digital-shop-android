package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;
import android.view.View;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksRecommendedView;
import ph.com.cdu.apptwo.data.rest.response.RecommendedBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.BooksRecommendedInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksRecommendedInteractorImpl;
import ph.com.cdu.apptwo.util.NetworkConnection;

import static android.view.View.GONE;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksRecommendedPresenterImpl implements BooksRecommendedPresenter, BooksRecommendedInteractor.onBooksRecommendedDataLoadedListener {

    private Context mContext;
    private BooksRecommendedView mBooksRecommendedView;
    private BooksRecommendedInteractor mBooksRecommendedInteractor;

    public BooksRecommendedPresenterImpl(Context context, BooksRecommendedView booksRecommendedView) {
        mContext = context;
        mBooksRecommendedView = booksRecommendedView;
        mBooksRecommendedInteractor = new BooksRecommendedInteractorImpl();
    }

    @Override
    public void onCreate() {
        mBooksRecommendedView.setUpListeners();
        mBooksRecommendedView.setUpRecyclerViewDeck();
    }

    @Override
    public void onConnected() {
        mBooksRecommendedView.getBooks();
    }

    @Override
    public void initRecommendedBooks() {
        mBooksRecommendedView.onInitRecommendedBooksContent();
    }

    @Override
    public void initLinearLayoutManagerDeck() {
        mBooksRecommendedView.setUpLayoutManagerDeck();
    }

    @Override
    public void onSuccess(RecommendedBooksResponse recommendedBooksData) {
        for (int i = mContext.getResources().getInteger(R.integer.initial_value);
             i<recommendedBooksData.getResult().size(); i++) {
            mBooksRecommendedView.onStoreRecommendedBookContent(recommendedBooksData.getResult());
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void initLoadRecommendedBooksContent() {
        mBooksRecommendedInteractor.requestRecommendedBooks(this, mContext);
    }

    @Override
    public void onFinish() {
        mBooksRecommendedView.setUpBookViews();
    }
}
