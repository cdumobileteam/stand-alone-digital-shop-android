package ph.com.cdu.apptwo.presentation.fragment;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.BuildConfig;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.presentation.adapter.MyBooksAdapter;
import ph.com.cdu.apptwo.presentation.adapter.MyCShopAdapter;
import ph.com.cdu.apptwo.presentation.presenter.MyBooksPresenter;
import ph.com.cdu.apptwo.presentation.presenter.MyBooksPresenterImpl;
import ph.com.cdu.apptwo.presentation.util.ServiceDownloader;
import ph.com.cdu.apptwo.util.Gaid;
import ph.com.cdu.apptwo.util.Logger;
import ph.com.cdu.apptwo.util.PDFViewer;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.MyBooks;
import ph.com.cdu.apptwo.presentation.fragment.view.MyBooksView;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;

/**
 * Created by Neil Cruz on 11/10/2017.
 */

public class MyBooksFragment extends Fragment implements MyBooksView, View.OnClickListener {

    private Context mContext;
    private MainView mMainView;
    private BroadcastReceiver onComplete;
    private DataDownloader mSharedPreferences;
    private PDFViewer mPdfViewer;
    private Logger mLogger;
    private Gaid mAnalytics;
    private GridLayoutManager mGridLayoutManager1;
    private GridLayoutManager mGridLayoutManager2;
    private LinearLayoutManager mLnrLayoutManager;
    private MyCShopAdapter myCShopAdapter;
    private MyBooksAdapter mMyBookAdapter;
    private MyBooksPresenter mPresenter;
    private FragmentManager mFragmentManager;
    @BindView(R.id.scrollViewMyBooks)
    ScrollView mScrllViewMyBooks;
    @BindView(R.id.recyclerViewMyBooks)
    RecyclerView mRcyclerViewMyBooks;
    @BindView(R.id.recyclerView)
    RecyclerView mRcyclrView;
    @BindView(R.id.textViewMyBooks)
    TextView mTxtViewMyBooks;
    @BindView(R.id.frameLayoutBookStatus)
    FrameLayout mFrmLayoutBookStatus;
    @BindView(R.id.textViewNoBooks)
    TextView mTxtViewNoBooks;
    @BindView(R.id.linearLayoutRefresh)
    LinearLayout mLnrLayoutRefresh;
    @BindView(R.id.imageViewNoConnection)
    ImageView mImgViewNoConnection;

    public MyBooksFragment() {
        // Required empty public constructor
    }

    public static MyBooksFragment newInstance() {
        MyBooksFragment fragment = new MyBooksFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_books, container, false);
        ButterKnife.bind(this, view);
        mContext = getContext();
        mSharedPreferences = new DataDownloader(mContext);
        mFragmentManager = getActivity().getSupportFragmentManager();
        mPresenter = new MyBooksPresenterImpl(mContext,this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.initAnalytics();
        mPresenter.onCreate();
    }

    @Override
    public void setUpRecyclerView() {
        myCShopAdapter = new MyCShopAdapter(mContext, this, new ArrayList<MyBooks>(0),
                new ArrayList<BookInfo>(0));
        mMyBookAdapter = new MyBooksAdapter(mContext,this, new ArrayList<BookInfo>(0));
        mLnrLayoutManager = new LinearLayoutManager(mContext);
        mLnrLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mGridLayoutManager1 = new GridLayoutManager(mContext, 3,GridLayoutManager.VERTICAL,false);
        mGridLayoutManager2 = new GridLayoutManager(mContext,3,GridLayoutManager.VERTICAL,false);
        mRcyclerViewMyBooks.setAdapter(myCShopAdapter);
        mRcyclrView.setAdapter(mMyBookAdapter);
    }

    @Override
    public void onLoadMyBooksContent(List<BookInfo> bookInfo, String user_id_fk) {
        setLayoutManager(bookInfo.size());
        mMyBookAdapter.updateDeck(bookInfo, user_id_fk);
        setUpViews(View.GONE,View.GONE, View.GONE);
        mScrllViewMyBooks.setVisibility(View.GONE);
    }

    private void setLayoutManager(int size) {
        if (size > 1) {
            mRcyclrView.setLayoutManager(mGridLayoutManager2);
        } else {
            mRcyclrView.setLayoutManager(mLnrLayoutManager);
        }
    }

    @Override
    public void setUpListeners() {
        mLnrLayoutRefresh.setOnClickListener(this);
        mImgViewNoConnection.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainView) {
            mMainView = (MainView) context;
        } else {
            throw new ClassCastException("Must implement MainView");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMainView.setUpDownloadManager();
    }

    @Override
    public void openBook(BookInfo book) {
//        File urlBook = new File(book.getUrlDownload());
        mPresenter.onTappedOpenPDFReader(book.getPid(),book.getUserId(),book.getId(),
                book.getTitle(),getParser(book.getUrlDownload()));
//        view.openPDFReader(getParser(book.getUrlDownload()), getString(R.string.mime_type_pdf));
    }

//    private Uri getParser(String bookURLDownload) {
    private Uri getParser (String bookUri) {
        Uri uri = Uri.parse(bookUri);
        File file = new File(uri.getPath());
        return FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID, file);
//        return Uri.fromFile(bookURLDownload);
//        return Uri.parse(bookURLDownload);
    }

    @Override
    public void onLoadUserTransactions(int response_code, List<MyBooks> userBooks) {
        if (response_code == 0) {
            mPresenter.initMyBooks(mSharedPreferences.getUserId(), userBooks);
        } else if (response_code == 1) {
            mImgViewNoConnection.setVisibility(View.GONE);
            setUpViews(View.VISIBLE,View.GONE, View.VISIBLE);
        } else {
            mPresenter.initMyBooks(mSharedPreferences.getUserId(), userBooks);
        }
    }

    @Override
    public void showBookStatus(boolean hasNetwork) {
        if (hasNetwork) {
            mScrllViewMyBooks.setVisibility(View.VISIBLE);
            setUpViews(View.VISIBLE,View.GONE, View.VISIBLE);
        } else {
            mImgViewNoConnection.setVisibility(View.VISIBLE);
            mScrllViewMyBooks.setVisibility(View.GONE);
            setUpViews(View.VISIBLE,View.GONE, View.GONE);
        }
    }

    @Override
    public void showMyCShopBooks(List<MyBooks> cShopBooks, String user_id_fk, List<BookInfo> myBooks) {
        setUpViews(View.GONE,View.GONE, View.GONE);
        mScrllViewMyBooks.setVisibility(View.VISIBLE);
        mRcyclerViewMyBooks.setLayoutManager(mGridLayoutManager1);
        myCShopAdapter.updateDeck(cShopBooks, user_id_fk, myBooks);
    }

    private void setUpViews(int bookStatus, int refreshButton, int noBooks) {
        mFrmLayoutBookStatus.setVisibility(bookStatus);
        mLnrLayoutRefresh.setVisibility(refreshButton);
        mTxtViewNoBooks.setVisibility(noBooks);
    }

    @Override
    public void viewBook(final int id, final String userId, final String bookId, final String title, final String uri) {
        mPresenter.onTappedOpenPDFReader(id,userId,bookId,title,getParser(uri));
    }

    @Override
    public void getBook(MyBooks myBooks) {
        mAnalytics.sendGAID(getString(R.string.title_fragment_my_book),getString(R.string.log_category_action),getString(R.string.log_action_view_purchase));
        mLogger.logBookDownload();
        Intent intent = new Intent(getActivity().getApplicationContext(), ServiceDownloader.class);
        intent.putExtra(getString(R.string.params_userId),myBooks.getUser_id_fk());
        intent.putExtra(getString(R.string.params_book_id), myBooks.getBook_id());
        intent.putExtra(getString(R.string.params_book_title), myBooks.getBook_title());
        intent.putExtra(getString(R.string.params_book_image_url), myBooks.getBook_image());
        intent.putExtra(getString(R.string.params_book_url_download), myBooks.getBook_url());
        getActivity().startService(intent);

        mSharedPreferences.setBookDownloaded(myBooks.getBook_id());
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        onComplete = new ServiceDownloader.MyRequestReceiver();
        mContext.registerReceiver(onComplete, filter);

//        Timer myTimer = new Timer();
//        myTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                DownloadManager mDownloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//                DownloadManager.Query query = new DownloadManager.Query();
////                q.setFilterById(mDownloadedFileID);
//                Cursor cursor = mDownloadManager.query(query);
//                cursor.moveToFirst();
//                try {
//                    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
//                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
//                    cursor.close();
//                    final int dl_progress = (bytes_downloaded * 100 / bytes_total);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Log.d("progress: ", dl_progress + "");
//                        }
//                    });
//                } catch (Exception e) {
//                    Log.d("error: ", e.toString());
//                }
//
//            }
//
//        }, 0, 10);

        mMainView.onDownloadComplete();
    }

    public void unregisterReceiver() {
        if (onComplete != null) {
            mContext.unregisterReceiver(onComplete);
        }
    }

    @Override
    public void goAnalytics() {
        mLogger = new Logger(mContext);
        mAnalytics = new Gaid(mContext);
    }

    @Override
    public void goToViewBook(Uri uri, String mime) {
        mMainView.onTapped(mPdfViewer.openPDFReader(uri, mime));
    }

    @Override
    public void onClick(View v) {
        mPresenter.onTappedButton();
        switch (v.getId()) {
            case R.id.linearLayoutRefresh:
                setUpViews(View.VISIBLE,View.VISIBLE, View.GONE);
                mMainView.onTapped(getString(R.string.text_loading_in_progress));
                break;
        }
    }

    private void openPDFReader(int id, String userId,String bookId,String bookTitle, Uri urlBook) {
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.params_unique_id),id);
        bundle.putString(getString(R.string.params_userId),userId);
        bundle.putString(getString(R.string.params_book_id),bookId);
        bundle.putString(getString(R.string.params_book_title),bookTitle);
        bundle.putParcelable(getString(R.string.params_book_url_download), urlBook);
        ViewPdfFragment viewMyBooks = new ViewPdfFragment();
        viewMyBooks.setArguments(bundle);
        mFragmentManager.beginTransaction().addToBackStack(getString(R.string.title_activity_main))
                .replace(R.id.flContent, viewMyBooks)
                .commit();
    }

    @Override
    public void initReader(final int pid, final String userId, final String id, final String title, final Uri uriPath) {
        mPdfViewer = new PDFViewer(mContext);
        final File filePath = Environment.getExternalStoragePublicDirectory("/"+mContext.getPackageName()+"/" + id+userId);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLogger.logBookShare(getString(R.string.view_book),id);
                mPresenter.onView(pid,filePath,uriPath,getString(R.string.mime_type_pdf));
//                openPDFReader(pid,userId,id,title,uri);
            }
        }, mContext.getResources().getInteger(R.integer.shorter_relay));
    }

    @Override
    public void goToNetworkConnection() {
        mMainView.goToNoNetworkConnection();
    }

    @Override
    public void onViewBookFailed() {
        Toast.makeText(mContext, getString(R.string.alert_error_occured_please_download_again), Toast.LENGTH_SHORT).show();
    }
}
