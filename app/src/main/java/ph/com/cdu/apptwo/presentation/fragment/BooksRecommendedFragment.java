package ph.com.cdu.apptwo.presentation.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.requestParameters.CShopBooksParams;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.presentation.adapter.Recommended.BooksRomanceAdapter;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksRecommendedView;
import ph.com.cdu.apptwo.presentation.presenter.BooksRecommendedPresenterImpl;
import ph.com.cdu.apptwo.presentation.presenter.BooksRecommendedPresenter;
import ph.com.cdu.apptwo.util.NetworkConnection;

/**
 * Created by Neil Cruz on 11/10/2017.
 */

public class BooksRecommendedFragment extends Fragment implements BooksRecommendedView, View.OnClickListener {

    private Context mContext;
    private MainView mMainView;
    private CShopBooksParams mBooksParams;
    private FragmentManager mFragmentManager;
    private Fragment mFragment;
    private BooksRecommendedPresenter mPresenter;
    private GridLayoutManager mGridLayoutManager;
    private NetworkConnection mInternet;
//    private LinearLayoutManager mLnrLayout1Manager;
//    private LinearLayoutManager mLnrLayout2Manager;
//    private LinearLayoutManager mLnrLayout3Manager;
//    private LinearLayoutManager mLnrLayout4Manager;
//    private LinearLayoutManager mLnrLayout5Manager;
//    private LinearLayoutManager mLnrLayout6Manager;
    private BooksRomanceAdapter mRomanceDeckAdapter;
//    private BooksHorrorMysteryAdapter mHorrorMysteryDeckAdapter;
//    private BooksFantasyAdapter mFantasyDeckAdapter;
//    private BooksSciFiAdapter mSciFiDeckAdapter;
//    private BooksActionAdventureAdapter mActionAdventureDeckAdapter;
//    private BooksComedyAdapter mComedyDeckAdapter;
    @BindView(R.id.linearLayoutRomance)
    LinearLayout mLnrLayoutRomance;
    @BindView(R.id.linearLayoutHorrorMystery)
    LinearLayout mLnrLayoutHorrorMystery;
    @BindView(R.id.linearLayoutFantasy)
    LinearLayout mLnrLayoutFantasy;
    @BindView(R.id.linearLayoutSciFi)
    LinearLayout mLnrLayoutSciFi;
    @BindView(R.id.linearLayoutActionAdventure)
    LinearLayout mLnrLayoutActionAdventure;
    @BindView(R.id.linearLayoutComedy)
    LinearLayout mLnrLayoutComedy;
    @BindView(R.id.scrollViewRecommendedBooks)
    NestedScrollView mSrlViewRecommendedBooks;
    @BindView(R.id.imageViewNoConnection)
    ImageView mImgViewNoConnection;
    @BindView(R.id.recyclerViewRomanceDeck)
    RecyclerView mRcylrViewRomanceDeck;
    @BindView(R.id.recyclerViewHorrorMysteryDeck)
    RecyclerView mRcylrViewHorrorMysteryDeck;
    @BindView(R.id.recyclerViewFantasyDeck)
    RecyclerView mRcyclrViewFantasyDeck;
    @BindView(R.id.recyclerViewSciFiDeck)
    RecyclerView mRcyclrViewSciFiDeck;
    @BindView(R.id.recyclerViewActionAdventureDeck)
    RecyclerView mRcyclrViewActionAdventureDeck;
    @BindView(R.id.recyclerViewComedyDeck)
    RecyclerView mRcyclrViewComedyDeck;
    @BindView(R.id.textViewBooksAvailable)
    TextView mTxtViewBooksAvailable;

    public BooksRecommendedFragment() {
        // Required empty public constructor
    }

    public static BooksRecommendedFragment newInstance() {
        BooksRecommendedFragment fragment = new BooksRecommendedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_books_recommended, container, false);
        ButterKnife.bind(this, view);
        mContext = getContext();
        mPresenter = new BooksRecommendedPresenterImpl(mContext, this);
        mBooksParams = new CShopBooksParams();
        mInternet = new NetworkConnection(mContext);
        mFragmentManager = getActivity().getSupportFragmentManager();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainView) {
            mMainView = (MainView) context;
        } else {
            throw new ClassCastException("Must implement MainView");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onCreate();
        mPresenter.onConnected();
//        mPresenter.initRecommendedBooks();
    }

    @Override
    public void setUpListeners() {
        mImgViewNoConnection.setOnClickListener(this);
    }

    @Override
    public void setUpLayoutManagerDeck() {
        mGridLayoutManager = new GridLayoutManager(mContext,3,GridLayoutManager.VERTICAL,false);

//        mLnrLayout1Manager = new LinearLayoutManager(mContext);
//        mLnrLayout1Manager.setOrientation(LinearLayoutManager.HORIZONTAL);

//        mLnrLayout2Manager = new LinearLayoutManager(mContext);
//        mLnrLayout2Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//
//        mLnrLayout3Manager = new LinearLayoutManager(mContext);
//        mLnrLayout3Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//
//        mLnrLayout4Manager = new LinearLayoutManager(mContext);
//        mLnrLayout4Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//
//        mLnrLayout5Manager = new LinearLayoutManager(mContext);
//        mLnrLayout5Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//
//        mLnrLayout6Manager = new LinearLayoutManager(mContext);
//        mLnrLayout6Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
    }

    @Override
    public void setUpRecyclerViewDeck() {
        mRomanceDeckAdapter = new BooksRomanceAdapter(mMainView, mContext, this ,
                new ArrayList<FreeBooks>(0));
        mPresenter.initLinearLayoutManagerDeck();
        mRcylrViewRomanceDeck.setLayoutManager(mGridLayoutManager);
        mRcylrViewRomanceDeck.getLayoutManager().setAutoMeasureEnabled(true);
        mRcylrViewRomanceDeck.setNestedScrollingEnabled(false);
        mRcylrViewRomanceDeck.setHasFixedSize(false);
        mRcylrViewRomanceDeck.setAdapter(mRomanceDeckAdapter);

//        mHorrorMysteryDeckAdapter = new BooksHorrorMysteryAdapter(mMainView, mContext, this ,
//                new ArrayList<BookShop>(0));
//        mPresenter.initLinearLayoutManagerDeck();
//        mRcylrViewHorrorMysteryDeck.setAdapter(mHorrorMysteryDeckAdapter);
//        mRcylrViewHorrorMysteryDeck.setLayoutManager(mLnrLayout2Manager);
//
//        mFantasyDeckAdapter = new BooksFantasyAdapter(mMainView, mContext, this ,
//                new ArrayList<BookShop>(0));
//        mPresenter.initLinearLayoutManagerDeck();
//        mRcyclrViewFantasyDeck.setAdapter(mFantasyDeckAdapter);
//        mRcyclrViewFantasyDeck.setLayoutManager(mLnrLayout3Manager);
//
//        mSciFiDeckAdapter = new BooksSciFiAdapter(mMainView, mContext, this ,
//                new ArrayList<BookShop>(0));
//        mPresenter.initLinearLayoutManagerDeck();
//        mRcyclrViewSciFiDeck.setAdapter(mSciFiDeckAdapter);
//        mRcyclrViewSciFiDeck.setLayoutManager(mLnrLayout4Manager);
//
//        mActionAdventureDeckAdapter = new BooksActionAdventureAdapter(mMainView, mContext, this ,
//                new ArrayList<BookShop>(0));
//        mPresenter.initLinearLayoutManagerDeck();
//        mRcyclrViewActionAdventureDeck.setAdapter(mActionAdventureDeckAdapter);
//        mRcyclrViewActionAdventureDeck.setLayoutManager(mLnrLayout5Manager);
//
//        mComedyDeckAdapter = new BooksComedyAdapter(mMainView, mContext, this ,
//                new ArrayList<BookShop>(0));
//        mPresenter.initLinearLayoutManagerDeck();
//        mRcyclrViewComedyDeck.setAdapter(mComedyDeckAdapter);
//        mRcyclrViewComedyDeck.setLayoutManager(mLnrLayout6Manager);
    }

    @Override
    public void getBooks() {
        if (mInternet.hasActiveConnection(mContext.getString(R.string.url_digitalshop2))) {
            mPresenter.initLoadRecommendedBooksContent();
        } else {
            mImgViewNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStoreRecommendedBookContent(List<FreeBooks> result) {
        mRomanceDeckAdapter.updateDeck(result);
//        for (int i=mContext.getResources().getInteger(R.integer.initial_value);i<result.size();i++) {
//            InsertBooksDirectoryAsyncTask insertBooksDirectoryAsyncTask =
//                    new InsertBooksDirectoryAsyncTask(mContext,result.get(i).getId(),
//                            result.get(i).getTitle(),result.get(i).getAuthor(),
//                            result.get(i).getGenre(),result.get(i).getDescription(),
//                            result.get(i).getImage_url(),result.get(i).getBook_url(),
//                            result.get(i).getPrice(),getString(R.string.params_recommended_books_content));
//            insertBooksDirectoryAsyncTask.execute();
//        }
//        mPresenter.initRecommendedBooks();
        mPresenter.onFinish();
    }

    @Override
    public void countBooksAvailable(int librarySize) {
        mTxtViewBooksAvailable.setText(librarySize+ " " + getResources().getQuantityText(R.plurals.number_of_book, librarySize));
    }

    @Override
    public void goToFullVersion(FreeBooks recommendedBooksData) {
        mFragment = new BookDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.book_content),
                getString(R.string.params_recommended_category));
        bundle.putParcelable("RecommendedBooksContent", mBooksParams.getFreeBooksDetails(mContext, recommendedBooksData));
        mFragment.setArguments(bundle);
        mFragmentManager.beginTransaction().addToBackStack(getString(R.string.title_activity_main)).
                replace(R.id.flContent, mFragment)
                .commit();
        mMainView.initBookView();
    }

    @Override
    public void onInitRecommendedBooksContent() {
//        GetBookCategoryAsyncTask getBookCategoryAsyncTask = new GetBookCategoryAsyncTask(mContext);
//        getBookCategoryAsyncTask.setOnCallBack(new GetBookCategoryAsyncTask.CallBackTask() {
//            @Override
//            public void callback(List<BookShop> bookShops) {
//                mRomanceDeckAdapter.updateDeck(bookShops);
//                mHorrorMysteryDeckAdapter.updateDeck(bookShops);
//                mFantasyDeckAdapter.updateDeck(bookShops);
//                mSciFiDeckAdapter.updateDeck(bookShops);
//                mActionAdventureDeckAdapter.updateDeck(bookShops);
//                mComedyDeckAdapter.updateDeck(bookShops);
//                mPresenter.onFinish();
//            }
//        });
//        getBookCategoryAsyncTask.execute("RecommendedBooksContent");
    }

    @Override
    public void noRomanceAvailable(int romance) {
        mLnrLayoutRomance.setVisibility(romance);
    }

    @Override
    public void noHorrorMysteryAvailable(int horrorMystery) {
        mLnrLayoutHorrorMystery.setVisibility(horrorMystery);
    }

    @Override
    public void noFantasyAvailable(int fantasy) {
        mLnrLayoutFantasy.setVisibility(fantasy);
    }

    @Override
    public void noSciFiAvailable(int sciFi) {
        mLnrLayoutSciFi.setVisibility(sciFi);
    }

    @Override
    public void noActionAdventureAvailable(int actionAdventure) {
        mLnrLayoutActionAdventure.setVisibility(actionAdventure);
    }

    @Override
    public void noComedyAvailable(int comedy) {
        mLnrLayoutComedy.setVisibility(comedy);
    }

    @Override
    public void setUpBookViews() {
        mImgViewNoConnection.setVisibility(View.GONE);
        mSrlViewRecommendedBooks.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        mPresenter.initLoadRecommendedBooksContent();
    }
}
