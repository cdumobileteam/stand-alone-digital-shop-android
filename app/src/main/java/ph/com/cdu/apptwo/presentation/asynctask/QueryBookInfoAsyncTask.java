package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 26/10/2017.
 */

public class QueryBookInfoAsyncTask extends AsyncTask<Void, Void, List<BookInfo>> {
    private Context mContext;
    private CallBackTask mCallBackTask;

    public QueryBookInfoAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<BookInfo> doInBackground(Void... params) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        return databaseInteractor.loadAllBookInfo(mContext);
    }

    @Override
    protected void onPostExecute(List<BookInfo> bookInfos) {
        super.onPostExecute(bookInfos);
        mCallBackTask.callBack(bookInfos);
    }

    public void setOnCallBack(CallBackTask _cbj) {
        mCallBackTask = _cbj;
    }

    public interface CallBackTask {
        void callBack(List<BookInfo> result);
    }
}
