package ph.com.cdu.apptwo.data.database;

import android.content.Context;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.domain.repository.BookShopRepository;

/**
 * Created by Patrick Santos on 09/02/2018.
 */

public class BookShopRepositoryImpl implements BookShopRepository {

    private AppDatabase mAppDatabase;

    public BookShopRepositoryImpl(Context context) {
        mAppDatabase = AppDatabase.getDatabase(context);
    }

    @Override
    public List<BookShop> getALLBook() {
        return mAppDatabase.bookShopDao().selectAllBooks();
    }

    @Override
    public Long insert(BookShop bookShop) {
        return mAppDatabase.bookShopDao().insertBook(bookShop);
    }

    @Override
    public void delete() {
        mAppDatabase.bookShopDao().deleteAllBooks();
    }

    @Override
    public List<BookShop> getBookCategory(String bookCategory) {
        return mAppDatabase.bookShopDao().selectBookByCategory(bookCategory);
    }

    @Override
    public List<String> getBookTitle(String bookTitle) {
        return mAppDatabase.bookShopDao().selectBookByTitle(bookTitle);
    }

    @Override
    public List<BookShop> getBookInfo(String bookTitle) {
        return mAppDatabase.bookShopDao().selectBookInfo(bookTitle);
    }


}
