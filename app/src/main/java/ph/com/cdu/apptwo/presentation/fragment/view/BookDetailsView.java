package ph.com.cdu.apptwo.presentation.fragment.view;

import java.util.Date;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public interface BookDetailsView {
    void setUpListeners();

    void getContentDetails();

    void setFreeBookDetails();

    void initBookPurchase();

    void insertBookDetails(String bookId, CharSequence bookTitle, String bookImageUrl, String bookURLDownload, Date date);

    void checkBookStatus();

    void onBindService();

    void setService();

    void getPurchases();

    void setPurchases();

    void consumePurchases(String purchaseToken);

    void setBookTransaction();

    void setBookDetails(String book_title, String book_id);

    void setBookConfirmation();

    void onBookConfirmationDone();

    void goAnalytics();

    void initButtonPrice(String status, int color, boolean function);

    void removeDownloadQueue();

    void onValidationSuccessful(String filePath, String id, String userId);
}
