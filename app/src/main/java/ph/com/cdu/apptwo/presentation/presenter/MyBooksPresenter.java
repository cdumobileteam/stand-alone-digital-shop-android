package ph.com.cdu.apptwo.presentation.presenter;

import android.net.Uri;

import java.io.File;
import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.MyBooks;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public interface MyBooksPresenter {

    void onCreate();

    void initMyBooks(String user_id_fk, List<MyBooks> cShopBooks);

    void onTappedButton();

    void onTappedOpenPDFReader(int pid, String userId, String id, String title, Uri uri);

    void onView(int uniqueId, File filePath, Uri uriPath, String mime);

    void initAnalytics();
}
