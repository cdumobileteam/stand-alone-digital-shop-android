package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;

import java.util.Date;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.requestParameters.MyBooksParams;
import ph.com.cdu.apptwo.data.rest.requestParameters.PaymentParams;
import ph.com.cdu.apptwo.data.rest.requestParameters.PurchaseNewBookParams;
import ph.com.cdu.apptwo.data.rest.response.MyBooksResponse;
import ph.com.cdu.apptwo.data.rest.response.PaymentResponse;
import ph.com.cdu.apptwo.data.rest.response.PurchaseNewBookResponse;
import ph.com.cdu.apptwo.domain.interactor.MyBooksInteractor;
import ph.com.cdu.apptwo.domain.interactor.MyBooksInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.PaymentInteractor;
import ph.com.cdu.apptwo.domain.interactor.PaymentInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.PurchaseNewBookInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.PurchaseNewBooksInteractor;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteBookInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.fragment.view.BookDetailsView;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public class BookDetailsPresenterImpl implements BookDetailsPresenter, PurchaseNewBooksInteractor.onPurchaseNewBooksDataLoader,
        PaymentInteractor.onPaymentDataLoadedListener, MyBooksInteractor.onMyBooksOnDataListener{

    private Context mContext;
    private DataDownloader mSharedPreferences;
    private BookDetailsView mBookDetailsView;
    private MyBooksInteractor mMyBooksInteractor;
    private PurchaseNewBooksInteractor mPurchaseNewBooksInteractor;
    private PaymentInteractor mPaymentInteractor;
    private PaymentParams mPaymentParams;

    public BookDetailsPresenterImpl(Context context, BookDetailsView bookDetailsView) {
        mContext = context;
        mBookDetailsView = bookDetailsView;
        mSharedPreferences = new DataDownloader(mContext);
        mMyBooksInteractor = new MyBooksInteractorImpl();
        mPurchaseNewBooksInteractor = new PurchaseNewBookInteractorImpl();
        mPaymentInteractor = new PaymentInteractorImpl();
    }

    @Override
    public void onCreate() {
        mBookDetailsView.setUpListeners();
        mBookDetailsView.getContentDetails();
    }

    @Override
    public void onDataLoad() {
        mBookDetailsView.setFreeBookDetails();
    }

    @Override
    public void onSuccess() {
        mBookDetailsView.setService();
    }

    @Override
    public void onReDownload() {
        mBookDetailsView.removeDownloadQueue();
    }

    @Override
    public void onCheckPurchaseHistory() {
        mBookDetailsView.getPurchases();
    }

    @Override
    public void onPurchaseTapped() {
        mBookDetailsView.setPurchases();
    }

    @Override
    public void initConsumePurchase(String purchaseToken) {
        mBookDetailsView.consumePurchases(purchaseToken);
    }

    @Override
    public void initBindService() {
        mBookDetailsView.onBindService();
    }

    @Override
    public void setUpButtonPrice(String status, int color, boolean function) {
        mBookDetailsView.initButtonPrice(status,color,function);
    }

    @Override
    public void onDownloadTapped() {
        mBookDetailsView.initBookPurchase();
    }

    @Override
    public void initBookStatus() {
        mBookDetailsView.setBookTransaction();
    }

    @Override
    public void onGooglePlayPayment(String userId, String sku, String transactionId,
                                    String orderId, String bookId, CharSequence bookTitle,
                                    String bookImage, String bookURL) {
        mPaymentParams = new PaymentParams(userId, sku, transactionId, orderId, bookId,
                bookTitle.toString(), bookImage, bookURL);
        mPaymentInteractor.onRequestPayment(this,mContext,mPaymentParams);
    }

    @Override
    public void onCheckUserTransaction() {
        MyBooksParams myBooksParams = new MyBooksParams(mSharedPreferences.getUserId());
        mMyBooksInteractor.onRequestMyBooks(this,mContext,myBooksParams);
    }

    @Override
    public void onTransactionDone(String bookId, CharSequence bookTitle, String bookImageUrl,
                                  String bookURLDownload, Date date, String bookPrice) {
        mBookDetailsView.insertBookDetails(bookId, bookTitle, bookImageUrl, bookURLDownload, date);
        PurchaseNewBookParams purchaseNewBookParams = new PurchaseNewBookParams("0", mSharedPreferences.getUserId(),
                bookId, bookTitle.toString(), bookImageUrl, bookURLDownload, "", bookPrice);
        mPurchaseNewBooksInteractor.requestPurchaseNewBook(this,mContext,purchaseNewBookParams);
    }

    @Override
    public void onSuccess(PurchaseNewBookResponse purchaseNewBookResponse) {

    }

    @Override
    public void onSuccess(PaymentResponse paymentData) {

    }

    @Override
    public void onMiss(int uniqueId) {
        DeleteBookInfoAsyncTask deleteBookInfoAsyncTask = new DeleteBookInfoAsyncTask(mContext,uniqueId);
        deleteBookInfoAsyncTask.execute();
    }

    @Override
    public void validateLocalFile(String id, String userId) {
        mBookDetailsView.onValidationSuccessful(mContext.getString(R.string.app_root_directory),id,userId);
    }

    @Override
    public void onSuccess(MyBooksResponse myBooksData) {
        if (myBooksData.getResult() != null) {
            mBookDetailsView.setBookConfirmation();
            for (int i = 0; i < myBooksData.getResult().size(); i++) {
                mBookDetailsView.setBookDetails(myBooksData.getResult().get(i).getBook_title(),
                        myBooksData.getResult().get(i).getBook_id());
                mBookDetailsView.checkBookStatus();
            }
        } else {
            mBookDetailsView.onBookConfirmationDone();
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void initAnalytics() {
        mBookDetailsView.goAnalytics();
    }

}
