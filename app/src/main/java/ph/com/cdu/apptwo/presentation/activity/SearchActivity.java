package ph.com.cdu.apptwo.presentation.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.data.rest.requestParameters.CShopBooksParams;
import ph.com.cdu.apptwo.domain.model.books.SearchBooks;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.presentation.activity.view.SearchView;
import ph.com.cdu.apptwo.presentation.adapter.BookInfoAdapter;
import ph.com.cdu.apptwo.presentation.fragment.BookDetailsFragment;
import ph.com.cdu.apptwo.presentation.presenter.SearchPresenter;
import ph.com.cdu.apptwo.presentation.presenter.SearchPresenterImpl;
import ph.com.cdu.apptwo.presentation.util.PermissionChecker;
import ph.com.cdu.apptwo.util.Logger;

public class SearchActivity extends AppCompatActivity implements SearchView, View.OnClickListener {

    private Bundle mBundle;
    private Context mContext;
    private PermissionChecker mPermissionChecker;
    private CShopBooksParams mNewBooksParams;
    private BookInfoAdapter mAdapter;
    List<SearchBooks> mSearchBooks = new ArrayList<>();
    private Fragment mFragment;
    private Snackbar mSnackbar;
    private Logger mLogger;
    private LinearLayoutManager mLinearLayoutManager;
    private FragmentManager mFragmentManager;
    private SearchPresenter mPresenter;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.imageViewLoader)
    ImageView mImgViewLoader;
    @BindView(R.id.imageButtonBack)
    ImageButton mImgButtonBack;
    @BindView(R.id.frameLayoutBookSearch)
    FrameLayout mFrmLayoutBookSearch;
    @BindView(R.id.recyclerViewBookSearch)
    RecyclerView mRcyclrViewBookSearch;
    @BindView(R.id.linearLayoutSearchFilter)
    LinearLayout mLnrLayoutSearchFilter;
    @BindView(R.id.editTextBookTitle)
    EditText mEdtTextBookTitle;
    @BindView(R.id.buttonBookSearch)
    Button mBtnBookSearch;
    @BindView(R.id.linearLayoutIndicator)
    LinearLayout mLnrLayoutIndicator;
    @BindView(R.id.textViewSearchResult)
    TextView mTxtViewSearchResult;
    @BindView(R.id.linearLayoutTitle)
    LinearLayout mLnrLayoutTitle;
    @BindView(R.id.textViewBookTitle)
    TextView mTxtViewBookTitle;
    @BindView(R.id.textViewBookPages)
    TextView mTxtViewBookPages;
    SearchBooks mResultBooks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        mContext = this;
        mPresenter = new SearchPresenterImpl(mContext, this);
        mFragmentManager = getSupportFragmentManager();
        mPermissionChecker = new PermissionChecker(mContext);
        mAdapter = new BookInfoAdapter(mContext,this,mSearchBooks);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcyclrViewBookSearch.setAdapter(mAdapter);
        mRcyclrViewBookSearch.setLayoutManager(mLinearLayoutManager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.initAnalytics();
        mPresenter.onCreate();
    }

    @Override
    public void setUpSearchParams() {
        mBundle = new Bundle();
        mNewBooksParams = new CShopBooksParams();
        mFragmentManager = getSupportFragmentManager();
        mImgButtonBack.setOnClickListener(this);
        mBtnBookSearch.setOnClickListener(this);
    }

    @Override
    public void goToBookDetails(SearchBooks bookShop) {
        if (!mPermissionChecker.readAndWriteExternalStorage()){
            mResultBooks = bookShop;
            return;
        }
        mFragment = new BookDetailsFragment();
        mBundle.putString(getString(R.string.book_content),
                getString(R.string.params_search_category));
        mBundle.putParcelable(getString(R.string.params_recommended_books_content),
                mNewBooksParams.getBooksDetails(mContext, bookShop));
        mPresenter.onTappedBook(mBundle, mFragment);
        mFrmLayoutBookSearch.setVisibility(View.VISIBLE);
    }

    private void showResult() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mRcyclrViewBookSearch.getWindowToken(), 0);
    }

    @Override
    public void setUpSnackBar() {
        mSnackbar = Snackbar.make(findViewById(android.R.id.content),
                R.string.alert_no_connection,
                Snackbar.LENGTH_LONG);
    }

    @Override
    public void showNoConnection() {
        mSnackbar.show();
    }

    @Override
    public void initSearchView(String bookTitle) {
        int currentSize = mSearchBooks.size();
        mSearchBooks.clear();
        mAdapter.notifyItemRangeRemoved(0, currentSize);
        showResult();
//        mBundle.putString(getString(R.string.params_book_title), bookTitle);
//        mFragment = new SearchFragment();
        mPresenter.onTappedButton(bookTitle, mFragment);
        mLogger.logBookSearch(bookTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBookSearch:
                mPresenter.onTappedSearch(mEdtTextBookTitle.getText().toString());
                break;
            case R.id.imageButtonBack:
                mPresenter.onTappedBack();
                break;
        }
    }

    @Override
    public void makeArguments(Bundle bundle, Fragment fragment) {
        mFragment.setArguments(bundle);
        mFragmentManager.beginTransaction().addToBackStack(getString(R.string.title_activity_search))
                .setCustomAnimations(R.anim.fade_in,0)
                .replace(R.id.frameLayoutBookSearch,fragment)
                .commitAllowingStateLoss();
        initSearchResults(View.VISIBLE,View.GONE,View.GONE,View.GONE,View.GONE);
    }

    @Override
    public void initSearchResults(int toolbar, int filter, int result, int search, int count) {
        mToolbar.setVisibility(toolbar);
        mLnrLayoutSearchFilter.setVisibility(filter);
        mLnrLayoutTitle.setVisibility(result);
        mRcyclrViewBookSearch.setVisibility(search);
        mLnrLayoutIndicator.setVisibility(count);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPresenter.initMain();
    }

    @Override
    public void countBooksAvailable(int size) {
            mPresenter.setUpFind(size + " " + getResources().getQuantityText(R.plurals.number_of_results, size));
    }

    @Override
    public void showMessage() {
        initSearchResults(View.VISIBLE,View.VISIBLE,View.GONE,View.GONE,View.GONE);
    }

    @Override
    public void goToBookAvailability(String bookTitle) {

//        mPresenter.onTappedTitle(objectList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    goToBookDetails(mResultBooks);
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    showMessageOk(getString(R.string.text_allow_access_storage),null);
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void setUpBookDetails(FreeBooks bookShops, String search) {
//        mAdapter = new BookInfoAdapter(mContext,this,new ArrayList<BookShop>(0));
        SearchBooks searchBooks = new SearchBooks(bookShops.getId(),bookShops.getTitle(),
                bookShops.getAuthor(),bookShops.getGenre(),bookShops.getDescription(),bookShops.getImage_url(),
                bookShops.getBook_url(),bookShops.getPrice());
        mSearchBooks.add(searchBooks);
        mAdapter.notifyItemRangeInserted(0, mSearchBooks.size());
        mAdapter.notifyDataSetChanged();
//        mAdapter.updateSearch(newBooks, search);
    }

    @Override
    public void initBackButton() {
        if (mFrmLayoutBookSearch.isShown()) {
            mFrmLayoutBookSearch.setVisibility(View.GONE);
            initSearchResults(View.VISIBLE, View.VISIBLE, View.GONE, View.VISIBLE,View.GONE);
        } else {
            onBackPressed();
        }
    }

    @Override
    public void setMainTitle(String title, String pages) {
        initSearchResults(View.VISIBLE, View.GONE, View.VISIBLE, View.GONE, View.GONE);
        mTxtViewBookTitle.setText(title);
        mTxtViewBookPages.setText(pages);
    }

    @Override
    public void showMessageOk(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.text_ok, okListener)
                .create()
                .show();
    }

    @Override
    public void onTapped(String status) {
        if (status != null) {
            mSnackbar.setText(status);
            mSnackbar.show();
            mSnackbar.setAction(null, null);
        }
    }

    @Override
    public void showFindResult(CharSequence message) {
        mTxtViewSearchResult.setText(message);
    }

    @Override
    public void goToMain() {
        mImgViewLoader.setVisibility(View.VISIBLE);
        initSearchResults(View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);
        finish();
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }

    @Override
    public void goAnalytics() {
        mLogger = new Logger(this);
    }
}
