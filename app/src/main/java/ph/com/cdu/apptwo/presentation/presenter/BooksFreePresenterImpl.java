package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksFreeView;
import ph.com.cdu.apptwo.data.rest.response.FreeBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.BooksFreeInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksFreeInteractorImpl;
import ph.com.cdu.apptwo.util.NetworkConnection;

import static android.view.View.GONE;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksFreePresenterImpl implements BooksFreePresenter, BooksFreeInteractor.onBooksFreeDataLoadedListener {

    private Context mContext;
    private BooksFreeView mBooksFreeView;
    private BooksFreeInteractor mBooksFreeInteractor;

    public BooksFreePresenterImpl(Context context, BooksFreeView booksFreeView) {
        mContext = context;
        mBooksFreeView = booksFreeView;
        mBooksFreeInteractor = new BooksFreeInteractorImpl();
    }

    @Override
    public void onCreate() {
        mBooksFreeView.setUpListeners();
        mBooksFreeView.setUpRecyclerViewDeck();
    }

    @Override
    public void onConnected() {
        mBooksFreeView.getBooks();
    }

    @Override
    public void initFreeBooks() {
        mBooksFreeView.onInitFreeBooksContent();
    }

    @Override
    public void initLinearLayoutManagerDeck() {
        mBooksFreeView.setUpLayoutManagerDeck();
    }

    @Override
    public void onSuccess(FreeBooksResponse freeBooksData) {
        for (int i = mContext.getResources().getInteger(R.integer.initial_value);
             i<freeBooksData.getResult().size(); i++) {
            mBooksFreeView.onStoreFreeBookContent(freeBooksData.getResult());
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void initLoadFreeBooksContent() {
        mBooksFreeInteractor.requestFreeBooks(this, mContext);
    }

    @Override
    public void onFinish() {
        mBooksFreeView.setUpBookViews();
    }
}
