package ph.com.cdu.apptwo.presentation.activity.view;

import com.google.android.gms.plus.model.people.Person;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.FreeBooks;

/**
 * Created by Neil Cruz on 17/10/2017.
 */

public interface SplashScreenView {
    void setUpListeners();

    void initFacebookSdk();

    void setUserInfo(String socialApp, String userId, String email, String firstName, String lastName, String gender, String profilePicUri);

    void initGoogleSdk();

    void checkGooglePlusStatus();

    void setOtherInfo(Person mOtherInfo);

    void checkFacebookStatus();

    void validateLogin();

    void setUpSnackBars();

    void onNewUserAccount(int responseCode);

    void initUserAccount();

    void onLoadRecommendedBooksContent(List<FreeBooks> recommended);

    void onLoadFreeBooksContent(List<FreeBooks> free);

    void onLoadPaidBooksContent(List<FreeBooks> paid);

    void onLoadTopSellingContent(List<FreeBooks> top);

    void onLoadNewContent(List<FreeBooks> latest);

    void getCshop();

    void goAnalytics();

    void onFailed(String errorMessage);
}
