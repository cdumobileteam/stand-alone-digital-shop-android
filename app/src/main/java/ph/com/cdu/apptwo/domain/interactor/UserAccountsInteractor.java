package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.UserAccountsResponse;

/**
 * Created by Neil Cruz on 17/01/2018.
 */

public interface UserAccountsInteractor {
    interface onUserAccountsDataLoadedListener {
        void onSuccess(UserAccountsResponse userAccountsData);

        void onFailed();
    }

    void requestUserAccount(onUserAccountsDataLoadedListener listener, Context context);
}
