package ph.com.cdu.apptwo.presentation.presenter;

import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.model.people.Person;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.books.NewBooks;
import ph.com.cdu.apptwo.domain.model.books.PaidBooks;
import ph.com.cdu.apptwo.domain.model.books.RecommendedBooks;
import ph.com.cdu.apptwo.domain.model.books.TopSellingBooks;

/**
 * Created by Neil Cruz on 17/10/2017.
 */

public interface SplashScreenPresenter {

    void onCreate();

    void setFacebookData(LoginResult loginResult);

    void onLoginGooglePlus(GoogleApiClient mGoogleApiClient);

    void onTappedLoginGooglePlus();

    void setGooglePlusData(GoogleSignInAccount mGoogleAccount, Person personalInfo);

    void onLoginFacebook();

    void onSuccessLogin(GoogleApiClient mGoogleApiClient);

    void onTappedLoginFacebook();

    void onSocialPluginLaunch();

    void onColdSignedIn();

    void storeRecommendedBooks(List<FreeBooks> result);

    void storeFreeBooks(List<FreeBooks> result);

    void storePaidBooks(List<FreeBooks> result);

    void storeTopSellingBooks(List<FreeBooks> result);

    void storeNewBooks(List<FreeBooks> result);

    void onReCreate();

    void setCShop();

    void initAnalytics();

}
