package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.RecommendedBooksResponse;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksRecommendedInteractor {
    interface onBooksRecommendedDataLoadedListener {
        void onSuccess(RecommendedBooksResponse recommendedBooksData);

        void onFailed();
    }

    void requestRecommendedBooks(onBooksRecommendedDataLoadedListener listener, Context context);
}
