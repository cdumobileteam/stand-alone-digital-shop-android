package ph.com.cdu.apptwo.domain.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

@Entity(tableName = "userinfo")
public class UserInfo {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "user_id")
    private String id;
    @ColumnInfo(name = "user_email")
    private String email;
    @ColumnInfo(name = "user_fname")
    private String firstName;
    @ColumnInfo(name = "user_lname")
    private String lastName;
    @ColumnInfo(name = "gender")
    private String gender;
    @ColumnInfo(name = "profilePic")
    private String profilePicUri;
    @ColumnInfo(name = "socialAccount")
    private String socialAccount;

    public UserInfo(String id, String email, String firstName, String lastName, String gender, String profilePicUri, String socialAccount) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.profilePicUri = profilePicUri;
        this.socialAccount = socialAccount;
    }

    @Ignore
    public UserInfo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfilePicUri() {
        return profilePicUri;
    }

    public void setProfilePicUri(String profilePicUri) {
        this.profilePicUri = profilePicUri;
    }

    public String getSocialAccount() {
        return socialAccount;
    }

    public void setSocialAccount(String socialAccount) {
        this.socialAccount = socialAccount;
    }
}
