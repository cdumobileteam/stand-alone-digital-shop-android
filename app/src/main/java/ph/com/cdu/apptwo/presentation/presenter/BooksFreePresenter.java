package ph.com.cdu.apptwo.presentation.presenter;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public interface BooksFreePresenter {

    void onCreate();

    void initFreeBooks();

    void initLinearLayoutManagerDeck();

    void initLoadFreeBooksContent();

    void onFinish();

    void onConnected();
}
