package ph.com.cdu.apptwo.presentation.util;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Patrick Santos on 06/04/2018.
 */

public class TimerUtil {

    private Thread backgroundThread;
    private boolean run;
    private int bytes_downloaded;
    private int bytes_total;
    private int dl_progress;
    private Cursor mCursor;
    private Timer mTimer;
    private Context mContext;
    private DownloadManager.Query mQuery;
    private DownloadManager mDownloadManager;

    public TimerUtil(Context context) {
        mTimer = new Timer();
        this.mContext = context;
        mDownloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public void startTimer(final Long downloadedFileID) {
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (run) {
                    mQuery = new DownloadManager.Query();
                    mQuery.setFilterById(downloadedFileID);
                    mCursor = mDownloadManager.query(mQuery);
                    mCursor.moveToFirst();
                    try {
                        bytes_downloaded = mCursor.getInt(mCursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                        bytes_total = mCursor.getInt(mCursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                        mCursor.close();
                        dl_progress = (bytes_downloaded * 100 / bytes_total);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Log.d("progress: ", dl_progress + "");
//                            }
//                        });
                    } catch (Exception e) {
//                        Log.d("error: ", e.toString());
                    }
                } else {
                    mTimer.cancel();
                    mTimer.purge();
                }
            }

        }, 0, 10);

    }

    public void initTimer(boolean b) {
        run = b;
    }

    public void enqueueDownload() {
        backgroundThread = Thread.currentThread();
        synchronized (Thread.currentThread()) {
            try {
                Thread.currentThread().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void startDownload() {
        try {
            synchronized (backgroundThread) {
                backgroundThread.notify();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
