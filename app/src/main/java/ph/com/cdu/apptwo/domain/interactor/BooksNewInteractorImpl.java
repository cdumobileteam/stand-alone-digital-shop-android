package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.response.NewBooksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public class BooksNewInteractorImpl implements BooksNewInteractor {

    private CDUService mService;

    @Override
    public void requestNewBooks(final onBooksNewDataLoadedListener listener, Context context) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_BOOKS);

        mService.getNewBooksContent().enqueue(new Callback<NewBooksResponse>() {
            @Override
            public void onResponse(Call<NewBooksResponse> call, Response<NewBooksResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<NewBooksResponse> call, Throwable t) {
                    listener.onFailed();
            }
        });

    }
}
