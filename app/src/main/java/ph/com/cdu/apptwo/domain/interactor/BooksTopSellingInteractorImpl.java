package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.response.TopSellingBooksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public class BooksTopSellingInteractorImpl implements BooksTopSellingInteractor {

    private CDUService mService;

    @Override
    public void requestTopSellingBooks(final onBooksTopSellingDataLoadedListener listener, Context context) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_BOOKS);

        mService.getTopSellingBooksContent().enqueue(new Callback<TopSellingBooksResponse>() {
            @Override
            public void onResponse(Call<TopSellingBooksResponse> call, Response<TopSellingBooksResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<TopSellingBooksResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
