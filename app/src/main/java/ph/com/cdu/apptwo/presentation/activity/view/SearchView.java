package ph.com.cdu.apptwo.presentation.activity.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import ph.com.cdu.apptwo.domain.model.books.SearchBooks;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public interface SearchView {

    void goToBookDetails(SearchBooks bookShop);

    void initSearchView(String bookTitle);

    void setUpSearchParams();

    void makeArguments(Bundle bundle, Fragment fragment);

    void initSearchResults(int toolbar, int filter, int result, int search, int count);

    void countBooksAvailable(int size);

    void showMessage();

    void setUpSnackBar();

    void goToBookAvailability(String bookTitle);

    void setUpBookDetails(FreeBooks bookShops, String search);

    void initBackButton();

    void setMainTitle(String title, String pages);

    void onTapped(String status);

    void showNoConnection();

    void goToMain();

    void showFindResult(CharSequence message);

    void goAnalytics();

    void showMessageOk(String message, DialogInterface.OnClickListener listener);
}
