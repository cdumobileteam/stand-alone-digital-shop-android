package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.Date;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 26/10/2017.
 */

public class InsertBookInfoAsyncTask extends AsyncTask<BookInfo, Void, Long> {
    private Context mContext;
    private String mBookId;
    private String mUserId, mBookImageUrl;
    private String mBookURLDownload;
    private CharSequence mBookTitle;
    private Date mBookTransactionDate;
    private BookInfo mBookDetails;

    public InsertBookInfoAsyncTask(Context mContext, String bookId, String userId,
                                   CharSequence bookTitle, String bookImageUrl, String bookURLDownload, Date date) {
        mBookDetails = new BookInfo();
        this.mContext = mContext;
        this.mBookId = bookId;
        this.mUserId = userId;
        this.mBookTitle = bookTitle;
        this.mBookImageUrl = bookImageUrl;
        this.mBookURLDownload = bookURLDownload;
        this.mBookTransactionDate = date;
    }

    @Override
    protected Long doInBackground(BookInfo... params) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        mBookDetails.setId(mBookId);
        mBookDetails.setUserId(mUserId);
        mBookDetails.setTitle(String.valueOf(mBookTitle));
        mBookDetails.setImageUrl(mBookImageUrl);
        mBookDetails.setUrlDownload(String.valueOf(mBookURLDownload));
        mBookDetails.setTransactionDate(String.valueOf(mBookTransactionDate));
        return databaseInteractor.insertBookInfo(mContext, mBookDetails);
    }

    private interface  CallBackTask {

    }
}
