package ph.com.cdu.apptwo.domain.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Patrick Santos on 09/02/2018.
 */

@Entity(tableName = "bookdirectory")
public class BookShop {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "unique_id")
    private int pid;
    @ColumnInfo(name = "book_id")
    private String id;
    @ColumnInfo(name = "book_title")
    private String title;
    @ColumnInfo(name = "book_author")
    private String author;
    @ColumnInfo(name = "book_genre")
    private String genre;
    @ColumnInfo(name = "book_description")
    private String description;
    @ColumnInfo(name = "book_image")
    private String image_url;
    @ColumnInfo(name = "book_url")
    private String book_url;
    @ColumnInfo(name = "book_price")
    private String price;
    @ColumnInfo(name = "book_category")
    private String category;

    public BookShop(int pid, String id, String title, String author, String genre, String description, String image_url, String book_url, String price, String category) {
        this.pid = pid;
        this.id = id;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.description = description;
        this.image_url = image_url;
        this.book_url = book_url;
        this.price = price;
        this.category = category;
    }

    @Ignore
    public BookShop() {
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getBook_url() {
        return book_url;
    }

    public void setBook_url(String book_url) {
        this.book_url = book_url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
