package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Patrick Santos on 09/02/2018.
 */

public class GetBookCategoryAsyncTask extends AsyncTask<String, Void, List<BookShop>> {
    private Context mContext;
    private CallBackTask mCallBackTask;

    public GetBookCategoryAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<BookShop> doInBackground(String... strings) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        return databaseInteractor.getAllBooksByCategory(mContext, strings[0]);
    }

    @Override
    protected void onPostExecute(List<BookShop> bookShops) {
        super.onPostExecute(bookShops);
        mCallBackTask.callback(bookShops);
    }

    public void setOnCallBack(CallBackTask ctx) {
        mCallBackTask = ctx;
    }

    public interface CallBackTask {
        void callback(List<BookShop> bookShops);
    }
}
