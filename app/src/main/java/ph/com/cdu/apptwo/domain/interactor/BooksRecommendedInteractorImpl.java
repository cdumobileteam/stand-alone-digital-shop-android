package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.response.RecommendedBooksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksRecommendedInteractorImpl implements BooksRecommendedInteractor {

    private CDUService mService;

    @Override
    public void requestRecommendedBooks(final onBooksRecommendedDataLoadedListener listener, Context context) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_BOOKS);

        mService.getRecommendedBooksContent().enqueue(new Callback<RecommendedBooksResponse>() {
            @Override
            public void onResponse(Call<RecommendedBooksResponse> call, Response<RecommendedBooksResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<RecommendedBooksResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
