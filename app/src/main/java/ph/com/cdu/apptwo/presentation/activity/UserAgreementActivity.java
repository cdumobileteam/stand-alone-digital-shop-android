package ph.com.cdu.apptwo.presentation.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;

public class UserAgreementActivity extends Activity implements View.OnClickListener {

//    @BindView(R.id.webViewPrivacyPolicy)
//    WebView mWebViewPrivacyPolicy;
    @BindView(R.id.textViewUserAgreement)
    TextView mTxtViewUserAgreement;
    @BindView(R.id.imageButtonBack)
    ImageButton mImgButtonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_agreement);
        ButterKnife.bind(this);
        mImgButtonBack.setOnClickListener(this);
        mTxtViewUserAgreement.setText(getString(R.string.user_agreement_title));
//        mWebViewPrivacyPolicy.setWebChromeClient(new PrivacyPolicyWebChromeClient());
//        mWebViewPrivacyPolicy.setWebViewClient(new PrivacyPolicyWebClient());
//        WebSettings settings = mWebViewPrivacyPolicy.getSettings();
//        settings.setJavaScriptEnabled(true);
//        mWebViewPrivacyPolicy.loadData("content://downloads/all_downloads/307","application/pdf","UTF-8");
//        mWebViewPrivacyPolicy.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }

    @Override
    public void onClick(View v) {
        onBackPressed();
    }

    //    private class PrivacyPolicyWebChromeClient extends WebChromeClient {
//
//    }
//
//    private class PrivacyPolicyWebClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//            return false;
//        }
//
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            super.onPageStarted(view, url, favicon);
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//        }
//    }
}
