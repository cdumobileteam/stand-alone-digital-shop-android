package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;

/**
 * Created by Patrick Santos on 08/02/2018.
 */

public class QueryUserInfoAsyncTask extends AsyncTask<Void, Void, List<UserInfo>> {
    private Context mContext;
    private CallBackTask mCallBackTask;

    public QueryUserInfoAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected List<UserInfo> doInBackground(Void... voids) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        return databaseInteractor.loadAllUserInfo(mContext);
    }

    @Override
    protected void onPostExecute(List<UserInfo> userInfos) {
        super.onPostExecute(userInfos);
        mCallBackTask.callback(userInfos);
    }

    public void setOnCallBack(CallBackTask ctx) {
        mCallBackTask = ctx;
    }

    public interface CallBackTask {
        void callback(List<UserInfo> result);
    }
}
