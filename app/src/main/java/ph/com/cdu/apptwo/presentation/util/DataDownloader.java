package ph.com.cdu.apptwo.presentation.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ph.com.cdu.apptwo.BuildConfig;
import ph.com.cdu.apptwo.R;

/**
 * Created by Neil Cruz on 19/01/2018.
 */

public class DataDownloader {

    public Context mContext;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public DataDownloader(Context context) {
        mContext = context;
    }

    public int getFirstTimeRun() {
        preferences = mContext.getSharedPreferences(mContext.getPackageName(), mContext.getResources().getInteger(R.integer.app_first_install));
        int result, currentVersionCode = BuildConfig.VERSION_CODE;
        int lastVersionCode = preferences.getInt(mContext.getString(R.string.app_first_install),
                mContext.getResources().getInteger(R.integer.app_undetermined));
        if (lastVersionCode == mContext.getResources().getInteger(R.integer.app_undetermined)) result =
                mContext.getResources().getInteger(R.integer.app_first_install); else
            result = (lastVersionCode == currentVersionCode) ? mContext.getResources().getInteger(R.integer.app_first_run) :
                    mContext.getResources().getInteger(R.integer.app_update);
        preferences.edit().putInt(mContext.getString(R.string.app_first_install), currentVersionCode).apply();
        return result;
    }

    public void setUserAccount(String userId, String name, String email, String social) {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
        editor.putString(mContext.getString(R.string.params_userId),userId);
        editor.putString(mContext.getString(R.string.params_name), name);
        editor.putString(mContext.getString(R.string.params_email), email);
        editor.putString(mContext.getString(R.string.params_social), social);
        editor.apply();
    }

    public void setBookDownloaded(String bookId) {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
        editor.putString(mContext.getString(R.string.params_book_id),bookId);
        editor.apply();
    }

    public void setDownloadedFileID(long downloadedFileID) {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
        editor.putLong(mContext.getString(R.string.params_downloaded_file_id),downloadedFileID);
        editor.apply();
    }

    public String getUserId() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(mContext.getString(R.string.params_userId), "");
    }

    public String getUserName() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(mContext.getString(R.string.params_name), "");
    }

    public String getUserEmail() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(mContext.getString(R.string.params_email), "");
    }

    public String getSocial() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(mContext.getString(R.string.params_social), "");
    }

    public String getBookDownloaded() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(mContext.getString(R.string.params_book_id), "");
    }

    public Long getDownloadedFileID() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getLong(mContext.getString(R.string.params_downloaded_file_id), 0);
    }

    public void logOutUser() {
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
