package ph.com.cdu.apptwo.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.presentation.activity.view.SearchView;
import ph.com.cdu.apptwo.util.NetworkConnection;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Patrick Santos on 14/02/2018.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private Context mContext;
    private SearchView mSearchView;
    private List<BookShop> mBookShop;
    private NetworkConnection mInternet;

    public SearchAdapter(Context context, SearchView searchView, List<BookShop> bookShop) {
        mContext = context;
        mSearchView = searchView;
        mBookShop = bookShop;
        mInternet = new NetworkConnection(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_search, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        BookShop bookShop = mBookShop.get(position);
        holder.mTxtViewBookTitle.setText(bookShop.getTitle());
//        Picasso.with(mContext).load(bookShop).into(holder.mImgButtonBookImage, new Callback() {
//            @Override
//            public void onSuccess() {
//                holder.mImgViewPlaceholder.setVisibility(GONE);
//            }
//
//            @Override
//            public void onError() {
//
//            }
//        });
    }

    public void updateSearch(List<BookShop> bookShop) {
        mBookShop = bookShop;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mBookShop != null) {
            mSearchView.countBooksAvailable(mBookShop.size());
            return mBookShop.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

//        @BindView(R.id.imageButtonBookImage)
//        ImageButton mImgButtonBookImage;
//        @BindView(R.id.imageViewPlaceholder)
//        ImageView mImgViewPlaceholder;
        @BindView(R.id.textViewBookTitle)
        TextView mTxtViewBookTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            mImgButtonBookImage.setOnClickListener(this);
            mTxtViewBookTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            BookShop bookShop = getBookDetails(getAdapterPosition());
            if (checkConnection()) {
//                mSearchView.goToBookAvailability(bookShop);
//                mSearchView.goToBookDetails(bookShop);
            } else {
                mSearchView.showNoConnection();
            }
        }

        private boolean checkConnection() {
            if (mInternet.hasActiveConnection(mContext.getString(R.string.url_digitalshop2))) {
                mSearchView.initSearchResults(GONE,GONE,GONE,VISIBLE,VISIBLE);
                return true;
            } else {
                mSearchView.initSearchResults(VISIBLE,VISIBLE,VISIBLE,VISIBLE,VISIBLE);
            }
            return false;
        }
    }

    private BookShop getBookDetails(int position) {
    return mBookShop.get(position);
    }
}
