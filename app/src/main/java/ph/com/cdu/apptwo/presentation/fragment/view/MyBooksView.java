package ph.com.cdu.apptwo.presentation.fragment.view;

import android.net.Uri;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.books.MyBooks;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public interface MyBooksView {

    void setUpRecyclerView();

    void onLoadMyBooksContent(List<BookInfo> bookInfo, String user_id_fk);

    void openBook(BookInfo myBooks);

    void onLoadUserTransactions(int response_code, List<MyBooks> userBooks);

    void showBookStatus(boolean hasNetwork);

    void setUpListeners();

    void showMyCShopBooks(List<MyBooks> cShopBooks, String user_id_fk, List<BookInfo> myBooks);

    void getBook(MyBooks myBooks);

    void viewBook(int id, String userId, String bookId,String title, String uri);

    void unregisterReceiver();

    void initReader(int pid, String userId, String id, String title, Uri uri);

    void goToNetworkConnection();

    void goToViewBook(Uri uri, String mime);

    void onViewBookFailed();

    void goAnalytics();
}
