package ph.com.cdu.apptwo.data.database;

import android.content.Context;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.domain.repository.BookInfoRepository;

/**
 * Created by Neil Cruz on 25/10/2017.
 */

public class BookInfoRepositoryImpl implements BookInfoRepository {

    private AppDatabase mAppDatabase;

    public BookInfoRepositoryImpl(Context context) {
        mAppDatabase = AppDatabase.getDatabase(context);
    }

    @Override
    public List<BookInfo> getALLBookInfo() {
        return mAppDatabase.bookInfoDao().selectAllBookInfo();
    }

    @Override
    public List<BookInfo> getUserBooks(String userId) {
        return mAppDatabase.bookInfoDao().selectBookInfoById(userId);
    }

    @Override
    public Long insert(BookInfo bookInfo) {
        return mAppDatabase.bookInfoDao().insertBookInfo(bookInfo);
    }

    @Override
    public int delete(BookInfo bookInfo) {
        return mAppDatabase.bookInfoDao().deleteBookInfo(bookInfo);
    }
}
