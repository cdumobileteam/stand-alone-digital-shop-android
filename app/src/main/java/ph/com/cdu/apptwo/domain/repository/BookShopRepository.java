package ph.com.cdu.apptwo.domain.repository;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.database.BookShop;

/**
 * Created by Patrick Santos on 09/02/2018.
 */

public interface BookShopRepository {
    List<BookShop> getALLBook();

    Long insert(BookShop bookShop);

    void delete();

    List<BookShop> getBookCategory(String bookCategory);

    List<String> getBookTitle(String bookTitle);

    List<BookShop> getBookInfo(String bookTitle);
}
