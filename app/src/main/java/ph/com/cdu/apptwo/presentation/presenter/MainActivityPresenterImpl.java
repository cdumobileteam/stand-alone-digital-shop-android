package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.response.UserAccountsResponse;
import ph.com.cdu.apptwo.domain.interactor.UserAccountsInteractorImpl;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.domain.interactor.UserAccountsInteractor;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteBooksAsyncTask;

/**
 * Created by Neil Cruz on 11/10/2017.
 */

public class MainActivityPresenterImpl implements MainActivityPresenter, UserAccountsInteractor.onUserAccountsDataLoadedListener {

    private Context mContext;
    private MainView mMainView;
    private UserAccountsInteractor mUserAccountsInteractor;

    public MainActivityPresenterImpl(Context context, MainView mainView) {
        mContext = context;
        mMainView = mainView;
        mUserAccountsInteractor = new UserAccountsInteractorImpl();
    }

    @Override
    public void onCreate() {
        mMainView.getArguments();
        mMainView.initNavigationLayout();
        mMainView.getRecommendedBooks();
        mMainView.initViewPager();
        mMainView.setUpListeners();
    }

    @Override
    public void onLoginSuccess() {
        mMainView.setArguments();
        mUserAccountsInteractor.requestUserAccount(this, mContext);
        mMainView.setService();
    }

    @Override
    public void initBindService() {
        mMainView.onBindService();
    }

    @Override
    public void initUserAccount() {
        mMainView.onUserAccount();
    }

    @Override
    public void onTappedSearch() {
        mMainView.goToBookSearch();
    }

    @Override
    public void onTappedLogoutFacebook() {
        mMainView.signOutFacebook();
    }

    @Override
    public void onTappedLogoutGooglePlus() {
        mMainView.signOutGooglePlus();
    }

    @Override
    public void onLogoutSuccess() {
        mMainView.initSocialLogout();
    }

    @Override
    public void onSuccess(UserAccountsResponse userAccountsData) {
        mMainView.onLoadUserAccounts(userAccountsData.getResult());
    }

    @Override
    public void onFailed() {
        mMainView.checkNetwork();
    }

    @Override
    public void onDisplayedUserInfo(String accountUsed, String fName, String lName, String profilePicUri) {
        mMainView.getUserInfo(fName + " " + lName, profilePicUri);
        mMainView.setUpSocialLogin(accountUsed);
    }

    @Override
    public void onManageGoogleApi() {
        mMainView.handleSocialPlugins();
    }

    @Override
    public void onReCreate() {
        DeleteBooksAsyncTask deleteBooksAsyncTask = new DeleteBooksAsyncTask(mContext);
        deleteBooksAsyncTask.execute();
    }

    @Override
    public void initAnalytics() {
        mMainView.setUpSnackBar();
        mMainView.setUpPermissions();
        mMainView.goAnalytics();
    }

    @Override
    public void onNetworkConnectionLoss() {
        mMainView.goToNoNetworkConnection();
    }

    @Override
    public void initDownloadRequest() {
        mMainView.setUpDownloadManager();
    }

    @Override
    public void setUpSocialButtons(int socialMedia, int color) {
        mMainView.initSocialMediaButtons(socialMedia,color);
    }
}
