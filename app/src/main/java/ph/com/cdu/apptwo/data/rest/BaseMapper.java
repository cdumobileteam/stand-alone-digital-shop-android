package ph.com.cdu.apptwo.data.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * Created by Neil Cruz on 06/11/2017.
 */

public class BaseMapper {
    public static Map<String, Object> mapParameters(Object objectToMap) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        return mapper.convertValue(objectToMap,
                new TypeReference<Map<String, Object>>() {
                });
    }

    public static Map<String, String> mapParametersString(Object objectToMap) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        return mapper.convertValue(objectToMap,
                new TypeReference<Map<String, String>>() {
                });
    }
}
