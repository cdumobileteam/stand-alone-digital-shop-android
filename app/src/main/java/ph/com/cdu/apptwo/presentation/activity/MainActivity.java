package ph.com.cdu.apptwo.presentation.activity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.domain.model.User.UserAccounts;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.presentation.adapter.MainAdapter;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteUserInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.fragment.BookDetailsFragment;
import ph.com.cdu.apptwo.presentation.fragment.MyBooksFragment;
import ph.com.cdu.apptwo.presentation.presenter.MainActivityPresenter;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.presentation.fragment.BooksRecommendedFragment;
import ph.com.cdu.apptwo.presentation.presenter.MainActivityPresenterImpl;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;
import ph.com.cdu.apptwo.presentation.util.PermissionChecker;
import ph.com.cdu.apptwo.presentation.util.TimerUtil;
import ph.com.cdu.apptwo.util.Gaid;
import ph.com.cdu.apptwo.util.Logger;
import ph.com.cdu.apptwo.util.NetworkConnection;
import timber.log.Timber;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MainView, OnClickListener, OnTouchListener, Callback, GoogleApiClient.OnConnectionFailedListener, ViewPager.OnPageChangeListener {

    private Context mContext;
    private PermissionChecker mPermissionChecker;
    private MainActivityPresenter mPresenter;
    private MainAdapter mMainAdapter;
    private IntentFilter mIntentCallback;
    private TimerUtil mTimer;
    private ActionBarDrawerToggle mToggle;
    private GoogleSignInOptions mGoogleSigninOptions;
    private GoogleApiClient mGoogleApiClient;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private BroadcastReceiver mCallback;
    private ServiceConnection mServiceConn;
    private Fragment mFragment;
    private Bundle mBundle;
    private NetworkConnection mInternet;
    private Animation mAnimEnter;
    private Animation mAnimExit;
    private DataDownloader mSharedPreferences;
    private Snackbar mSnackbar;
    private Logger mLogger;
    private Gaid mAnalytics;
    private IInAppBillingService mService;
    @BindView(R.id.imageButtonBookSearch)
    ImageButton mImgButtonBookSearch;
    @BindView(R.id.textViewTitle)
    TextView mTxtViewTitle;
    @BindView(R.id.textViewBookPages)
    TextView mTxtViewPages;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.linearLayoutBookCategory)
    LinearLayout mLnrLayoutBookCategory;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.linearLayoutNavigationTabs)
    LinearLayout mLnrLayoutNavigationTabs;
    @BindView(R.id.linearLayoutNavigationTabIndicators)
    LinearLayout mLnrLayoutNavigationIndicators;
    @BindView(R.id.linearLayoutPlaceholder)
    LinearLayout mLnrLayoutPlaceholder;
    @BindView(R.id.buttonFree)
    Button mBtnFree;
    @BindView(R.id.buttonPaid)
    Button mBtnPaid;
    @BindView(R.id.buttonTopSelling)
    Button mBtnTopSelling;
    @BindView(R.id.buttonNew)
    Button mBtnNew;
    @BindView(R.id.viewFree)
    View mViewFree;
    @BindView(R.id.viewPaid)
    View mViewPaid;
    @BindView(R.id.viewTopSelling)
    View mViewTopSelling;
    @BindView(R.id.viewNew)
    View mViewNew;
    @BindView(R.id.linearLayoutBookDetails)
    LinearLayout mLnrLayoutBookDetails;
    @BindView(R.id.frameLayoutRecommended)
    FrameLayout mFrmLayoutRecommended;
    View mHeader;
    int mRequestCode;
    String mSocialApp, mUserId, mName, mPicture;
    ImageView mImgViewUserPic;
    TextView mTxtViewUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;
        mSharedPreferences = new DataDownloader(mContext);
        mPresenter = new MainActivityPresenterImpl(mContext, this);
        mFragmentManager = getSupportFragmentManager();
        mInternet = new NetworkConnection(mContext);
        mPresenter.initBindService();
        mTimer = new TimerUtil(mContext);
    }

    @Override
    public void setUpPermissions() {
        mPermissionChecker = new PermissionChecker(mContext);
    }

    @Override
    public void initNavigationLayout() {
        setSupportActionBar(mToolbar);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mToggle.syncState();
        mDrawerLayout.setDrawerListener(mToggle);
        mNavigationView.setNavigationItemSelectedListener(this);
        mHeader = mNavigationView.getHeaderView(0);
        mImgViewUserPic = mHeader.findViewById(R.id.imageViewUserPic);
        mTxtViewUserName = mHeader.findViewById(R.id.textViewUserName);
        mAnimEnter = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        mAnimExit = AnimationUtils.loadAnimation(this, R.anim.fade_out);

    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.initAnalytics();
        mPresenter.onCreate();
        mPresenter.onLoginSuccess();
    }

    @Override
    public void getArguments() {
        mBundle = getIntent().getExtras();
        mSocialApp = mBundle.getString(getString(R.string.params_social));
        mUserId = mBundle.getString(getString(R.string.params_userId));
        mName = mBundle.getString(getString(R.string.params_firstName)) + " " +
                mBundle.getString(getString(R.string.params_lastName));
        mPicture = mBundle.getString(getString(R.string.params_profilePic));
    }

    @Override
    public void onBindService() {
        mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                mService = IInAppBillingService.Stub.asInterface(service);
            }
        };
    }

    @Override
    public void setUpSocialLogin(String mSocialApp) {
        if (mSocialApp.equals(getString(R.string.social_media_facebook))) {
            mPresenter.setUpSocialButtons(R.id.facebook,Color.BLUE);
        } else if (mSocialApp.equals(getString(R.string.social_media_google_plus))) {
            mPresenter.setUpSocialButtons(R.id.googleplus,Color.RED);
            mGoogleSigninOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            try {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, this)
                        .addApi(Plus.API)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSigninOptions)
                        .build();
            } catch (Exception e) {
                Log.e("GoogleApiClient", e.getMessage());
            }
        }
    }

    private void showMyBooks() {
        mLogger.logBookCategory(getString(R.string.navigation_my_books));
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.params_userId), mUserId);
        MyBooksFragment viewMyBooks = new MyBooksFragment();
        viewMyBooks.setArguments(bundle);
        mFragmentManager.beginTransaction().addToBackStack(getString(R.string.title_fragment_my_book))
                .replace(R.id.flContent, viewMyBooks)
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        initPageTitle();
        if (mLnrLayoutBookCategory.isShown()) {
           showMessageOKCancel(null,false);
        } else {
            mLnrLayoutBookCategory.setVisibility(VISIBLE);
            mLnrLayoutBookDetails.startAnimation(mAnimExit);
            mLnrLayoutBookDetails.setVisibility(GONE);
        }
    }

    private void showMessageOKCancel(String title,final boolean logout) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(R.string.text_are_you_sure_you_want_to_proceed)
                .setCancelable(false)
                .setPositiveButton(R.string.text_ok,new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,int id) {
                        if (logout) {
                            mSharedPreferences.logOutUser();
                            Intent newIntent = new Intent(mContext,SplashScreenActivity.class);
                            newIntent.putExtra(getString(R.string.has_user_logout), true);
                            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);
                            overridePendingTransition(R.anim.blink_in, R.anim.blink_out);
                            finish();
                        } else {
                            finish();
                            overridePendingTransition(R.anim.blink_in,R.anim.blink_out);
                        }
                    }
                })
                .setNegativeButton(R.string.text_cancel,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    private void initPageTitle() {
        setTitle(getString(R.string.app_name), "",GONE,VISIBLE);
    }

    private void setTitle(String title, String pages, int viewPages, int viewSearch) {
        mTxtViewTitle.setText(title);
        mTxtViewPages.setText(pages);
        mTxtViewPages.setVisibility(viewPages);
        mImgButtonBookSearch.setVisibility(viewSearch);
    }

    @Override
    public void setMainTitle(String title, String pages, int search) {
        setTitle(title, pages, VISIBLE, search);
    }

    @Override
    public void goToBookDetails() {
        mLogger.logAppEvent(getString(R.string.params_contentType_bookDetails));
        mAnalytics.sendGAID(getString(R.string.title_fragment_book_details),
                getString(R.string.log_category_action),getString(R.string.log_action_view));
        if (mInternet.hasActiveConnection(getString(R.string.url_digitalshop2)) &&
                hasPermissionRequest(mContext.getResources().getInteger(R.integer.book_details))) {
            mLnrLayoutBookDetails.startAnimation(mAnimEnter);
            setUpViewPager(VISIBLE, GONE, VISIBLE);
        } else if (!mInternet.hasActiveConnection(getString(R.string.url_digitalshop2))) {
            mPresenter.onNetworkConnectionLoss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mRequestCode == mContext.getResources().getInteger(R.integer.book_details)) {
                        goToBookDetails();
                    } else if (mRequestCode == mContext.getResources().getInteger(R.integer.my_purchases)) {
                        initNavMyBooks();
                    }
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    onTapped(getString(R.string.text_allow_access_storage));
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void setArguments() {
        mSharedPreferences.setUserAccount(mUserId, mName, mBundle.getString(getString(R.string.params_email)), mSocialApp);
    }

    @Override
    public void getUserInfo(String name, String picture) {
        if (picture.isEmpty()) {
            mImgViewUserPic.setImageResource(R.drawable.no_photo);
        } else {
            Picasso.with(getApplicationContext()).load(picture).placeholder(R.drawable.no_photo)
                    .error(R.drawable.no_photo)
                    .resize(mContext.getResources().getInteger(R.integer.image_target_width),
                            mContext.getResources().getInteger(R.integer.image_target_height))
                    .centerCrop().into(mImgViewUserPic, this);
        }
        if (name.contains(getString(R.string.keyword_null))) {
            mTxtViewUserName.setText(mSharedPreferences.getUserEmail());
        } else {
            mTxtViewUserName.setText(name);
        }
    }

    @Override
    public void onLoadUserAccounts(List<UserAccounts> userAccounts) {
        for (int i = 0; i < userAccounts.size(); i++) {
            if (userAccounts.get(i).getAccount_id().equals(mUserId)) {
                mPresenter.onDisplayedUserInfo(userAccounts.get(i).getAccount_used(), userAccounts.get(i).getFirst_name(),
                        userAccounts.get(i).getLast_name(), userAccounts.get(i).getProfile_pic_uri());
            }
        }
    }

    @Override
    public void checkNetwork() {
        mSnackbar.show();
        mTxtViewUserName.setText(mSharedPreferences.getUserName());
        if (mSharedPreferences.getSocial().equals(getString(R.string.social_media_google_plus))) {
            mNavigationView.getMenu().findItem(R.id.googleplus).setVisible(true);
        } else {
            mNavigationView.getMenu().findItem(R.id.facebook).setVisible(true);
        }
    }

    @Override
    public void getRecommendedBooks() {
        mViewPager.setVisibility(GONE);
        mLnrLayoutNavigationIndicators.setVisibility(GONE);
        mFragment = new BooksRecommendedFragment();
        mFragmentManager.beginTransaction()
                .replace(R.id.frameLayoutRecommended, mFragment)
                .commit();
    }

    @Override
    public void onUserAccount() {
        DeleteUserInfoAsyncTask deleteUserInfoAsyncTask = new DeleteUserInfoAsyncTask(mContext);
        deleteUserInfoAsyncTask.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private class CallbackManager extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction())) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragment().equals(getString(R.string.title_fragment_my_book))) {
                                showMyBooks();
                            }
                        } catch (Exception e) {
                            System.err.println(e.toString());
                        }
                    }
                }, mContext.getResources().getInteger(R.integer.normal_relay));
            }
        }
    }

    private String getFragment() {
        int index = mFragmentManager.getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = mFragmentManager.getBackStackEntryAt(index);
        return backEntry.getName();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        initPageTitle();
        if (id == R.id.nav_home) {
            setUpViewPager(VISIBLE, VISIBLE,GONE);
            showRecommended();
        } else if (id == R.id.nav_my_books) {
            initNavMyBooks();
        } else if (id == R.id.nav_about) {
            showAbout();
        } else if (id == R.id.nav_user_agreement) {
            showUserAgreement();
        } else if (id == R.id.facebook) {
            mPresenter.onTappedLogoutFacebook();
        } else if (id == R.id.googleplus) {
            mPresenter.onTappedLogoutGooglePlus();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initNavMyBooks() {
        if(hasPermissionRequest(mContext.getResources().getInteger(R.integer.my_purchases))){
            setUpViewPager(GONE, GONE, VISIBLE);
            showMyBooks();
        }
    }

    private void showAbout() {
        Intent aboutApp = new Intent(this, AboutActivity.class);
        startActivity(aboutApp);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
//        setUpViewPager(VISIBLE, VISIBLE,GONE);
    }

    private void showUserAgreement() {
        Intent privacyPolicy = new Intent(this, UserAgreementActivity.class);
        startActivity(privacyPolicy);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
//        setUpViewPager(VISIBLE, VISIBLE,GONE);
    }

    private void showRecommended() {
        mFrmLayoutRecommended.setVisibility(VISIBLE);
        mViewPager.setVisibility(GONE);
        mLnrLayoutNavigationIndicators.setVisibility(GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mPresenter.onLogoutSuccess();
        if (mService != null) {
            unbindService(mServiceConn);
        }
        mPresenter.onManageGoogleApi();
    }

    @Override
    public void onPause() {
        super.onPause();
//        mPresenter.onLogoutSuccess();
        mPresenter.onManageGoogleApi();
    }

    @Override
    public void initViewPager() {
        mMainAdapter = new MainAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mMainAdapter);
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }

    @Override
    public void setUpListeners() {
        mBtnFree.setOnClickListener(this);
        mBtnPaid.setOnClickListener(this);
        mBtnTopSelling.setOnClickListener(this);
        mBtnNew.setOnClickListener(this);
        mViewPager.setOnTouchListener(this);
        mViewPager.addOnPageChangeListener(this);
        mSnackbar.setAction(R.string.button_retry, this);
        mImgButtonBookSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        initPageTitle();
        switch (view.getId()) {
            case R.id.buttonFree:
                mViewPager.setCurrentItem(0);
                mLogger.logAppEvent(getString(R.string.params_contentType_freeBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_free_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewFree, mViewPaid, mViewTopSelling, mViewNew);
                break;
            case R.id.buttonPaid:
                mViewPager.setCurrentItem(1);
                mLogger.logAppEvent(getString(R.string.params_contentType_paidBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_paid_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewPaid, mViewFree, mViewTopSelling, mViewNew);
                break;
            case R.id.buttonNew:
                mViewPager.setCurrentItem(2);
                mLogger.logAppEvent(getString(R.string.params_contentType_newBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_new_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewNew, mViewFree, mViewPaid, mViewTopSelling);
                break;
            case R.id.buttonTopSelling:
                mViewPager.setCurrentItem(3);
                mLogger.logAppEvent(getString(R.string.params_contentType_topSellingBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_topselling_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewTopSelling, mViewFree, mViewPaid, mViewNew);
                break;
            case R.id.imageButtonBookSearch:
                mLogger.logAppEvent(getString(R.string.params_contentType_searchBooks));
                mAnalytics.sendGAID(getString(R.string.title_activity_search),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                mPresenter.onTappedSearch();
                break;
            default:
                onStart();
                break;
        }
    }

    private void reActivateTabs() {
        mLnrLayoutNavigationTabs.setVisibility(VISIBLE);
        mLnrLayoutNavigationIndicators.setVisibility(VISIBLE);
    }

    private void activateTab(View view0, View view1, View view2, View view3) {
        view0.setBackgroundResource(R.color.colorPrimaryDark);
        view1.setBackgroundResource(0);
        view2.setBackgroundResource(0);
        view3.setBackgroundResource(0);
        hideRecommended();
    }

    private void hideRecommended() {
        reActivateTabs();
        mViewPager.setVisibility(VISIBLE);
        mLnrLayoutNavigationIndicators.setVisibility(VISIBLE);
        mFrmLayoutRecommended.setVisibility(GONE);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void setUpSnackBar() {
        mSnackbar = Snackbar.make(findViewById(android.R.id.content),
                R.string.alert_no_connection,
                Snackbar.LENGTH_LONG).setActionTextColor(Color.RED);
    }

    @Override
    public void onSuccess() {
        Bitmap imageBitmap = ((BitmapDrawable) mImgViewUserPic.getDrawable()).getBitmap();
        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
        imageDrawable.setCircular(true);
        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
        mImgViewUserPic.setImageDrawable(imageDrawable);
    }

    @Override
    public void onError() {
        mImgViewUserPic.setImageResource(R.drawable.no_photo);
    }

    @Override
    public void signOutGooglePlus() {
        if (mInternet.hasActiveConnection(getString(R.string.url_google))) {
            try {
                mGoogleApiClient.connect();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Log.d(getString(R.string.title_activity_splash_screen), status.toString());
                    }
                });
            } catch (Exception e) {
                Timber.e(e.getMessage());
            }
            mLogger.logSignOut(mSharedPreferences.getSocial());
            mPresenter.onLogoutSuccess();
        } else {
            mPresenter.onNetworkConnectionLoss();
        }
    }

    @Override
    public void signOutFacebook() {
        if (mInternet.hasActiveConnection(getString(R.string.url_facebook))) {
            LoginManager.getInstance().logOut();
            mLogger.logSignOut(mSharedPreferences.getSocial());
            mPresenter.onLogoutSuccess();
        } else {
            mPresenter.onNetworkConnectionLoss();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(getString(R.string.title_activity_splash_screen), "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void goToBookSearch() {
        if (mInternet.hasActiveConnection(getString(R.string.url_digitalshop2))) {
            Intent bookSearch = new Intent(this, SearchActivity.class);
            startActivity(bookSearch);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            setUpViewPager(VISIBLE, VISIBLE,GONE);
        } else {
            mPresenter.onNetworkConnectionLoss();
        }
    }

    @Override
    public void goToNoNetworkConnection() {
        mSnackbar.setText(getString(R.string.alert_no_connection))
                .setActionTextColor(Color.RED).
                setAction(getString(R.string.button_retry),null).show();
    }

    @Override
    public void onResume(){
        super.onResume();
        showRecommended();
//        mPresenter.initDownloadRequest();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        switch (position) {
            case 0:
                mLogger.logAppEvent(getString(R.string.params_contentType_freeBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_free_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewFree, mViewPaid, mViewTopSelling, mViewNew);
                break;
            case 1:
                mLogger.logAppEvent(getString(R.string.params_contentType_paidBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_paid_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewPaid, mViewFree, mViewTopSelling, mViewNew);
                break;
            case 2:
                mLogger.logAppEvent(getString(R.string.params_contentType_newBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_new_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewNew, mViewFree, mViewPaid, mViewTopSelling);
                break;
            case 3:
                mLogger.logAppEvent(getString(R.string.params_contentType_topSellingBooks));
                mAnalytics.sendGAID(getString(R.string.title_fragment_topselling_books),
                        getString(R.string.log_category_action),getString(R.string.log_action_view));
                activateTab(mViewTopSelling, mViewFree, mViewPaid, mViewNew);
                break;
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setUpViewPager(int bookProgress, int bookCategory, int bookDetails) {
        mLnrLayoutPlaceholder.setVisibility(bookProgress);
        mLnrLayoutBookCategory.setVisibility(bookCategory);
        mLnrLayoutBookDetails.setVisibility(bookDetails);
        showBookInfo();
    }

    private void showBookInfo() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLnrLayoutPlaceholder.setVisibility(GONE);
            }
        }, mContext.getResources().getInteger(R.integer.shorter_relay));
    }

    @Override
    public void initSocialLogout() {
        mPresenter.initUserAccount();
        mPresenter.onReCreate();
        mPresenter.onManageGoogleApi();
        showMessageOKCancel(getString(R.string.text_shop_ph_is_logging_out),true);
    }

    @Override
    public void handleSocialPlugins() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void setUpDownloadManager() {
        if (mSharedPreferences.getDownloadedFileID() != 0) {
            DownloadManager downloadManager = (DownloadManager) mContext
                    .getSystemService(Context.DOWNLOAD_SERVICE);
            downloadManager.remove(mSharedPreferences.getDownloadedFileID());
            mSharedPreferences.setDownloadedFileID(0);
        }
    }

    private boolean hasPermissionRequest(int requestCode) {
        if(!mPermissionChecker.readAndWriteExternalStorage()){
            mRequestCode = requestCode;
           return false;
        }
        return true;
    }

    @Override
    public void setService() {
        Intent serviceIntent =
                new Intent(getString(R.string.action_inapp_billing_service_bind));
        serviceIntent.setPackage(getString(R.string.params_vending));
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void initBookView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLnrLayoutPlaceholder.setVisibility(GONE);
            }
        }, mContext.getResources().getInteger(R.integer.shortest_relay));
    }

    @Override
    public void goAnalytics() {
        mLogger = new Logger(mContext);
        mAnalytics = new Gaid(mContext);
    }

    @Override
    public void onTapped(String status) {
        if (status != null) {
            mSnackbar.setText(status);
            mSnackbar.show();
            mSnackbar.setAction(null, null);
        }
    }

    @Override
    public void onDownloadComplete() {
        onTapped(getString(R.string.alert_download_progress));
        mCallback =  new CallbackManager();
        mIntentCallback = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mContext.registerReceiver(mCallback,mIntentCallback);
    }

    @Override
    public void initSocialMediaButtons(int socialMedia, int color) {
        mNavigationView.getMenu().findItem(socialMedia).setVisible(true);
//        mNavigationView.getMenu().findItem(socialMedia).getIcon().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    private class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}
