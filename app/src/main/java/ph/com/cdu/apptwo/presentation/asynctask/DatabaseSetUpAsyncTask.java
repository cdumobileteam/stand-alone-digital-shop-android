package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import timber.log.Timber;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

public class DatabaseSetUpAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private Context mContext;

    public DatabaseSetUpAsyncTask(Context context) {
        mContext = context;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        try {
            databaseInteractor.setUpDatabase(mContext);
        } catch (IOException e) {
            Timber.e(e);
            return false;
        }
        return true;
    }
}
