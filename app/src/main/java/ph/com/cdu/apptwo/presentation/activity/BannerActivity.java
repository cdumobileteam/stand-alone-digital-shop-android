package ph.com.cdu.apptwo.presentation.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;
import java.util.Locale;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteBooksAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.DeleteUserInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.asynctask.QueryUserInfoAsyncTask;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;
import ph.com.cdu.apptwo.util.Gaid;

public class BannerActivity extends AppCompatActivity {

    private Context mContext;
    private DataDownloader mDataDownloader;
    private Gaid mAnalytics;
    private BroadcastReceiver onInstall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);
        mContext = this;
        mDataDownloader = new DataDownloader(mContext);
        mAnalytics = new Gaid(mContext);
        checkAppData();
        IntentFilter filter = new IntentFilter();
        onInstall = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Get the intent that started this Activity.
                Uri uri = intent.getData();
                mAnalytics.sendAppTracker(getString(R.string.title_activity_banner),
                        uri.getPath());

                // When you're done, pass the intent to the Google Analytics receiver.
                new CampaignTrackingReceiver().onReceive(context, intent);
            }
        };
        registerReceiver(onInstall, filter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setApplicationInfo();
            }
        }, mContext.getResources().getInteger(R.integer.longest_relay));

        Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
        Debug.getMemoryInfo(memoryInfo);

        String memMessage = String.format(Locale.ENGLISH,
                "Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
                memoryInfo.getTotalPss() / 1024.0,
                memoryInfo.getTotalPrivateDirty() / 1024.0,
                memoryInfo.getTotalSharedDirty() / 1024.0);

        Log.d("PSSMEASURE: ", memMessage);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContext.unregisterReceiver(onInstall);
    }

    private void checkAppData() {
        Log.d("runnew", mDataDownloader.getFirstTimeRun() + "");
        Log.d("runupdate", mContext.getResources().getInteger(R.integer.app_update) + "");
        if (mDataDownloader.getFirstTimeRun() == mContext.getResources().getInteger(R.integer.app_update)){
            clearLocalData();
        }
    }

    private void checkUserStatus() {
        QueryUserInfoAsyncTask queryUserInfoAsyncTask = new QueryUserInfoAsyncTask(getApplicationContext());
        queryUserInfoAsyncTask.setOnCallBack(new QueryUserInfoAsyncTask.CallBackTask() {
            @Override
            public void callback(List<UserInfo> result) {
                if (result.size() == 0) {
                    goToLogin();
                } else {
                    for (int i=0;i<result.size();i++) {
                        goToMain(result.get(i).getSocialAccount(),result.get(i).getEmail(),
                                result.get(i).getFirstName(),result.get(i).getLastName(),
                                result.get(i).getGender(),result.get(i).getProfilePicUri(),
                                result.get(i).getId());
                    }
                }
            }
        });
        queryUserInfoAsyncTask.execute();
    }

    private void setApplicationInfo() {
        if (checkPlayServices()) {
            checkUserStatus();
        }
    }

    private void goToMain(String socialAccount,String email,String firstName,String lastName,String gender,String profilePicUri,String id) {
        Intent main = new Intent(BannerActivity.this, MainActivity.class);
        main.putExtra(getString(R.string.params_social),socialAccount);
        main.putExtra(getString(R.string.params_userId), id);
        main.putExtra(getString(R.string.params_email), email);
        main.putExtra(getString(R.string.params_firstName), firstName);
        main.putExtra(getString(R.string.params_lastName), lastName);
        main.putExtra(getString(R.string.params_gender), gender);
        main.putExtra(getString(R.string.params_profilePic), profilePicUri);
        startActivity(main);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    private void goToLogin() {
        Intent login = new Intent(BannerActivity.this, SplashScreenActivity.class);
        startActivity(login);
        overridePendingTransition(R.anim.blink_in, R.anim.blink_out);
        finish();
    }

    private void clearLocalData() {
        DeleteUserInfoAsyncTask deleteUserInfoAsyncTask = new DeleteUserInfoAsyncTask(mContext);
        deleteUserInfoAsyncTask.execute();

        DeleteBooksAsyncTask deleteBooksAsyncTask = new DeleteBooksAsyncTask(mContext);
        deleteBooksAsyncTask.execute();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 2404)
                        .show();
            } else if(resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED){
                apiAvailability.getErrorDialog(this, resultCode, 2404)
                        .show();
            } else {
                apiAvailability.getErrorDialog(this, resultCode, 2404)
                        .show();
                Log.i("Google Play Services: ", "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(mContext,"granted",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext,"denied",Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

                // other 'case' lines to check for other
                // permissions this app might request.
        }
    }

}