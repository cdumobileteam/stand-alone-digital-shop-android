package ph.com.cdu.apptwo.presentation.presenter;

import android.content.Context;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksNewView;
import ph.com.cdu.apptwo.data.rest.response.NewBooksResponse;
import ph.com.cdu.apptwo.domain.interactor.BooksNewInteractor;
import ph.com.cdu.apptwo.domain.interactor.BooksNewInteractorImpl;
import ph.com.cdu.apptwo.util.NetworkConnection;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksNewPresenterImpl implements BooksNewPresenter, BooksNewInteractor.onBooksNewDataLoadedListener {

    private Context mContext;
    private BooksNewView mBooksNewView;
    private BooksNewInteractor mBooksNewInteractor;

    public BooksNewPresenterImpl(Context context, BooksNewView booksNewView) {
        mContext = context;
        mBooksNewView = booksNewView;
        mBooksNewInteractor = new BooksNewInteractorImpl();
    }

    @Override
    public void onCreate() {
        mBooksNewView.setUpListeners();
        mBooksNewView.setUpRecyclerViewDeck();
    }

    @Override
    public void onConnected() {
        mBooksNewView.getBooks();
    }

    @Override
    public void initNewBooks() {
        mBooksNewView.onInitNewBooksContent();
    }

    @Override
    public void initLinearLayoutManagerDeck() {
        mBooksNewView.setUpLayoutManagerDeck();
    }

    @Override
    public void onSuccess(NewBooksResponse newBooksData) {
        for (int i = mContext.getResources().getInteger(R.integer.initial_value);
             i<newBooksData.getResult().size(); i++) {
            mBooksNewView.onStoreNewBookContent(newBooksData.getResult());
        }
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void initLoadNewBooksContent() {
        mBooksNewInteractor.requestNewBooks(this, mContext);
    }

    @Override
    public void onFinish() {
        mBooksNewView.setUpBookViews();
    }
}
