package ph.com.cdu.apptwo.presentation.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.MyBooks;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.presentation.fragment.view.MyBooksView;
import ph.com.cdu.apptwo.presentation.util.DataDownloader;
import ph.com.cdu.apptwo.util.NetworkConnection;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public class MyCShopAdapter extends RecyclerView.Adapter<MyCShopAdapter.ViewHolder> {

    private Context mContext;
    private NetworkConnection mInternet;
    private MyBooksView mListener;
    private List<MyBooks> mMyBooks;
    private List<BookInfo> mBooks;
    private String mUserId, mBookTitle, mBookUri;
    private DataDownloader mSharedPreferences;

    public MyCShopAdapter(Context context, MyBooksView myBooksView, ArrayList<MyBooks> myBooks,
                          ArrayList<BookInfo> books) {
        mContext = context;
        mListener = myBooksView;
        mMyBooks = myBooks;
        mBooks = books;
        mInternet = new NetworkConnection(mContext);
        mSharedPreferences = new DataDownloader(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_my_books, parent, false);
        return new ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        MyBooks myBooks = mMyBooks.get(position);
        if (mBooks.size() > 0) {
            for (int i=0;i<mBooks.size();i++) {
                if (mBooks.get(i).getTitle().equals(myBooks.getBook_title())) {
                    holder.mGifDrawable.stop();
                    holder.mBtnReDownload.setText(mContext.getString(R.string.view_book));
                    break;
                } else if (validateLocalFile(mContext.getString(R.string.app_root_directory),
                        myBooks.getBook_id(),mSharedPreferences.getUserId())) {
                    holder.mGifDrawable.stop();
                    holder.mBtnReDownload.setText(mContext.getString(R.string.view_book));
                } else {
                    holder.mBtnReDownload.setText(mContext.getString(R.string.get_book));
                }
            }
        }
        ViewGroup.LayoutParams params = holder.mFrmLayoutBook.getLayoutParams();
        params.height = getHeight(160);
        params.width = getWidth(100);
        holder.mFrmLayoutBook.setLayoutParams(params);
        if (myBooks.getUser_id_fk().equals(mUserId)) {
            holder.mLnrLayoutMyBooks.setVisibility(View.VISIBLE);
            holder.mBtnReDownload.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(myBooks.getBook_image()).fit().into(holder.mImgButtonBookImage, new Callback() {
                @Override
                public void onSuccess() {
                    holder.mGifDrawable.stop();
                    holder.mImgViewPlaceholder.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    holder.mImgButtonBookImage.setVisibility(View.INVISIBLE);
                }
            });
        } else {
            holder.mImgViewPlaceholder.setVisibility(View.GONE);
            holder.mLnrLayoutMyBooks.setVisibility(View.GONE);
        }
    }

    private boolean validateLocalFile(String filePath, String id, String userId) {
        File localFilePath = new File(filePath+id+userId);
        if (localFilePath.exists()) {
            mBookUri = localFilePath.getAbsolutePath();
            return true;
        }
        return false;
    }

    private int getWidth(int width) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width,
                mContext.getResources().getDisplayMetrics());
    }

    private int getHeight(int height) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height,
                mContext.getResources().getDisplayMetrics());
    }

    private MyBooks getMyBooks(int position) {
        return mMyBooks.get(position);
    }

    @Override
    public int getItemCount() {
        if (mMyBooks != null) {
            return mMyBooks.size();
        } else {
            return 0;
        }
    }

    public void updateDeck(List<MyBooks> myBooks, String user_id_fk, List<BookInfo> books) {
        mMyBooks = myBooks;
        mBooks = books;
        mUserId = user_id_fk;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private GifDrawable mGifDrawable;
        private MyBooksView mListener;
        @BindView(R.id.linearLayoutMyBooks)
        LinearLayout mLnrLayoutMyBooks;
        @BindView(R.id.frameLayoutBook)
        FrameLayout mFrmLayoutBook;
        @BindView(R.id.imageViewPlaceholder)
        ImageView mImgViewPlaceholder;
        @BindView(R.id.imageButtonBookImage)
        ImageButton mImgButtonBookImage;
        @BindView(R.id.buttonReDownload)
        TextView mBtnReDownload;
        @BindView(R.id.gifImageViewBookStatusLoading)
        GifImageView mImgViewBookLoading;

        public ViewHolder(View itemView, MyBooksView listener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            mListener = listener;
            mGifDrawable = (GifDrawable) mImgViewBookLoading.getDrawable();
            mBtnReDownload.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            MyBooks myBooks = getMyBooks(getAdapterPosition());
            if (mBtnReDownload.getText().equals(mContext.getString(R.string.view_book))) {
                for (int i = 0; i < mBooks.size(); i++) {
                    if (myBooks.getBook_title().equals(mBooks.get(i).getTitle())) {
                        mListener.viewBook(mBooks.get(i).getPid(),mBooks.get(i).getUserId(),
                                mBooks.get(i).getId(),mBooks.get(i).getTitle(),
                                mBooks.get(i).getUrlDownload());
                    }
                }
            } else if (!hasDownloadFinish()) {
                mBtnReDownload.setVisibility(View.INVISIBLE);
                mGifDrawable.start();
            } else {
                if (hasDownloadFinish() && mBtnReDownload.isClickable() && hasNetworkConnection()) {
                    mListener.unregisterReceiver();
                    mListener.getBook(myBooks);
                    mGifDrawable.start();
                    mBtnReDownload.setVisibility(View.INVISIBLE);
//                    mBtnReDownload.setText(mContext.getString(R.string.text_downloading));
//                    mBtnReDownload.setAllCaps(false);
//                    mBtnReDownload.setBackgroundResource(R.drawable.shape_rectangle_gray_rounded_corner);
//                    mBtnReDownload.setClickable(false);
                } else {
                    Toast.makeText(mContext,"Please wait downloading in progress...",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean hasNetworkConnection() {
        if (!mInternet.hasActiveConnection(mContext.getString(R.string.url_digitalshop2))){
            mListener.goToNetworkConnection();
            return false;
        }
        return true;
    }

    private boolean hasDownloadFinish() {
        return mSharedPreferences.getDownloadedFileID() == 0;
    }
}
