package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.response.FreeBooksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class AllBooksInteractorImpl implements AllBooksInteractor {

    private CDUService mService;

    @Override
    public void requestAllBooks(final onAllBooksDataLoadedListener listener, Context context) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_BOOKS);

        mService.getAllDigitalShopBooks().enqueue(new Callback<FreeBooksResponse>() {
            @Override
            public void onResponse(Call<FreeBooksResponse> call, Response<FreeBooksResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<FreeBooksResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
