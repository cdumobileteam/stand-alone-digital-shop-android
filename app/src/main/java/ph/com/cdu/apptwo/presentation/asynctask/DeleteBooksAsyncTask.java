package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;

/**
 * Created by Neil Cruz on 26/10/2017.
 */

public class DeleteBooksAsyncTask extends AsyncTask<BookInfo, Void, Void> {
    private Context mContext;

    public DeleteBooksAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected Void doInBackground(BookInfo... params) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        databaseInteractor.deleteAllBooks(mContext);
        return null;
    }
}
