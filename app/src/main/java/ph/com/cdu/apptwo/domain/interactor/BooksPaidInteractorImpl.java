package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.response.PaidBooksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Neil Cruz on 13/10/2017.
 */

public class BooksPaidInteractorImpl implements BooksPaidInteractor {

    private CDUService mService;

    @Override
    public void requestPaidBooks(final onBooksPaidDataLoadedListener listener, Context context) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_BOOKS);

        mService.getPaidBooksContent().enqueue(new Callback<PaidBooksResponse>() {
            @Override
            public void onResponse(Call<PaidBooksResponse> call, Response<PaidBooksResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<PaidBooksResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
