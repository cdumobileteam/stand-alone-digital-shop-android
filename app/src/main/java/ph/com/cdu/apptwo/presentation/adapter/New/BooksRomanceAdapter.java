package ph.com.cdu.apptwo.presentation.adapter.New;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.books.FreeBooks;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.presentation.activity.view.MainView;
import ph.com.cdu.apptwo.presentation.fragment.view.BooksNewView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class BooksRomanceAdapter extends RecyclerView.Adapter<BooksRomanceAdapter.ViewHolder> {

    private Context mContext;
    private MainView mListener;
    private BooksNewView mBooksNewView;
    private List<FreeBooks> mNewBooks;

    public BooksRomanceAdapter(MainView mainView, Context context,
                               BooksNewView booksNewView, ArrayList<FreeBooks> newBooks) {
        mListener = mainView;
        mContext = context;
        mBooksNewView = booksNewView;
        mNewBooks = newBooks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_deck1, parent, false);

        ViewHolder viewHolder = new ViewHolder(view, mListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        FreeBooks books = mNewBooks.get(position);
//        if (books.getGenre().contains("Romance") &&
//                books.getCategory().equals(mContext.getString(R.string.params_new_books_content))) {
//            holder.mFrmLayoutBook.setVisibility(VISIBLE);
            initBookInfo(holder, books);
//        } else {
//            holder.mFrmLayoutBook.setVisibility(GONE);
//        }
    }

    private void initBookInfo(final ViewHolder holder, FreeBooks books) {
        Picasso.with(mContext).load(books.getImage_url()).fit().into(holder.mImgButtonBookImage, new Callback() {
            @Override
            public void onSuccess() {
                holder.mImgViewPlaceholder.setVisibility(GONE);
            }

            @Override
            public void onError() {

                holder.mImgButtonBookImage.setVisibility(View.INVISIBLE);
            }
        });
    }

    private FreeBooks getNewBooksData(int position) {
        return mNewBooks.get(position);
    }

    @Override
    public int getItemCount() {
        if (mNewBooks != null && mNewBooks.size() >0) {
            mBooksNewView.noRomanceAvailable(VISIBLE);
            mBooksNewView.countBooksAvailable(mNewBooks.size());
            return mNewBooks.size();
        } else {
            mBooksNewView.noRomanceAvailable(GONE);
            return 0;
        }
    }

    public void updateDeck(List<FreeBooks> newBooks) {
//        for (int i = 0;i<newBooks.size();i++){
//            if (newBooks.get(i).getGenre().contains("Romance")) {
                mNewBooks = newBooks;
                notifyDataSetChanged();
//            }
//        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private MainView mListener;
        @BindView(R.id.frameLayoutBook)
        FrameLayout mFrmLayoutBook;
        @BindView(R.id.imageViewPlaceholder)
        ImageView mImgViewPlaceholder;
        @BindView(R.id.imageButtonBookImage)
        ImageButton mImgButtonBookImage;

        public ViewHolder(View itemView, MainView listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;

            mImgButtonBookImage.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imageButtonBookImage:
                    FreeBooks newBooksData = getNewBooksData(getAdapterPosition());
                    mListener.goToBookDetails();
                    mBooksNewView.goToFullVersion(newBooksData);
                    break;
            }
        }
    }
}
