package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import ph.com.cdu.apptwo.data.database.BookInfoRepositoryImpl;
import ph.com.cdu.apptwo.data.database.BookShopRepositoryImpl;
import ph.com.cdu.apptwo.data.database.UserInfoRepositoryImpl;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;
import ph.com.cdu.apptwo.domain.repository.BookInfoRepository;
import ph.com.cdu.apptwo.domain.repository.BookShopRepository;
import ph.com.cdu.apptwo.domain.repository.UserInfoRepository;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

public class DatabaseInteractorImpl implements DatabaseInteractor {
    @Override
    public void setUpDatabase(Context context) throws IOException {
        setUpUserInfo(context);
    }

    private void setUpUserInfo(Context context) throws IOException {
        UserInfoRepository userInfoRepository = new UserInfoRepositoryImpl(context);

    }

    @Override
    public List<UserInfo> loadAllUserInfo(Context context) {
        UserInfoRepository userInfoRepository = new UserInfoRepositoryImpl(context);
        return userInfoRepository.getALLUserInfo();
    }

    @Override
    public long insertUserInfo(Context context, UserInfo userInfo) {
        UserInfoRepository userInfoRepository = new UserInfoRepositoryImpl(context);
        return userInfoRepository.insert(userInfo);
    }

    @Override
    public void deleteUserInfo(Context context) {
        UserInfoRepository userInfoRepository = new UserInfoRepositoryImpl(context);
        userInfoRepository.delete();
    }

    @Override
    public boolean isUserInfoSaved(Context context, int userId) {
        UserInfoRepository userInfoRepository = new UserInfoRepositoryImpl(context);
        return userInfoRepository.isUserSaved(userId);
    }

    @Override
    public List<BookInfo> loadAllBookInfo(Context context) {
        BookInfoRepository bookInfoRepository = new BookInfoRepositoryImpl(context);
        return bookInfoRepository.getALLBookInfo();
    }

    @Override
    public long insertBookInfo(Context context, BookInfo bookInfo) {
        BookInfoRepository bookInfoRepository = new BookInfoRepositoryImpl(context);
        return bookInfoRepository.insert(bookInfo);
    }

    @Override
    public int deleteBookInfo(Context context, BookInfo bookInfo) {
        BookInfoRepository bookInfoRepository = new BookInfoRepositoryImpl(context);
        return bookInfoRepository.delete(bookInfo);
    }

    @Override
    public List<BookInfo> getUserBooks(Context context, String userId) {
        BookInfoRepository bookInfoRepository = new BookInfoRepositoryImpl(context);
        return bookInfoRepository.getUserBooks(userId);
    }

    @Override
    public List<BookShop> loadAllBooks(Context context) {
        BookShopRepository bookShopRepository = new BookShopRepositoryImpl(context);
        return bookShopRepository.getALLBook();
    }

    @Override
    public long insertBooks(Context context, BookShop bookShop) {
        BookShopRepository bookShopRepository = new BookShopRepositoryImpl(context);
        return bookShopRepository.insert(bookShop);
    }

    @Override
    public void deleteAllBooks(Context context) {
        BookShopRepository bookShopRepository = new BookShopRepositoryImpl(context);
        bookShopRepository.delete();
    }

    @Override
    public List<BookShop> getAllBooksByCategory(Context context, String bookCategory) {
        BookShopRepository bookShopRepository = new BookShopRepositoryImpl(context);
        return bookShopRepository.getBookCategory(bookCategory);
    }

    @Override
    public List<String> getBooksByTitle(Context context, String bookTitle) {
        BookShopRepository bookShopRepository = new BookShopRepositoryImpl(context);
        return bookShopRepository.getBookTitle(bookTitle);
    }

    @Override
    public List<BookShop> getBookInfo(Context context, String bookTitle) {
        BookShopRepository bookShopRepository =  new BookShopRepositoryImpl(context);
        return bookShopRepository.getBookInfo(bookTitle);
    }
}
