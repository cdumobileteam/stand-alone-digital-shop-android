package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.requestParameters.PurchaseNewBookParams;
import ph.com.cdu.apptwo.data.rest.response.PurchaseNewBookResponse;

/**
 * Created by Patrick Santos on 19/01/2018.
 */

public interface PurchaseNewBooksInteractor {
    interface onPurchaseNewBooksDataLoader {
        void onSuccess(PurchaseNewBookResponse purchaseNewBookResponse);

        void onFailed();
    }

    void requestPurchaseNewBook(onPurchaseNewBooksDataLoader listener, Context context, PurchaseNewBookParams purchaseNewBookParams);
}
