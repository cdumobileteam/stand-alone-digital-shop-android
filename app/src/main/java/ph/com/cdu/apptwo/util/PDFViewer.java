package ph.com.cdu.apptwo.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import ph.com.cdu.apptwo.R;

/**
 * Created by Neil Cruz on 26/10/2017.
 */

public class PDFViewer {
    private Context mContext;

    public PDFViewer(Context context) {
        this.mContext = context;
    }

    public String openPDFReader(Uri bookURL, String mimeType) {
        Intent mPdfIntent = new Intent(Intent.ACTION_VIEW);
        mPdfIntent.setDataAndType(bookURL, mimeType);
        mPdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (mPdfIntent.resolveActivity(mContext.getPackageManager()) != null) {
            mContext.startActivity(Intent.createChooser(mPdfIntent,
                    null));
            Toast.makeText(mContext, mContext.getString(R.string.text_opening_book), Toast.LENGTH_SHORT).show();
            return null;
        } else {
            return mContext.getString(R.string.text_no_apps_to_perform_this_action);
        }
    }

}
