package ph.com.cdu.apptwo.data.rest.requestParameters;

import com.fasterxml.jackson.annotation.JsonProperty;

import ph.com.cdu.apptwo.data.rest.BaseParameter;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public class MyBooksParams extends BaseParameter {
    @JsonProperty("user_id")
    private String user_id;

    public MyBooksParams(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
