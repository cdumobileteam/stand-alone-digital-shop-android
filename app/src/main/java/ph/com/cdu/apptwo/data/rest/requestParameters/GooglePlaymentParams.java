package ph.com.cdu.apptwo.data.rest.requestParameters;

import com.fasterxml.jackson.annotation.JsonProperty;

import ph.com.cdu.apptwo.data.rest.BaseParameter;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public class GooglePlaymentParams extends BaseParameter {
    @JsonProperty("account_id")
    private String account_id;

    public GooglePlaymentParams(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }
}
