package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.CDUService;
import ph.com.cdu.apptwo.data.rest.RetrofitClient;
import ph.com.cdu.apptwo.data.rest.requestParameters.PurchaseNewBookParams;
import ph.com.cdu.apptwo.data.rest.response.PurchaseNewBookResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Patrick Santos on 19/01/2018.
 */

public class PurchaseNewBookInteractorImpl implements PurchaseNewBooksInteractor {

    private CDUService mService;

    @Override
    public void requestPurchaseNewBook(final onPurchaseNewBooksDataLoader listener, Context context, PurchaseNewBookParams purchaseNewBookParams) {

        mService = RetrofitClient.getCduService(context, RetrofitClient.ApiType.API_PURCHASE_BOOK);

        mService.purchaseNewBook(purchaseNewBookParams.mapParameters(context)).enqueue(new Callback<PurchaseNewBookResponse>() {
            @Override
            public void onResponse(Call<PurchaseNewBookResponse> call, Response<PurchaseNewBookResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailed();
                }
            }

            @Override
            public void onFailure(Call<PurchaseNewBookResponse> call, Throwable t) {
                listener.onFailed();
            }
        });
    }
}
