package ph.com.cdu.apptwo.presentation.activity.view;

import java.util.List;
import java.util.Timer;

import ph.com.cdu.apptwo.domain.model.User.UserAccounts;
import ph.com.cdu.apptwo.presentation.fragment.MyBooksFragment;

/**
 * Created by Neil Cruz on 11/10/2017.
 */

public interface MainView {

    void getArguments();

    void initViewPager();

    void setUpListeners();

    void goToBookDetails();

    void getUserInfo(String name, String mPicture);

    void onLoadUserAccounts(List<UserAccounts> userAccounts);

    void initNavigationLayout();

    void setUpSocialLogin(String mSocialApp);

    void signOutGooglePlus();

    void signOutFacebook();

    void getRecommendedBooks();

    void setUpSnackBar();

    void initSocialLogout();

    void handleSocialPlugins();

    void setArguments();

    void checkNetwork();

    void setService();

    void onBindService();

    void onUserAccount();

    void initBookView();

    void goToBookSearch();

    void goAnalytics();

    void setMainTitle(String title, String pages, int search);

    void setUpPermissions();

    void onTapped(String status);

    void goToNoNetworkConnection();

    void onDownloadComplete();

    void setUpDownloadManager();

    void initSocialMediaButtons(int socialMedia, int color);
}
