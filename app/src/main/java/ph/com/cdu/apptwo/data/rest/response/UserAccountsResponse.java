package ph.com.cdu.apptwo.data.rest.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

import ph.com.cdu.apptwo.domain.model.User.UserAccounts;

/**
 * Created by Neil Cruz on 17/01/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountsResponse {
    private List<UserAccounts> result;

    public List<UserAccounts> getResult() {
        return result;
    }

    public void setResult(List<UserAccounts> result) {
        this.result = result;
    }
}
