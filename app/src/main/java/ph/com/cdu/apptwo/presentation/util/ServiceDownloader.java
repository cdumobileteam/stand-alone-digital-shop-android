package ph.com.cdu.apptwo.presentation.util;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.presentation.asynctask.InsertBookInfoAsyncTask;
import ph.com.cdu.apptwo.util.DateFormatter;

/**
 * Created by Neil Cruz on 25/10/2017.
 */

public class ServiceDownloader extends IntentService {

    static InsertBookInfoAsyncTask insertBookInfoAsyncTask;
    static DownloadManager mDownloadManager;
    static DataDownloader mSharedPreferences;
    DownloadManager.Request mRequest;
    static long mDownloadedFileID;
    static String userId;
    static String bookId;
    static String bookTitle;
    static String bookImageUrl;
    String bookUrlDownload;
    String transactionDate;

    public ServiceDownloader() {
        super("PDFViewerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        userId = intent.getStringExtra(getString(R.string.params_userId));
        bookId = intent.getStringExtra(getString(R.string.params_book_id));
        bookTitle = intent.getStringExtra(getString(R.string.params_book_title));
        bookImageUrl = intent.getStringExtra(getString(R.string.params_book_image_url));
        bookUrlDownload = intent.getStringExtra(getString(R.string.params_book_url_download));
        DateFormatter mFormat = new DateFormatter();
        transactionDate =  mFormat.formatDateToShowFormat(new Date());

        mSharedPreferences = new DataDownloader(getApplicationContext());

        if (bookUrlDownload != null) {
            onDownloadStart(bookTitle, bookUrlDownload);
        }
    }

    private void onDownloadStart(final String bookTitle, final String bookUrlDownload) {
        mDownloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        mRequest = new DownloadManager.Request(Uri.parse(bookUrlDownload));
        mRequest.setTitle(bookTitle).setDescription(getString(R.string.text_download_in_progress))
                .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setMimeType(getString(R.string.mime_type_pdf))
                .setDestinationInExternalPublicDir("/"+getPackageName(), bookId+userId)
                .setVisibleInDownloadsUi(false)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .allowScanningByMediaScanner();

        mDownloadedFileID = mDownloadManager.enqueue(mRequest);
        mSharedPreferences.setDownloadedFileID(mDownloadedFileID);
    }

    static void encrypt(Context context) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        // Here you read the cleartext.
        FileInputStream fis = new FileInputStream(context.getString(R.string.app_root_directory)+bookId+userId);
        // This stream write the encrypted text. This stream will be wrapped by another stream.
        FileOutputStream fos = new FileOutputStream(context.getString(R.string.app_root_directory)+userId+bookId);
        insertBookInfoAsyncTask = new InsertBookInfoAsyncTask(context, bookId,
                userId, bookTitle, bookImageUrl, context.getString(R.string.app_root_directory)+userId+bookId
                , new Date());
        insertBookInfoAsyncTask.execute();
        // Length is 16 byte
        // Careful when taking user input!!! https://stackoverflow.com/a/3452620/1188357
        SecretKeySpec sks = new SecretKeySpec(context.getString(R.string.app_password).getBytes(),
                context.getString(R.string.app_type_file));
        // Create cipher
        Cipher cipher = Cipher.getInstance(context.getString(R.string.app_type_file));
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[8];
        while((b = fis.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        fis.close();
        File encrypt = new File(context.getString(R.string.app_root_directory)+bookId+userId);
        if (encrypt.exists()) {
            if (encrypt.delete()) {
                System.out.print(context.getString(R.string.app_root_directory)+bookId+userId);
            } else {
                System.out.print(context.getString(R.string.app_root_directory)+bookId+userId);
            }
        }
    }

    // Function is called once download completes.
    public static class MyRequestReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(mDownloadedFileID);
                Cursor cursor = mDownloadManager.query(query);
                if (cursor.moveToFirst()) {
                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    // status contain Download Status
                    // download_id contain current download reference id
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,0);
                        if (mSharedPreferences.getBookDownloaded().equals(bookId) &&
                                mSharedPreferences.getUserId().equals(userId) &&
                                downloadId == mDownloadedFileID) {
                            String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                            insertBookInfoAsyncTask = new InsertBookInfoAsyncTask(context, bookId,
                                    userId, bookTitle, bookImageUrl, file, new Date());
                            insertBookInfoAsyncTask.execute();
//                            try {
//                                encrypt(context);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            } catch (NoSuchAlgorithmException e) {
//                                e.printStackTrace();
//                            } catch (NoSuchPaddingException e) {
//                                e.printStackTrace();
//                            } catch (InvalidKeyException e) {
//                                e.printStackTrace();
//                            }
                        }
                        mSharedPreferences.setDownloadedFileID(0);
                        mSharedPreferences.setBookDownloaded("");
                        Toast.makeText(context, R.string.download_complete,Toast.LENGTH_SHORT).show();
                    }
                }
                cursor.close();
            }
        }
    }

}
