package ph.com.cdu.apptwo.presentation.presenter;

import java.util.Date;

/**
 * Created by Neil Cruz on 16/10/2017.
 */

public interface BookDetailsPresenter {
    void onCreate();

    void onDataLoad();

    void onDownloadTapped();

    void onTransactionDone(String bookId, CharSequence bookTitle, String bookImageUrl,
                           String bookURLDownload, Date date, String bookPrice);

    void onSuccess();

    void initBindService();

    void onCheckPurchaseHistory();

    void onPurchaseTapped();

    void initConsumePurchase(String purchaseToken);

    void initBookStatus();

    void onGooglePlayPayment(String userId, String sku, String transactionId, String orderId,
                             String bookId, CharSequence bookTitle, String bookImage, String bookURL);

    void onCheckUserTransaction();

    void initAnalytics();

    void onMiss(int uniqueId);

    void setUpButtonPrice(String status, int color, boolean function);

    void onReDownload();

    void validateLocalFile(String id, String userId);
}
