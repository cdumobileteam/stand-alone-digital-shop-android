package ph.com.cdu.apptwo.domain.interactor;

import android.content.Context;

import ph.com.cdu.apptwo.data.rest.requestParameters.PaymentParams;
import ph.com.cdu.apptwo.data.rest.response.PaymentResponse;

/**
 * Created by Patrick Santos on 31/01/2018.
 */

public interface PaymentInteractor {
    interface onPaymentDataLoadedListener {
        void onSuccess(PaymentResponse paymentData);

        void onFailed();
    }

    void onRequestPayment(onPaymentDataLoadedListener listener, Context context, PaymentParams paymentParams);
}
