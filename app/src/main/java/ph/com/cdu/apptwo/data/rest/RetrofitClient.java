package ph.com.cdu.apptwo.data.rest;

import android.content.Context;
import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import ph.com.cdu.apptwo.R;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import timber.log.Timber;

/**
 * Created by Neil Cruz on 12/10/2017.
 */

public class RetrofitClient {

    public enum ApiType {
        API_GOOGLE_PAYMENT, API_PURCHASE_BOOK, API_BOOKS, API_NEW_USER, API_USERS
    }

    public static CDUService getCduService(Context context, ApiType apiType) {

        String baseURL;

        if (apiType == ApiType.API_PURCHASE_BOOK) {
            baseURL = context.getString(R.string.url_digitalshop2);
        } else if(apiType == ApiType.API_BOOKS) {
            baseURL = context.getString(R.string.url_digitalshop2);
        } else if (apiType == ApiType.API_NEW_USER) {
            baseURL = context.getString(R.string.url_digitalshop2);
        } else if(apiType == ApiType.API_USERS) {
            baseURL = context.getString(R.string.url_digitalshop2);
        } else {
            baseURL = context.getString(R.string.url_digitalshop2);
        }

        return getClient(baseURL).create(CDUService.class);
    }

    private static Retrofit getClient(String baseUrl) {

        Retrofit retrofit;

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("OKHttp",message);
            }
        });
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit;
    }
}
