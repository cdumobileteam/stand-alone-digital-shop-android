package ph.com.cdu.apptwo.util;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.util.HashMap;

import ph.com.cdu.apptwo.R;

/**
 * Created by Patrick Santos on 20/02/2018.
 */

public class Gaid extends Application {

    public static int GENERAL_TRACKER = 0;
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    public HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    private Context mContext;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    public Gaid(Context context) {
        this.mContext = context;
        sAnalytics = GoogleAnalytics.getInstance(mContext);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    public synchronized Tracker getDefaultTracker(TrackerName trackerId) {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (!mTrackers.containsKey(trackerId)) {
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ?
                    sAnalytics.newTracker("UA-108351510-3"): (trackerId == TrackerName.GLOBAL_TRACKER) ?
                    sAnalytics.newTracker(R.xml.global_tracker): sAnalytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
//        if (sTracker == null) {
//            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
//        }
//
//        return sTracker;
    }

    synchronized public void sendGAID(String screenName, String category, String action) {
        sTracker = getDefaultTracker(TrackerName.GLOBAL_TRACKER);
        sTracker.setScreenName(screenName);
        sTracker.send(new HitBuilders.EventBuilder().setLabel(screenName)
                .setCategory(category)
                .setAction(action)
                .build());
    }

    synchronized  public void sendAppTracker(String screenName, String campaignData) {
        sTracker = getDefaultTracker(TrackerName.APP_TRACKER);
        sTracker.setScreenName(screenName);
        // Campaign data sent with this hit.
        sTracker.send(new HitBuilders.ScreenViewBuilder()
                .setCampaignParamsFromUrl(campaignData)
                .build()
        );
    }
}
