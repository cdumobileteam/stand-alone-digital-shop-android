package ph.com.cdu.apptwo.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Neil Cruz on 23/10/2017.
 */

public class NetworkConnection {
    Context ctx;
    public NetworkConnection(Context context){
        this.ctx = context;
    }

    public boolean hasActiveConnection(String link){
//        checking if has network connection
        ConnectivityManager connectivityManager = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

//            checking if has active connection
            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                URL url = new URL(link);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setRequestProperty("User-Agent", "test");
                connection.setRequestProperty("Connection", "close");
                connection.setConnectTimeout(1500);
                connection.setReadTimeout(1500);
                connection.connect();

                boolean connetionStatus = connection.getResponseCode() == 200;
                if (connetionStatus){
                    System.out.println("Network Connection: Has active internet connection. Response Code: " + connection.getResponseCode());
                    return true;
                } else {
                    System.out.println("Network Connection: Internet Connection Problem. Response Code: " + connection.getResponseCode());
                    return false;
                }
            } catch (IOException e) {
                System.out.println("Network Connection: Internet Connection Problem.\nCause: " + e.toString());
                return false;
            }

        }else{
            System.out.println("Network Connection: No network connection");
            return false;
        }
    }
}
