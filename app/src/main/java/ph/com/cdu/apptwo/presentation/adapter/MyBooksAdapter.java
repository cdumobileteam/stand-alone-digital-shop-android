package ph.com.cdu.apptwo.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.presentation.fragment.view.MyBooksView;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Neil Cruz on 27/10/2017.
 */

public class MyBooksAdapter extends RecyclerView.Adapter<MyBooksAdapter.ViewHolder> {

    private Context mContext;
    private MyBooksView mListener;
    private List<BookInfo> mMyBooks;
    private String mUserId;

    public MyBooksAdapter(Context context, MyBooksView myBooksView, ArrayList<BookInfo> bookInfos) {
        mContext = context;
        mListener = myBooksView;
        mMyBooks = bookInfos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_my_books, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, mListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        BookInfo myBooks = mMyBooks.get(position);
        ViewGroup.LayoutParams params = holder.mFrmLayoutBook.getLayoutParams();
//        if (mMyBooks.size() == 1) {
//            params.height = getHeight(560);
//            params.width = getWidth(340);
//        } else {
            params.height = getHeight(160);
            params.width = getWidth(100);
//        }
        holder.mFrmLayoutBook.setLayoutParams(params);
        if (myBooks.getUserId().equals(mUserId)) {
            holder.mLnrLayoutMyBooks.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(myBooks.getImageUrl()).fit().into(holder.mImgButtonBookImage, new Callback() {
                @Override
                public void onSuccess() {
                    holder.mGifDrawable.stop();
                    holder.mImgViewBookLoading.setVisibility(View.GONE);
                    holder.mImgViewPlaceholder.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
        } else {
            holder.mImgViewPlaceholder.setVisibility(View.GONE);
            holder.mLnrLayoutMyBooks.setVisibility(View.GONE);
        }
    }

    private int getWidth(int width) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width,
                mContext.getResources().getDisplayMetrics());
    }

    private int getHeight(int height) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height,
                mContext.getResources().getDisplayMetrics());
    }

    private BookInfo getMyBooks(int position) {
        return mMyBooks.get(position);
    }

    @Override
    public int getItemCount() {
        if (mMyBooks != null) {
            return mMyBooks.size();
        } else {
            return 0;
        }
    }

    public void updateDeck(List<BookInfo> myBooks, String user_id_fk) {
        mMyBooks = myBooks;
        mUserId = user_id_fk;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private GifDrawable mGifDrawable;
        private MyBooksView mListener;
        @BindView(R.id.linearLayoutMyBooks)
        LinearLayout mLnrLayoutMyBooks;
        @BindView(R.id.frameLayoutBook)
        FrameLayout mFrmLayoutBook;
        @BindView(R.id.imageViewPlaceholder)
        ImageView mImgViewPlaceholder;
        @BindView(R.id.imageButtonBookImage)
        ImageButton mImgButtonBookImage;
        @BindView(R.id.gifImageViewBookStatusLoading)
        GifImageView mImgViewBookLoading;

        public ViewHolder(View itemView, MyBooksView listener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            mListener = listener;
            mGifDrawable = (GifDrawable) mImgViewBookLoading.getDrawable();
            mImgButtonBookImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            BookInfo myBooks = getMyBooks(getAdapterPosition());
            mListener.openBook(myBooks);
        }
    }
}
