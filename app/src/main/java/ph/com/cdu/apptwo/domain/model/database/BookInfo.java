package ph.com.cdu.apptwo.domain.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Neil Cruz on 25/10/2017.
 */

@Entity(tableName = "bookinfo")
public class BookInfo {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "unique_id")
    private int pid;
    @ColumnInfo(name = "book_id")
    private String id;
    @ColumnInfo(name = "userId")
    private String userId;
    @ColumnInfo(name = "book_title")
    private String title;

    private String encodedImageUrl;
    @ColumnInfo(name = "book_image_url")
    private String imageUrl;
    @ColumnInfo(name = "book_url")
    private String urlDownload;
    @ColumnInfo(name = "transaction_date")
    private String transactionDate;

    public BookInfo(String id, String userId, String title, String encodedImageUrl, String imageUrl, String urlDownload, String transactionDate) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.encodedImageUrl = encodedImageUrl;
        this.imageUrl = imageUrl;
        this.urlDownload = urlDownload;
        this.transactionDate = transactionDate;
    }

    @Ignore
    public BookInfo() {
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEncodedImageUrl() {
        return encodedImageUrl;
    }

    public void setEncodedImageUrl(String encodedImageUrl) {
        this.encodedImageUrl = encodedImageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrlDownload() {
        return urlDownload;
    }

    public void setUrlDownload(String urlDownload) {
        this.urlDownload = urlDownload;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
}
