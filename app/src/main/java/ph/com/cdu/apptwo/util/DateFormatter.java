package ph.com.cdu.apptwo.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Neil Cruz on 25/10/2017.
 */

public class DateFormatter {

    private SimpleDateFormat mDateFormat;

    public String formatDateToShowFormat(Date date) {
        mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return mDateFormat.format(date);
    }
}
