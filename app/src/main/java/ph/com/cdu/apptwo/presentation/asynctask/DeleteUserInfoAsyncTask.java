package ph.com.cdu.apptwo.presentation.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractor;
import ph.com.cdu.apptwo.domain.interactor.DatabaseInteractorImpl;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;

/**
 * Created by Patrick Santos on 08/02/2018.
 */

public class DeleteUserInfoAsyncTask extends AsyncTask<UserInfo, Void, Void> {
    private Context mContext;

    public DeleteUserInfoAsyncTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected Void doInBackground(UserInfo... userInfos) {
        DatabaseInteractor databaseInteractor = new DatabaseInteractorImpl();
        databaseInteractor.deleteUserInfo(mContext);
        return null;
    }
}
