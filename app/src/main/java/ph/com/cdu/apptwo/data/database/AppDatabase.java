package ph.com.cdu.apptwo.data.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import ph.com.cdu.apptwo.data.database.dao.BookInfoDao;
import ph.com.cdu.apptwo.data.database.dao.BookShopDao;
import ph.com.cdu.apptwo.domain.model.database.BookInfo;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.domain.model.database.UserInfo;
import ph.com.cdu.apptwo.data.database.converter.DateConverter;
import ph.com.cdu.apptwo.data.database.dao.UserInfoDao;

/**
 * Created by Neil Cruz on 24/10/2017.
 */

@Database(entities = {UserInfo.class, BookInfo.class, BookShop.class}, version = 1)

@TypeConverters(DateConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    static final String DATABASE_NAME = "cosmicdigitaluniversedb";

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,
                    DATABASE_NAME).addMigrations(MIGRATION_1_2).build();
        }

        return INSTANCE;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DELETE FROM userinfo");
            database.execSQL("DELETE FROM bookinfo");
            database.execSQL("DELETE FROM bookdirectory");
        }
    };

    public abstract UserInfoDao userInfoDao();

    public abstract BookInfoDao bookInfoDao();

    public abstract BookShopDao bookShopDao();
}
