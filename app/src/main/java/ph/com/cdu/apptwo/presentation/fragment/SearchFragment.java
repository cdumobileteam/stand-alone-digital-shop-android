package ph.com.cdu.apptwo.presentation.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.cdu.apptwo.R;
import ph.com.cdu.apptwo.domain.model.database.BookShop;
import ph.com.cdu.apptwo.presentation.activity.SearchActivity;
import ph.com.cdu.apptwo.presentation.adapter.SearchAdapter;
import ph.com.cdu.apptwo.presentation.asynctask.GetBookTitleAsyncTask;

public class SearchFragment extends Fragment {

    private SearchActivity mSearchActivity;
    private Context mContext;
    private SearchAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private String mBookTitle;
    @BindView(R.id.recyclerViewBookSearch)
    RecyclerView mRcyclrBookSearch;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBookTitle = getArguments().getString(getString(R.string.params_book_title));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this,view);
        mContext = getContext();
        mAdapter = new SearchAdapter(mContext,mSearchActivity,new ArrayList<BookShop>(0));
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRcyclrBookSearch.setAdapter(mAdapter);
        mRcyclrBookSearch.setLayoutManager(mLinearLayoutManager);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
        GetBookTitleAsyncTask getBookTitleAsyncTask = new GetBookTitleAsyncTask(mContext);
        getBookTitleAsyncTask.setmCallBackTask(new GetBookTitleAsyncTask.CallBackTask() {
            @Override
            public void callBack(List<String> bookShops) {
                Log.d("query", bookShops.toString());
//                List<BookShop> objectList = new ArrayList<BookShop>(bookShops.);
//                mAdapter.updateSearch(objectList);
            }
        });
        getBookTitleAsyncTask.execute("%"+mBookTitle+"%");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SearchActivity) {
            mSearchActivity = (SearchActivity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
